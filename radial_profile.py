def radial_profile(data, centre = None):
    if centre = None :
        centre = [data.shape[0]/2-1, data.shape[1]/2-1]
    
    i, j = np.indices((data.shape))
    r = np.sqrt((j - center[1])**2 + (i - center[0])**2)
    r = r.astype(np.int)

    tbin = np.bincount(r.ravel(), data.ravel())
    nr = np.bincount(r.ravel())
    radialprofile = tbin / nr
    return radialprofile

def radial_profile_partial(data, ):
    if centre = None :
        centre = [data.shape[0]/2-1, data.shape[1]/2-1]
    
    i, j = np.indices((data.shape))
    r = np.sqrt((j - center[1])**2 + (i - center[0])**2)
    r = r.astype(np.int)

    tbin = np.bincount(r.ravel(), data.ravel())
    nr = np.bincount(r.ravel())
    radialprofile = tbin / nr
    return radialprofile

if __name__ == '__main__':
    array = np.random.random((400, 100)).reshape((400, 100))
    r_profile, nr, r = radial_profile(array, (49, 199))


