#!/usr/bin/env python
"""
cspadMaskMaker:
A GUI for creating bad pixel maps. 

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.

UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pprint
import random
import wx
import sys
import h5py
import numpy as np
from scipy import sparse
import lcls as lc
import blackbox as bx
import thread

# The recommended way to use wx with mpl is with the WXAgg
# backend. 
#
import matplotlib as mpl
mpl.use('WXAgg')
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

#---------------------------------------------------------------------------

# This is how you pre-establish a file filter so that the dialog
# only shows the extension(s) you want it to.
wildcard = "Hierarchical Data Format (*.h5)|*.h5|"     \
           "Python source (*.py)|*.py|"     \
           "Compiled Python (*.pyc)|*.pyc|" \
           "Binary dat (*.dat)|*.dat|" \
           "All files (*.*)|*.*"

#---------------------------------------------------------------------------

class mainFrame(wx.Frame):
    """ The main frame of the application
    """
    title = "LCLS CsPAD Mask Maker"
    myDet = lc.cspad()
    cold = myDet.cold
    hot = myDet.hot
    det_numPixX = det_numPixY = myDet.det_numPixX
    cmax = 0
    cmin = 0
    myFiles = []
    initMask = 1
    maskKeyPressed = 0
    unmaskKeyPressed = 0
    flags = wx.ALIGN_LEFT | wx.ALL | wx.ALIGN_CENTER_VERTICAL
    # Front detector
    img = np.zeros((1,1))
    haveImage = False

    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, self.title)
  
        self.create_menu()
        self.create_status_bar()
        self.create_main_panel()
        self.draw_figure()

    def create_menu(self):
        self.menubar = wx.MenuBar()
        
        menu_file = wx.Menu()
        m_expt = menu_file.Append(-1, "&Save plot\tCtrl-S", "Save plot to file")
        self.Bind(wx.EVT_MENU, self.on_save_plot, m_expt)
        menu_file.AppendSeparator()
        m_exit = menu_file.Append(-1, "E&xit\tCtrl-X", "Exit")
        self.Bind(wx.EVT_MENU, self.on_exit, m_exit)
        
        menu_help = wx.Menu()
        m_about = menu_help.Append(-1, "&About\tF1", "About the demo")
        self.Bind(wx.EVT_MENU, self.on_about, m_about)
        
        self.menubar.Append(menu_file, "&File")
        self.menubar.Append(menu_help, "&Help")
        self.SetMenuBar(self.menubar)

    def create_main_panel(self):
        """ Creates the main panel with all the controls on it:
             * mpl canvas 
             * mpl navigation toolbar
             * Control panel for interaction
        """
        self.panel = wx.Panel(self)
        
        # Create the mpl Figure and FigCanvas objects. 
        # 8x8 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig = plt.Figure((8.0, 8.0), dpi=self.dpi) # figsize,dpi
        self.canvas = FigCanvas(self.panel, -1, self.fig)
        
        # hbox2
        self.caxis_label = wx.StaticText(self.panel, -1, 
            "caxis (min,max): ")        
        self.caxisMinTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_caxisMin_enter, 
                  self.caxisMinTextbox)
        self.caxisMaxTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_caxisMax_enter, 
                  self.caxisMaxTextbox)
        self.logCheckbox = wx.CheckBox(self.panel, -1, 
            "Log scale",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_logcheckbox_grid, self.logCheckbox)
        self.cmax_label = wx.StaticText(self.panel, -1, 
            "max: %s" % self.cmax)
        self.cmin_label = wx.StaticText(self.panel, -1, 
            "min: %s" % self.cmin)
        # hbox3
        self.maskRows_label = wx.StaticText(self.panel, -1, 
            "Rows (Start,End): ") 
        self.maskRowsStartTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_maskRowsStart_enter, self.maskRowsStartTextbox)
        self.maskRowsEndTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_maskRowsEnd_enter, self.maskRowsEndTextbox)
        # hbox4
        self.maskCols_label = wx.StaticText(self.panel, -1, 
            "Columns (Start,End): ") 
        self.maskColsStartTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_maskColsStart_enter, self.maskColsStartTextbox)
        self.maskColsEndTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_maskColsEnd_enter, self.maskColsEndTextbox)
        self.maskDead_label = wx.StaticText(self.panel, -1, 
            "Dead pixels (cold,hot): ") 
        self.maskDeadColdTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_maskDeadCold_enter, self.maskDeadColdTextbox)
        self.maskDeadHotTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_maskDeadHot_enter, self.maskDeadHotTextbox)

        self.open = wx.Button(self.panel, -1, "Open hdf5")
        self.Bind(wx.EVT_BUTTON, self.OnOpen, self.open)

        self.save = wx.Button(self.panel, -1, "Save hdf5")
        self.Bind(wx.EVT_BUTTON, self.OnSave, self.save)
        
        # Create the navigation toolbar, tied to the canvas
        #
        self.toolbar = NavigationToolbar(self.canvas)
        
        #
        # Layout with box sizers
        #
        
        # hbox2
        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.Add(self.caxis_label, 0, flag=self.flags)
        self.hbox2.AddSpacer(30)
        self.hbox2.Add(self.caxisMinTextbox, 0, border=3, flag=self.flags)
        self.hbox2.Add(self.caxisMaxTextbox, 0, border=3, flag=self.flags)
        self.hbox2.Add(self.logCheckbox, 0, border=3, flag=self.flags)
        self.logCheckbox.SetValue(0)
        self.hbox2.Add(self.cmax_label, 0, flag=self.flags)
        self.hbox2.AddSpacer(70)
        self.hbox2.Add(self.cmin_label, 0, flag=self.flags)

        # hbox3
        self.hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox3.Add(self.maskRows_label, 0, flag=self.flags)
        self.hbox3.Add(self.maskRowsStartTextbox, 0, border=3, flag=self.flags)
        self.hbox3.Add(self.maskRowsEndTextbox, 0, border=3, flag=self.flags)

        # hbox4
        self.hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox4.Add(self.maskCols_label, 0, flag=self.flags)
        self.hbox4.AddSpacer(65)
        self.hbox4.Add(self.maskColsStartTextbox, 0, border=3, flag=self.flags)
        self.hbox4.Add(self.maskColsEndTextbox, 0, border=3, flag=self.flags)

        # hbox4a
        self.hbox4a = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox4a.Add(self.maskDead_label, 0, flag=self.flags)
        self.hbox4a.AddSpacer(65)
        self.hbox4a.Add(self.maskDeadColdTextbox, 0, border=3, flag=self.flags)
        self.hbox4a.Add(self.maskDeadHotTextbox, 0, border=3, flag=self.flags)

        # hbox6
        self.hbox6 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox6.Add(self.open, 0, border=3, flag=self.flags)
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.save, 0, border=3, flag=self.flags)        
        
        # vbox
        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)
        self.vbox.AddSpacer(10)
        self.vbox.Add(self.hbox2, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox3, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox4, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox4a, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox6, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        
        self.panel.SetSizer(self.vbox)
        self.vbox.Fit(self)

    def OnOpen(self, evt):
        # Create the dialog. In this case the current directory is forced 
        # as the starting directory for the dialog, and no default file name is 
        # forced. This can easilly be changed in your program. This is an 'open' 
        # dialog, and allows multitple file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, 
        # this dialog is set up to change the current working directory to the 
        # path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # Show the dialog and retrieve the user response. If it is the OK 
        # response, process the data.
        if dlg.ShowModal() == wx.ID_OK:
            # This returns a Python list of files that were selected.
            self.filename = dlg.GetPath()

            self.flash_status_message('Drawing image ...')

            # Get the image and display
            self.get_image()
            self.flash_status_message('Resetting mask ...')
            self.mask = np.ones(np.shape(self.img))
            self.maskAsicEdge = np.ones(np.shape(self.img))
            self.maskDead = np.ones(np.shape(self.img))
            self.initMask = 1
            self.haveImage = True
            self.draw_figure()
            
        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def get_image(self):
        self.img = self.myDet.loadUnassembledCSPAD( self.filename )
        
    def OnSave(self, evt):
        #self.log.WriteText("CWD: %s\n" % os.getcwd())

        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'save' dialog.
        #
        # Unlike the 'open dialog' example found elsewhere, this example does NOT
        # force the current working directory to change if the user chooses a different
        # directory than the one initially set.
        dlg = wx.FileDialog(
            self, message="Save file as ...", defaultDir=os.getcwd(), 
            defaultFile="badpixelmap.h5", wildcard=wildcard, style=wx.SAVE
            )

        # This sets the default filter that the user will initially see. Otherwise,
        # the first filter in the list will be used by default.
        dlg.SetFilterIndex(3)

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()

            f = h5py.File(path,"w")
            data = f.create_group("data");
            data.create_dataset("data",data=self.img);
            f.close()

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def OnLoad(self, evt):
        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'open' dialog, and allows multitple
        # file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, this
        # dialog is set up to change the current working directory to the path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # This sets the default filter that the user will initially see. Otherwise,
        # the first filter in the list will be used by default.
        dlg.SetFilterIndex(3)

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()

            self.myLabel = np.loadtxt(path)

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def on_logcheckbox_grid(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()
        
    def on_caxisMin_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_caxisMax_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()      

    def on_maskRowsStart_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_maskRowsEnd_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_maskColsStart_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_maskColsEnd_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_maskDeadCold_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_maskDeadHot_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()
 
    def create_status_bar(self):
        self.statusbar = self.CreateStatusBar()

    def draw_figure(self):
        """ Redraws the figure
        """        
        caxisMin = self.caxisMinTextbox.GetValue()
        caxisMax = self.caxisMaxTextbox.GetValue()
        maskRowsStart = self.maskRowsStartTextbox.GetValue() # must be int for now
        maskRowsEnd = self.maskRowsEndTextbox.GetValue()
        maskColsStart = self.maskColsStartTextbox.GetValue() # must be int for now
        maskColsEnd = self.maskColsEndTextbox.GetValue()
        maskDeadCold = self.maskDeadColdTextbox.GetValue() # must be int for now
        maskDeadHot = self.maskDeadHotTextbox.GetValue()
        logscale = self.logCheckbox.GetValue()
        rx = ry = cx = cy = ''

        img = self.myDet.data

        # Check sensible input
        if not (caxisMin == '' or caxisMax == ''):
            vmin = caxisMin
            vmax = caxisMax
        else:
            vmin = vmax = None

        if not (maskRowsStart == '' or maskRowsEnd == ''):
            rx = int(maskRowsStart)
            ry = int(maskRowsEnd)

        if not (maskColsStart == '' or maskColsEnd == ''):
            cx = int(maskColsStart)
            cy = int(maskColsEnd)
        
        if self.haveImage:
            if self.initMask:    
                # Set asics edge as badpixels
                asicfs = 194;
                asicss = 185;
                for i in range(0,8):
                    self.maskAsicEdge[:,i*asicfs] = 0
                    self.maskAsicEdge[:,(i+1)*asicfs-1] = 0
                    self.maskAsicEdge[i*asicss,:] = 0
                    self.maskAsicEdge[(i+1)*asicss-1,:] = 0
                self.initMask = 0

            # Set cold and hot pixels as badpixels
            self.maskDead = np.ones(np.shape(self.img))
            if not (maskDeadCold == ''):
                self.maskDead[img[:,:] < int(maskDeadCold)] = 0
            if not (maskDeadHot == ''):
                self.maskDead[img[:,:] > int(maskDeadHot)] = 0

            # Set user-defined rectangular area as badpixels
            if not (rx == '' and ry == '' and cx == '' and cy == ''):
                print "rectangular area"
                self.mask[rx:ry,cx:cy] = 0
            
            img = self.mask*self.maskDead*self.maskAsicEdge*img # apply mask
            self.img = img

        self.cmax = np.max(img)
        self.cmin = np.min(img)
        self.cmax_label.SetLabel("max: %6.1f" % self.cmax)
        self.cmin_label.SetLabel("min: %6.1f" % self.cmin)

        if logscale == True:
            eps = 1e-18
            img = np.log10(np.absolute(img)+ eps)

        #self.fig.clf()
        ax = self.fig.add_subplot(111, axisbg='r')

        if self.initMask:  
            self.fig.canvas.mpl_connect('key_press_event', self.on_press)
            self.fig.canvas.mpl_connect('key_release_event', self.on_release)
            self.fig.canvas.mpl_connect('button_press_event', self.on_click)

        imgplot = ax.imshow(img,interpolation="nearest",cmap='jet',vmin=vmin,vmax=vmax)
        ax.set_xlabel('Columns')
        ax.set_ylabel('Rows')
        self.canvas.draw()
    
    def on_save_plot(self, event):
        file_choices = "PNG (*.png)|*.png"
        
        dlg = wx.FileDialog(
            self, 
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="dp.png",
            wildcard=file_choices,
            style=wx.SAVE)
        
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)
        
    def on_exit(self, event):
        self.Destroy()
        
    def on_about(self, event):
        msg = """ A GUI for viewing/labeling diffraction patterns.
        """
        dlg = wx.MessageDialog(self, msg, "About", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
    
    def flash_status_message(self, msg, flash_len_ms=500):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(
            wx.EVT_TIMER, 
            self.on_flash_status_off, 
            self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)
    
    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')

    # Mouse and key events
    def on_click(self, event):
        if self.maskKeyPressed:
            self.mask[round(event.ydata),round(event.xdata)] = 0
            print "Masking img(",round(event.ydata),",",round(event.xdata),"): ", self.img[round(event.ydata),round(event.xdata)]
            self.draw_figure()
        elif self.unmaskKeyPressed:
            self.mask[round(event.ydata),round(event.xdata)] = 1
            print "img:",self.img[round(event.ydata),round(event.xdata)]
            print "orig:",self.myDet.data[round(event.ydata),round(event.xdata)]
            self.img[round(event.ydata),round(event.xdata)] = self.myDet.data[round(event.ydata),round(event.xdata)]
            print "Unmasking img(",round(event.ydata),",",round(event.xdata),"): ", self.myDet.data[round(event.ydata),round(event.xdata)]
            self.draw_figure()
        else:
            print "Current pos(",round(event.ydata),",",round(event.xdata),"): ", self.img[round(event.ydata),round(event.xdata)]

    def on_press(self, event):
        if event.key == 'm':
            self.maskKeyPressed = 1
            #print('In masking mode: ', event.key)
        elif event.key == 'u':
            self.unmaskKeyPressed = 1
            #print('In unmasking mode: ', event.key)

    def on_release(self, event):
        if event.key == 'm':
            self.maskKeyPressed = 0
            #print('Exiting masking mode: ', event.key)
        elif event.key == 'u':
            self.unmaskKeyPressed = 0
            #print('Exiting masking mode: ', event.key)
#----------------------------------------------------------------------
class MySplashScreen(wx.SplashScreen):
    def __init__(self):
        bmp = wx.Image(opj("bitmaps/unagi.png")).ConvertToBitmap()
        wx.SplashScreen.__init__(self, bmp,
                                 wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT,
                                 30, None, -1) # 3000
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.fc = wx.FutureCall(10, self.ShowMain) # 1000


    def OnClose(self, evt):
        # Make sure the default handler runs too so this window gets
        # destroyed
        evt.Skip()
        self.Hide()
        
        # if the timer is still running then go ahead and show the
        # main frame now
        if self.fc.IsRunning():
            self.fc.Stop()
            self.ShowMain()


    def ShowMain(self):
        frame = mainFrame()
        frame.Show()

def opj(path):
    """Convert paths to the platform-specific separator"""
    st = apply(os.path.join, tuple(path.split('/')))
    # HACK: on Linux, a leading / gets lost...
    if path.startswith('/'):
        st = '/' + st
    return st

#----------------------------------------------------------------------
if __name__ == '__main__':
    app = wx.App()
    splash = MySplashScreen()
    splash.Show()
    app.MainLoop()
