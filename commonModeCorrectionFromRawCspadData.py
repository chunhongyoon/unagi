import h5py
import numpy as np
import cspad_ij_to_xy as i2x
import matplotlib.pyplot as plt

# Set run number
runNum = 35

imageFile = "/reg/data/ana14/cxi/cxig2614/scratch/cheetah/hdf5/r00"+str(runNum)+"-histogram/r00"+str(runNum)+"-detector0-class0-sum.h5"
geomFile = '/reg/neh/home3/yoon82/unagi/cspad17oct.geom'
numDiv = 8

f = h5py.File(imageFile,"r")
data = np.array(f["data/data"])
y,x = data.shape
cspad_geom = i2x.apply_geom(geomFile, data)

plt.subplot(1,3,1)
plt.imshow(np.log(abs(cspad_geom))), plt.title(str(runNum)+": Cheetah sum (log scale)")
plt.colorbar()

temp = np.zeros([y,numDiv])
for j in range(y):
    for i in range(numDiv):
        start = i*x/numDiv
        finish = (i+1)*x/numDiv
        temp[j,i] = np.sum(data[j,start:finish])
commonMode = np.zeros([numDiv,numDiv])
for j in range(numDiv):
    for i in range(numDiv):
        start = i*y/numDiv
        finish = (i+1)*y/numDiv
        commonMode[i,j] = np.sum(temp[start:finish,j])
commonMode /= y/numDiv * x/numDiv
#print commonMode
#plt.subplot(3,1, 2)
#plt.imshow(commonMode,interpolation='none'), plt.title("Common Mode")
#plt.colorbar()

correctedData = data
for j in range(numDiv):
    rangeY = np.arange(j*y/numDiv, (j+1)*y/numDiv)
    for i in range(numDiv):
        cspad1x1 = commonMode[j,i] * np.ones([y/numDiv,x/numDiv])
        rangeX = np.arange(i*x/numDiv, (i+1)*x/numDiv)
        correctedData[j*y/numDiv:(j+1)*y/numDiv,i*x/numDiv:(i+1)*x/numDiv] -= cspad1x1

cspad_geom_corrected = i2x.apply_geom(geomFile, correctedData)
plt.subplot(1,3,2)
plt.imshow(np.log(abs(cspad_geom_corrected))), plt.title("Common mode corrected (log scale)")
plt.colorbar()

plt.subplot(1,3,3)
plt.imshow(cspad_geom - cspad_geom_corrected), plt.title("Common mode geom (linear scale)")
plt.colorbar()
plt.show()
