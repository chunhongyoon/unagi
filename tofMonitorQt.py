import sys
import numpy 
import zmq
import pickle
import signal
import pyqtgraph
import copy
import PyQt4.QtGui
import PyQt4.QtCore


class ZMQListener(PyQt4.QtCore.QObject):
    
    zmqmessage = PyQt4.QtCore.pyqtSignal(int, dict)
    zmqmessage_monitorparams = PyQt4.QtCore.pyqtSignal(dict)
    
    def __init__(self, curr_rec_ip, curr_rec_port, tof_size):
        
        PyQt4.QtCore.QObject.__init__(self)

        self.rec_ip = curr_rec_ip
        self.rec_port = curr_rec_port
        self.zmq_context = zmq.Context()
        self.tof_size = tof_size

        print ('Listening at tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe = self.zmq_context.socket(zmq.SUB)
        self.zmq_hwm(1)
        self.zmq_subscribe.connect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe.setsockopt_string(zmq.SUBSCRIBE, u'tofdata')

        self.zmq_subscribe_monitorparams = self.zmq_context.socket(zmq.SUB)
        self.zmq_subscribe_monitorparams.connect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe_monitorparams.setsockopt_string(zmq.SUBSCRIBE, u'monitorparams')

        self.zmq_poller = zmq.Poller()
        self.zmq_poller.register(self.zmq_subscribe, zmq.POLLIN)
        self.zmq_poller.register(self.zmq_subscribe_monitorparams, zmq.POLLIN)

        self.gui_integration_size = 1

        self.accumulator = {}
        for tag in range(0,8):
            self.accumulator[tag] = {}
            self.accumulator[tag]['tof_trace'] = numpy.zeros((1, self.tof_size))
            self.accumulator[tag]['frame_counter'] = 0

        self.listening_timer = PyQt4.QtCore.QTimer()
        self.listening_timer.timeout.connect(self.listen)


    def update_gui_integration_size(self, size):
        print 'GUI integration size updated'
        self.gui_integration_size = size
        for tag in range(0,8):
            self.accumulator[tag] = {}
            self.accumulator[tag]['tof_trace'] = numpy.zeros((1, self.tof_size))
            self.accumulator[tag]['frame_counter'] = 0


    def start_listening(self):
        self.listening_timer.start()


    def stop_listening(self):
        self.listening_timer.stop()
        print ('Disconnecting from tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe.disconnect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe_monitorparams.disconnect('tcp://%s:%d' % (self.rec_ip, self.rec_port))


    def listen(self):
        socks = dict(self.zmq_poller.poll(0))
        if self.zmq_subscribe in socks and socks[self.zmq_subscribe] == zmq.POLLIN:
            header, tag, msg = self.zmq_subscribe.recv_multipart()
            zmq_dict = pickle.loads(msg)
            recv_tag =int(tag)
            self.accumulator[recv_tag]['tof_trace'] += zmq_dict['tof_trace']
            self.accumulator[recv_tag]['frame_counter'] += 1
            if self.accumulator[recv_tag]['frame_counter'] == self.gui_integration_size:
                self.accumulator[recv_tag]['tof_trace'] /= self.accumulator[recv_tag]['frame_counter']
                self.zmqmessage.emit(recv_tag, self.accumulator[recv_tag])
                self.accumulator[recv_tag]['tof_trace'] = numpy.zeros((1, self.tof_size))
                self.accumulator[recv_tag]['frame_counter'] = 0

        if self.zmq_subscribe_monitorparams in socks and socks[self.zmq_subscribe_monitorparams] == zmq.POLLIN:
            header, msg = self.zmq_subscribe_monitorparams.recv_multipart()
            params_dict = pickle.loads(msg)
            self.zmqmessage_monitorparams.emit(params_dict)




class MainFrame(PyQt4.QtGui.QWidget):
    """
    The main frame of the application
    """
    gui_integration_size_change = PyQt4.QtCore.pyqtSignal(int)
    listening_thread_start_processing = PyQt4.QtCore.pyqtSignal()
    listening_thread_stop_processing = PyQt4.QtCore.pyqtSignal()

    def __init__(self, arg_list):
        super(MainFrame, self).__init__()
        
        self.title = 'LCLS TOF Monitor'

        self.integration_size = 1
        self.gui_integration_size = 1
        self.tof_size = 20000
        self.timePerPixel = 1e-9 * 1e6# us
        self.timeline = numpy.arange(self.tof_size)*self.timePerPixel
        self.col = numpy.array([0,0,0])
        self.tag = 0
        self.data = {}
        for tag in range(0,8):
            self.data[tag] = {}
            self.data[tag]['tof_trace'] = numpy.zeros((1,self.tof_size))
        self.local_data = self.data[self.tag]
        self.t0 = 'None'
        self.m1 = 1.0
        self.t1 = 0.932
        self.m2 = 240.0
        self.t2 = 8.036
        self.rec_ip = '127.0.0.1'
        self.rec_port = 14747
        self.snd_ip = None
        self.snd_port = None


        if len(arg_list) == 3:
            self.rec_ip = str(arg_list[1])
            self.rec_port = int(arg_list[2])

        self.livestream = False
        self.roi = 'P[1:2],P[7:10],B[17:19]'
        self.Peaks = []
        self.Background = []
        self.panningSize = 100
        self.panningIntensity = []
        self.panningDelay = []
        
        self.init_zmq()        
        self.init_listening_thread()
        self.initUI()
        self.init_timer()

        
    def init_zmq(self):
        self.zmq_controlling_context = zmq.Context()
        self.zmq_control = self.zmq_controlling_context.socket(zmq.REQ)            
        if self.snd_ip != None:
            sip = self.snd_ip
        else:
            sip = self.rec_ip
        if self.snd_port != None:
            sport = self.snd_port
        else:
            sport = self.rec_port+1
        self.curr_snd_ip = sip
        self.curr_snd_port = sport
        self.zmq_control.connect('tcp://%s:%d' % (sip, sport))
        return


    def init_listening_thread(self):
        self.zeromq_listener_thread = PyQt4.QtCore.QThread()
        self.zeromq_listener = ZMQListener(self.rec_ip, self.rec_port, self.tof_size)
        self.zeromq_listener.moveToThread(self.zeromq_listener_thread)
        self.zeromq_listener.zmqmessage.connect(self.data_received)
        self.zeromq_listener.zmqmessage_monitorparams.connect(self.parameter_changed)
        self.gui_integration_size_change.connect(self.zeromq_listener.update_gui_integration_size)
        self.listening_thread_start_processing.connect(self.zeromq_listener.start_listening)
        self.listening_thread_stop_processing.connect(self.zeromq_listener.stop_listening)
        self.zeromq_listener_thread.start()
        self.listening_thread_start_processing.emit()

                 
    def init_timer(self):
        self.refresh_timer = PyQt4.QtCore.QTimer()
        self.refresh_timer.timeout.connect(self.update_image)


    def initUI(self):
        self.intregex = PyQt4.QtCore.QRegExp('[0-9]+')
        self.floatregex = PyQt4.QtCore.QRegExp('[0-9\.]+')
        self.ipregex = PyQt4.QtCore.QRegExp('[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')
        
        self.qtintvalidator = PyQt4.QtGui.QRegExpValidator()
        self.qtintvalidator.setRegExp(self.intregex)
        self.qtfloatvalidator = PyQt4.QtGui.QRegExpValidator()
        self.qtfloatvalidator.setRegExp(self.floatregex)
        self.qtipvalidator = PyQt4.QtGui.QRegExpValidator()
        self.qtipvalidator.setRegExp(self.ipregex)

        self.line_pen = pyqtgraph.mkPen('r', width=2)

        # cspad histogram viewer
        self.plot1 = pyqtgraph.PlotWidget(self)
        self.plot1.setLabel('bottom', text = 'Measured TOF (us)')
        self.plot1.setLabel('left', text = 'Voltage(mV)')
        self.plot1.setTitle('ToF')
        
        self.plot2 = pyqtgraph.PlotWidget(self)
        self.plot2.setLabel('bottom', text = 'Mass/Charge')
        self.plot2.setLabel('left', text = 'Voltage(mV)')
        self.plot2.setTitle('Mass/Charge')
        
        self.plot3 = pyqtgraph.PlotWidget(self)
        self.plot3.setLabel('bottom', text = 'frame number')
        self.plot3.setLabel('left', text = 'integrated peak')
        self.plot3.setTitle('Integrated signal vs Frame no.')
        
        self.plot4 = pyqtgraph.PlotWidget(self)
        self.plot4.setLabel('bottom', text = 'frag delay')
        self.plot4.setLabel('left', text = 'integrated peak')
        self.plot4.setTitle('Integrated signal vs Frag delay')
        
        self.xray_button = PyQt4.QtGui.QPushButton('X-ray', self)
        self.xray_button.setCheckable(True)
        self.xray_button.move(10, 10)
        self.xray_button.clicked[bool].connect(self.setLaser)
        self.align_button = PyQt4.QtGui.QPushButton('Align', self)
        self.align_button.setCheckable(True)
        self.align_button.move(10, 10)
        self.align_button.clicked[bool].connect(self.setLaser)
        self.frag_button = PyQt4.QtGui.QPushButton('Frag', self)
        self.frag_button.setCheckable(True)
        self.frag_button.move(10, 10)
        self.frag_button.clicked[bool].connect(self.setLaser)

        self.integrate_label = PyQt4.QtGui.QLabel(self)
        self.integrate_label.setText('<b>Integrate images:</b>')
        self.integrate_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.integrate_lineedit.setValidator(self.qtintvalidator)
        self.integrate_lineedit.setText(str(self.integration_size))
        self.integrate_lineedit.editingFinished.connect(self.update_integration_size)

        #self.integrate_gui_label = PyQt4.QtGui.QLabel(self)
        #self.integrate_gui_label.setText('Integrate GUI images:')
        #self.integrate_gui_lineedit = PyQt4.QtGui.QLineEdit(self)
        #self.integrate_gui_lineedit.setValidator(self.qtintvalidator)
        #self.integrate_gui_lineedit.setText(str(self.gui_integration_size))
        #self.integrate_gui_lineedit.editingFinished.connect(self.update_gui_integration_size)

        self.t0_label = PyQt4.QtGui.QLabel(self)
        self.t0_label.setText('Initial t0 (In us):')
        self.t0_lineedit = PyQt4.QtGui.QLineEdit(self)
        #self.t0_lineedit.setValidator(self.qtfloatvalidator)
        self.t0_lineedit.setText(str(self.t0))
        self.t0_lineedit.editingFinished.connect(self.update_t0)
        
        self.m1_label = PyQt4.QtGui.QLabel(self)
        self.m1_label.setText('Mass m1:')
        self.m1_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.m1_lineedit.setValidator(self.qtfloatvalidator)
        self.m1_lineedit.setText(str(self.m1))
        self.m1_lineedit.editingFinished.connect(self.update_m1_t1_m2_t2)
        
        self.t1_label = PyQt4.QtGui.QLabel(self)
        self.t1_label.setText('TOF t1 (in us):')
        self.t1_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.t1_lineedit.setValidator(self.qtfloatvalidator)
        self.t1_lineedit.setText(str(self.t1))
        self.t1_lineedit.editingFinished.connect(self.update_m1_t1_m2_t2)
        
        self.m2_label = PyQt4.QtGui.QLabel(self)
        self.m2_label.setText('Mass m2:')
        self.m2_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.m2_lineedit.setValidator(self.qtfloatvalidator)
        self.m2_lineedit.setText(str(self.m2))
        self.m2_lineedit.editingFinished.connect(self.update_m1_t1_m2_t2)

        self.t2_label = PyQt4.QtGui.QLabel(self)
        self.t2_label.setText('TOF t2 (in us):')
        self.t2_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.t2_lineedit.setValidator(self.qtfloatvalidator)
        self.t2_lineedit.setText(str(self.t2))
        self.t2_lineedit.editingFinished.connect(self.update_m1_t1_m2_t2)
  
        self.livestream_checkbox = PyQt4.QtGui.QCheckBox('Live Stream', self)
        self.livestream_checkbox.stateChanged.connect(self.update_livestream)

        self.region_label = PyQt4.QtGui.QLabel(self)
        self.region_label.setText('ROIs (e.g.: P[1:5],B[6,7]):')
        self.region_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.region_lineedit.setText(str(self.roi))
        self.region_lineedit.editingFinished.connect(self.update_roi)

   
        self.connect_label = PyQt4.QtGui.QLabel(self)
        self.connect_label.setText('Connect To (IP, Port):')
        self.connectip_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.connectip_lineedit.setText(self.rec_ip)
        self.connectip_lineedit.setValidator(self.qtipvalidator)
        self.connectip_lineedit.editingFinished.connect(self.update_connection)
        self.connectport_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.connectport_lineedit.setText(str(self.rec_port))
        self.connectport_lineedit.setValidator(self.qtintvalidator)
        self.connectport_lineedit.editingFinished.connect(self.update_connection)  

        self.vsplitter1 = PyQt4.QtGui.QSplitter(PyQt4.QtCore.Qt.Vertical)
        self.vsplitter1.addWidget(self.plot1)
        self.vsplitter1.addWidget(self.plot2)
        self.p1 = self.plot1.plot()
        self.p2 = self.plot2.plot()

        hlayout0 = PyQt4.QtGui.QHBoxLayout()
        hlayout0.addWidget(self.xray_button)
        hlayout0.addWidget(self.align_button)
        hlayout0.addWidget(self.frag_button)
        hlayout0.addStretch()

        hlayout1 = PyQt4.QtGui.QHBoxLayout()
        hlayout1.addWidget(self.integrate_label)
        hlayout1.addWidget(self.integrate_lineedit)
        #hlayout1.addWidget(self.integrate_gui_label)
        #hlayout1.addWidget(self.integrate_gui_lineedit)

        hlayout2 = PyQt4.QtGui.QHBoxLayout()
        hlayout2.addWidget(self.t0_label)
        hlayout2.addWidget(self.t0_lineedit)
        hlayout2.addStretch()
        hlayout2.addStretch()

        hlayout3 = PyQt4.QtGui.QHBoxLayout()
        hlayout3.addWidget(self.m1_label)
        hlayout3.addWidget(self.m1_lineedit)
        hlayout3.addWidget(self.t1_label)
        hlayout3.addWidget(self.t1_lineedit)

        hlayout4 = PyQt4.QtGui.QHBoxLayout()
        hlayout4.addWidget(self.m2_label)
        hlayout4.addWidget(self.m2_lineedit)
        hlayout4.addWidget(self.t2_label)
        hlayout4.addWidget(self.t2_lineedit)
        
        hlayout5 = PyQt4.QtGui.QHBoxLayout()
        hlayout5.addWidget(self.livestream_checkbox)
        hlayout5.addWidget(self.region_label)
        hlayout5.addWidget(self.region_lineedit)
        
        hlayout6 = PyQt4.QtGui.QHBoxLayout()
        hlayout6.addWidget(self.connect_label)
        hlayout6.addWidget(self.connectip_lineedit)
        hlayout6.addWidget(self.connectport_lineedit)

        self.vsplitter2 = PyQt4.QtGui.QSplitter(PyQt4.QtCore.Qt.Vertical)
        self.vsplitter2.addWidget(self.plot3)
        self.vsplitter2.addWidget(self.plot4)
        self.p3 = self.plot3.plot()
        self.p4 = self.plot4.plot()
            
        hsplitter = PyQt4.QtGui.QSplitter(PyQt4.QtCore.Qt.Horizontal)
        hsplitter.addWidget(self.vsplitter1)
        hsplitter.addWidget(self.vsplitter2)

        vlayout0 = PyQt4.QtGui.QVBoxLayout()
        vlayout0.addWidget(hsplitter)
        vlayout0.addLayout(hlayout0)
        vlayout0.addLayout(hlayout1)
        vlayout0.addLayout(hlayout2)
        vlayout0.addLayout(hlayout3)
        vlayout0.addLayout(hlayout4)
        vlayout0.addLayout(hlayout5)
        vlayout0.addLayout(hlayout6)

        self.update_roi()                
        self.setLayout(vlayout0)
        self.resize(600,800)        
        self.show()


    def setLaser(self, pressed):
        source = self.sender()

        font = source.font()
        if pressed:
            val = 1
            font.setBold(True);
        else: 
            val = 0
            font.setBold(False)
                        
        source.setFont(font)
                        
        if source.text() == "X-ray":
            self.col[2] = val                
        elif source.text() == "Align":
            self.col[1] = val            
        else:
            self.col[0] = val 
        
        self.tag = numpy.sum( self.col * numpy.array([4, 2, 1]) )
        print self.col, self.tag  


    def data_received(self, tag, datdict):
        self.data[tag] = copy.deepcopy(datdict)


    def parameter_changed(self, di):
        if 'tof_integration_size' in di.keys():
            if di['tof_integration_size'] != self.integration_size:
                self.integrate_lineedit.setText(str(di['tof_integration_size']))
                self.dialog = PyQt4.QtGui.QMessageBox(self)
                self.dialog.setStandardButtons(PyQt4.QtGui.QMessageBox.Ok)
                self.dialog.setIcon(PyQt4.QtGui.QMessageBox.Warning)
                self.dialog.setWindowTitle('Monitor Parameter Changed')
                self.dialog.setText('WARNING!\n\nOne of the monitor parameters has been changed from another GUI')
                self.dialog.setModal(False)
                self.dialog.show()


    def update_integration_size(self):
        new_integration_size = int(self.integrate_lineedit.text())
        if new_integration_size != self.integration_size:
            self.integration_size = new_integration_size
            print 'Sending command to %s:%d' % (self.curr_snd_ip, self.curr_snd_port)
            dict_to_send = {}
            dict_to_send['tof_integration_size'] = self.integration_size
            self.zmq_control.send_pyobj(dict_to_send)
            self.zmq_control.recv()
            print 'Command received'


    def update_gui_integration_size(self):
        new_gui_integration_size = int(self.integrate_gui_lineedit.text())
        if new_gui_integration_size != self.gui_integration_size:
            self.gui_integration_size = new_gui_integration_size
            self.gui_integration_size_change.emit(new_gui_integration_size)


    def update_t0(self):   
        self.t0 = self.t0_lineedit.text()
        self.update_image()     


    def update_m1_t1_m2_t2(self):   
        self.m1 = float(self.m1_lineedit.text())
        self.t1 = float(self.t1_lineedit.text())
        self.m2 = float(self.m2_lineedit.text())
        self.t2 = float(self.t2_lineedit.text())
        self.update_image()     

    def update_roi(self):
        myROIs = self.region_lineedit.text().split(',')
        # Clean up ROIs
        for i in range(len(self.Peaks)):
            self.plot1.removeItem(self.Peaks[i])
        for i in range(len(self.Background)):
            self.plot1.removeItem(self.Background[i])
        # Add region of interest
        self.Peaks = []
        numPeaks = 0
        numBackground = 0
        for x in myROIs:
            header,rem=x.split('[')
            if header == 'P':
                tok,rem=rem.split(']')
                lb,ub=tok.split(':')
                self.Peaks.append( pyqtgraph.LinearRegionItem([int(lb), int(ub)], bounds=[0,self.tof_size], movable=True, brush=pyqtgraph.mkBrush(255,0,0,70)) )
                self.plot1.addItem(self.Peaks[numPeaks])
                numPeaks += 1
            elif header == 'B':
                tok,rem=rem.split(']')
                lb,ub=tok.split(':')
                self.Background = [] # Only accept one background
                self.Background.append( pyqtgraph.LinearRegionItem([int(lb), int(ub)], bounds=[0,self.tof_size], movable=True, brush=pyqtgraph.mkBrush(0,0,255,70)) )
                self.plot1.addItem(self.Background[0])
                numBackground += 1
            else:
                print 'Invalid string: %s\n' % header
        # Set up text display on screen
        self.textsPeaks = []
        self.textsBackground = []
        for i in range(numPeaks):
            self.textsPeaks.append( pyqtgraph.TextItem("test", anchor=(0,-0.5)) )
            self.textsPeaks[i].setParentItem(self.Peaks[i])
            # Listen for mouse event
            self.Peaks[i].sigRegionChangeFinished.connect(self.update_image)
        for i in range(numBackground):
            self.textsBackground.append( pyqtgraph.TextItem("test", anchor=(0,-0.5)) )
            self.textsBackground[i].setParentItem(self.Background[i])
            # Listen for mouse event
            self.Background[i].sigRegionChangeFinished.connect(self.update_image)
        self.update_image()

    def update_connection(self):
        new_rec_ip = str(self.connectip_lineedit.text())
        new_rec_port = int(self.connectport_lineedit.text())
        if new_rec_ip != self.rec_ip or new_rec_port != self.rec_port:
            self.rec_ip = new_rec_ip
            self.rec_port = new_rec_port  
            self.listening_thread_stop_processing.emit()
            self.zeromq_listener_thread.quit()
            self.zeromq_listener_thread.wait()
 
            self.init_listening_thread()
            self.zmq_control.disconnect('tcp://%s:%d' % (self.curr_snd_ip, self.curr_snd_port))
            self.init_zmq()


    def update_livestream(self, state):
        if state == PyQt4.QtCore.Qt.Checked:
            self.livestream = True
            self.refresh_timer.start(500)
        else:
            self.livestream = False
            self.refresh_timer.stop()   


    def update_image(self):
        PyQt4.QtGui.QApplication.processEvents()

        if len(self.data.keys()) != 0:

            if self.livestream == True:
                self.local_data = self.data[self.tag]
                self.data = {}

            self.potential = numpy.asarray(self.local_data['tof_trace']).reshape(-1)
            self.time = range(self.tof_size)

            background = 0
            # Calculate background
            for i in range(len(self.textsBackground)):
                lowBound = round(self.Background[i].getRegion()[0])
                upperBound = round(self.Background[i].getRegion()[1])
                background = numpy.mean(self.potential[lowBound:upperBound])

            # Plot 1
            self.potential -= background
            self.p1.setData(x=self.timeline, y=self.potential, pen='g')

            currentPeakVal = 0
            currentLowBound = 0
            currentUpperBound = 0
            # Display mean value on screen
            for i in range(len(self.textsPeaks)):
                lowBound = round(self.Peaks[i].getRegion()[0])
                upperBound = round(self.Peaks[i].getRegion()[1])
                self.textsPeaks[i].setText('[%0.2f]' % numpy.mean(self.potential[lowBound:upperBound]))
                self.textsPeaks[i].setPos(round(self.Peaks[i].getRegion()[0]), 0)
                if i == 0:
                    currentPeakVal = numpy.mean(self.potential[lowBound:upperBound])
                    currentLowBound = lowBound
                    currentUpperBound = upperBound
            for i in range(len(self.textsBackground)):
                lowBound = round(self.Background[i].getRegion()[0])
                upperBound = round(self.Background[i].getRegion()[1])
                background = numpy.mean(self.potential[lowBound:upperBound])
                self.textsBackground[i].setText('[%0.2f]' % background)
                self.textsBackground[i].setPos(round(self.Background[i].getRegion()[0]), 0)

            if self.time == None:
                return

            # Plot 2
            t0 = 0
            if self.t0 == 'None':
                t0 = (numpy.sqrt(self.m1/self.m2)*self.t2-self.t1) / (numpy.sqrt(self.m1/self.m2)-1)
            else:
                t0 = float(str(self.t0)) # take user input

            t1_real = self.t1-t0
            myMasses = []
            for t in self.time:
                myMasses.append( self.m1 * pow((t-t0)/t1_real , 2) )
            self.p2.setData(x=myMasses,y=self.potential)

            # Plot 3

            self.panningIntensity.append(currentPeakVal)
            if len(self.panningIntensity) > self.panningSize:
                self.panningIntensity.pop(0)
            self.p3.setData(y=self.panningIntensity)

            # Plot 4
            fragTime = numpy.random.rand(1)
            xrayTime = numpy.random.rand(1)
            delayTime = fragTime - xrayTime
            self.panningDelay.append(float(delayTime))
            if len(self.panningDelay) > self.panningSize:
                self.panningDelay.pop(0)
            pairs = zip(self.panningDelay,self.panningIntensity)
            pairs.sort()
            self.p4.setData(x=[x1[0] for x1 in pairs],y=[x1[1] for x1 in pairs])

def main():

    signal.signal(signal.SIGINT, signal.SIG_DFL)    
    app = PyQt4.QtGui.QApplication(sys.argv)
    ex = MainFrame(sys.argv)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()  
