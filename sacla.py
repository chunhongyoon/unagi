#!/usr/bin/env python
"""
SACLA toolbox
Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.
 
UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pprint
import random
import wx
import sys
import h5py
import numpy as np
from scipy import sparse

class mpccd:
    det_numPixX = 0 # assemble pixel dimension in x
    det_numPixY = 0 # assemble pixel dimension in y
    numSensors = 8
    cold = 0
    hot = 0

    def __init__(self):
        self.name = 'mpccd'
    
    def displayName(self):
        print self.name

def initMPCCD( filename ):
    """
    Reads parameters from the file and returns the values:
    run number, number of detectors, tags, number of diffraction patterns

    Also, check whether there is only unassembled, assembled data available.
    
    filename - SACLA hdf5 file
    """    
    f = h5py.File( filename , "r") # load hdf5 file for reading
    group = f.keys()
    myFileInfo = group[0]
    myRun = group[1]
    myRunGroupKeys = f[myRun].keys()
    myDet_2d = []
    detKey = []
    myTag = []
    ind = 1
    hasAssemData = 0
    hasUnassemData = 0
    hasFront = 0
    hasBack = 0
    
    # Check whether dataset has assembled data and unassembled data
    if 'detector_2d_assembled_1' in myRunGroupKeys:
        detKey = 'detector_2d_assembled_'
        hasAssemData = 1
        hasFront = 1
    if 'detector_2d_assembled_2' in myRunGroupKeys:
        detKey = 'detector_2d_assembled_'
        hasAssemData = 1
        hasBack = 1
    if 'detector_2d_1' in myRunGroupKeys:
        detKey = 'detector_2d_'
        hasUnassemData = 1
        hasFront = 1
    if 'detector_2d_10' in myRunGroupKeys:
        detKey = 'detector_2d_'
        hasUnassemData = 1
        hasBack = 1

    while detKey+str(ind) in myRunGroupKeys:
        myDet_2d.append(detKey+str(ind))
        ind += 1
    if ind > 1:
        mySensorGroup = f[myRun+'/'+myDet_2d[0]] # group all images of the first sensor
        mySensorDetInfo = mySensorGroup.keys()[0] # detector_info for sensor1
        numDP = len(mySensorGroup.keys())-1 #get the number of frames (one entry is the detector info)
        for i in range(numDP):
            myTag.append(mySensorGroup.keys()[i+1])            
    return myRun, myDet_2d, myTag, numDP, hasAssemData, hasUnassemData, hasFront, hasBack
    f.close()

def pixPosMPCCD( filename, myRun, myDet_2d, myTag, px, py, numSensors, front=1):
    f = h5py.File( filename , "r") # load hdf5 file for reading

    pixPosX = np.zeros(( py, px, numSensors))
    pixPosY = np.zeros(( py, px, numSensors))
    appPixPosX = np.zeros(( py, px, numSensors))
    appPixPosY = np.zeros(( py, px, numSensors))

    counter = 0
    if front == 1:
        startSensor = 0
        endSensor = startSensor + 8
    else:
        startSensor = 8
        endSensor = startSensor + 2
        
    for i in range(startSensor, endSensor):
        pixSize = f[myRun+'/'+myDet_2d[i]+'/detector_info/pixel_size_in_micro_meter']
        theta = f[myRun+'/'+myDet_2d[i]+'/detector_info/detector_stage_direction_in_degree'].value
        theta = theta*np.pi/180 # radians
        w = f[myRun+'/'+myDet_2d[i]+'/detector_info/detector_stage_shift_weight']
        L = f[myRun+'/'+myDet_2d[i]+'/detector_info/manipulator_position_in_micro_meter']
        #L = (10./3.)*np.absolute(L)*1e-6 # fudge factor and unit correction
        xyz = f[myRun+'/'+myDet_2d[i]+'/detector_info/detector_coordinate_in_micro_meter']
        x = xyz[0]/pixSize[0]
        y = xyz[1]/pixSize[0]
        theta_s = f[myRun+'/'+myDet_2d[i]+'/detector_info/detector_rotation_angle_in_degree'].value
        theta_s = theta_s*np.pi/180 # radians
        # Sensor with pixel center positions
        sx=np.linspace(0,511,512)  
        sy=np.linspace(0,-1023,1024)
        sX, sY = np.meshgrid(sx, sy)
        # Rotate ccw w.r.t x-axis
        R = np.mat([[np.cos(theta_s), -np.sin(theta_s)],[np.sin(theta_s), np.cos(theta_s)]])
        sR = R*[sX.flatten(1), sY.flatten(1)]
        sXR = sR[0,:].reshape( py, px,order='F').copy()
        sYR = sR[1,:].reshape( py, px,order='F').copy()
        # Add offset
        sXRT = sXR+x
        sYRT = sYR+y
        # Slide to open aperature
        sXRTS = sXRT+np.cos(theta)*w*L/pixSize[0]
        sYRTS = sYRT+np.sin(theta)*w*L/pixSize[0]
        # Save pixel positions
        pixPosX[:,:,counter] = sXRTS
        pixPosY[:,:,counter] = sYRTS
        appPixPosX[:,:,counter] = np.round(sXRTS)
        appPixPosY[:,:,counter] = np.round(sYRTS)
        counter += 1
            
    appPixPosX = appPixPosX-np.min(appPixPosX)
    appPixPosY = appPixPosY-np.min(appPixPosY)
    appPixPosX = appPixPosX.astype(int)
    appPixPosY = appPixPosY.astype(int)
    return appPixPosX, appPixPosY
    f.close()

def layoutMPCCD( filename, myRun, myDet_2d, myTag, tagInst,
                   appPixPosX, appPixPosY, det_numPixX, det_numPixY,
                   px, py, numSensors, front=1):
        """
        Return assembled detector image with geometry correction.

        filename - SACLA hdf5 file containing images
        myRun - run number
        myDet_2d - list of sensor names
        myTag - list of tag names
        tagInst - integer for choosing image
        appPixPosX, appPixPosY - approximate pixel positions from pixPosMPCCD()
        det_numPixX, det_numPixY - final detector size
        px, py - number of pixels of one sensor
        numSensors - numbor of sensors in detector
        """
	f = h5py.File( filename , "r") # load hdf5 file for reading

	det = np.zeros(( py, px, numSensors))
	gain1 = f[myRun+'/'+myDet_2d[0]+'/detector_info/absolute_gain'].value
        
        counter = 0
        if front == 1:
            startSensor = 0
            endSensor = startSensor + 8
        else:
            startSensor = 8
            endSensor = startSensor + 2
	for i in range(startSensor, endSensor):
		gain = f[myRun+'/'+myDet_2d[i]+'/detector_info/absolute_gain'].value
		det[:,:,counter] = gain/gain1*f[myRun+'/'+myDet_2d[i]+'/'+myTag[tagInst]+'/detector_data']
                counter += 1

	img = sparse.coo_matrix((det.flatten(1),(appPixPosY.flatten(1),appPixPosX.flatten(1))),shape=( det_numPixY, det_numPixX))
	img = np.flipud(img.todense())
	return img
	f.close()

def assembleMPCCD( det, appPixPosX, appPixPosY, det_numPixX, det_numPixY,
                   px, py, numSensors):
        """
        Return assembled detector image with geometry correction.

        det - detector pixel values without gemoetry correction
        appPixPosX, appPixPosY - approximate pixel positions from pixPosMPCCD()
        det_numPixX, det_numPixY - final detector size
        px, py - number of pixels of one sensor
        numSensors - numbor of sensors in detector
        """  
	img = sparse.coo_matrix((det.flatten(1),(appPixPosY.flatten(1),appPixPosX.flatten(1))),shape=( det_numPixY, det_numPixX))
	img = np.flipud(img.todense())
	return img

def loadAssembledMPCCD( filename, myRun, myTag, tagInst, front=1):
        """
        Return already assembled detector image with geometry correction.

        filename - SACLA hdf5 file containing dark images
        myRun - run number
        myTag - list of tag names
        tagInst - integer for choosing image
        """  
	f = h5py.File( filename , "r") # load hdf5 file for reading
        if front:
	    img = f[myRun+'/detector_2d_assembled_1/'+myTag[tagInst]+'/detector_data']
        else:
	    img = f[myRun+'/detector_2d_assembled_2/'+myTag[tagInst]+'/detector_data']
	return img
	f.close()

def getImgMPCCD( filename, myRun, myDet_2d, myTag, tagInst,
                 appPixPosX, appPixPosY, det_numPixX, det_numPixY,
                 px, py, numSensors, front=1):
        """
        Return detector pixel values without geometry correction.

        filename - SACLA hdf5 file containing images
        myRun - run number
        myDet_2d - list of sensor names
        myTag - list of tag names
        tagInst - integer for choosing image
        appPixPosX, appPixPosY - approximate pixel positions from pixPosMPCCD()
        det_numPixX, det_numPixY - final detector size
        px, py - number of pixels of one sensor
        numSensors - numbor of sensors in detector
        """        
	f = h5py.File( filename , "r") # load hdf5 file for reading

	det = np.zeros(( py, px, numSensors))
        counter = 0
        if front == 1:
            startSensor = 0
            endSensor = startSensor + 8
        else:
            startSensor = 8
            endSensor = startSensor + 2

	gain1 = f[myRun+'/'+myDet_2d[0]+'/detector_info/absolute_gain'].value
        
	for i in range(startSensor, endSensor):
		gain = f[myRun+'/'+myDet_2d[i]+'/detector_info/absolute_gain'].value
		det[:,:,counter] = gain/gain1*f[myRun+'/'+myDet_2d[i]+'/'+myTag[tagInst]+'/detector_data']
                counter += 1

	return det
	f.close()
	
def getAvgDarkMPCCD( filename, appPixPosX, appPixPosY, det_numPixX, det_numPixY,
                     px, py, numSensors, front=1):
        """
        Assemble dark image by averaging over dark hdf5 file.

        filename - SACLA hdf5 file containing dark images
        appPixPosX, appPixPosY - approximate pixel positions from pixPosMPCCD()
        det_numPixX, det_numPixY - final detector size
        px, py - number of pixels of one sensor
        numSensors - numbor of sensors in detector
        """
        haveDark = False
        myRun, myDet_2d, myTag, numDP, hasAssemData, hasUnassemData, hasFront, hasBack = initMPCCD( filename )
	f = h5py.File( filename , "r") # load hdf5 file for reading

	det = np.zeros(( py, px, numSensors))

        if front == 1:
            startSensor = 0
            endSensor = startSensor + 8
        else:
            startSensor = 8
            endSensor = startSensor + 2

        if (front == 1 and hasFront == 1) or (front == 0 and hasBack == 1):
            gain1 = f[myRun+'/'+myDet_2d[0]+'/detector_info/absolute_gain'].value

            limit = 10 # maximum number of darks to average
            maxPatts = len(myTag)
            if maxPatts > limit:
                maxPatts = limit

            for tagInst in range(myPatts):
                counter = 0
        	for i in range(startSensor, endSensor):
                    gain = f[myRun+'/'+myDet_2d[i]+'/detector_info/absolute_gain'].value
                    det[:,:,counter] += gain/gain1*f[myRun+'/'+myDet_2d[i]+'/'+myTag[tagInst]+'/detector_data']/myPatts
                    counter += 1
                haveDark = True
        return det, haveDark
	f.close()

def getAssemDarkMPCCD( filename, myRun, myTag, front=1):
        """
        Return already assembled detector image with geometry correction.

        filename - SACLA hdf5 file containing dark images
        myRun - run number
        myTag - list of tag names
        tagInst - integer for choosing image
        """
        haveDark = False
        myRun, myDet_2d, myTag, numDP, hasAssemData, hasUnassemData, hasFront, hasBack = initMPCCD( filename )  
        f = h5py.File( filename , "r") # load hdf5 file for reading
        limit = 10 # maximum number of darks to average
        maxPatts = len(myTag)
        if maxPatts > limit:
            maxPatts = limit
        if front:
            img = np.zeros((2399,2399))
        else:
            img = np.zeros((1100,1200))
        if hasAssemData:
            if front:
                if hasFront:
                    for tagInst in range(maxPatts):
                        temp = f[myRun+'/detector_2d_assembled_1/'+myTag[tagInst]+'/detector_data'].value
                        img += temp/maxPatts
            else:
                if hasBack:
                    for tagInst in range(maxPatts):
                        temp = f[myRun+'/detector_2d_assembled_2/'+myTag[tagInst]+'/detector_data'].value
                        img += temp/maxPatts
            haveDark = True
        else:
            print "WARNING: Assembled dark MPCCD is not available. I will return empty dark image."
	return img, haveDark
	f.close()

def getCommonModeMPCCD( detector, startRow=950, endRow=1000 ):
        """
        Return common mode without geometry correction.

        detector - dark correct detector values without geometry correction
        """
        sample = detector[startRow:endRow,:,:]
        sample = np.mean(sample,axis=0)
        # Normalize by dividing by the mean
        sample = sample/np.mean(sample)
        cm = np.zeros_like(detector)
        for i in range(sample.shape[1]):
                cm[:,:,i] = np.tile(sample[:,i],(1024,1))
        return cm

def showMe(filename,dark,N=400,M=400,title='',subDim=7,vmin=0,vmax=5,wspace=0.001,hspace=0.1):
    """
    Show subset of data.

    myData - intensity data # shape (n_samples, n_features)
    N,M - height and width of each image in myData
    label - labels of myData # shape (n_samples)
    criteria - one of the values in label array
    thresh - threshold value    
    """
    import matplotlib.pyplot as plt
    plt.figure(figsize=(20,20))
    fig = plt.gcf()
    fig.suptitle(title, fontsize=14)
    ind = 1
    counter = 0
    numShown = 0
    cx = cy = 1200
    myRun, myDet_2d, myTag, numDP, hasAssemData, hasUnassemData, hasFront, hasBack = initMPCCD( filename )
    while counter < numDP:
        ind = 1
        for i in range(subDim):
            for j in range(subDim):
              if counter < numDP:
                plt.subplot(subDim,subDim,ind)
                if hasAssemData:
                    img = loadAssembledMPCCD( filename, myRun, myTag, counter, 1) # Front detector
                    #img =  loadAssembledMPCCD( filename, myRun, myTag, counter, 0) # Back detector
                    imgplot = plt.imshow(np.log10(np.abs(img[cy-N:cy+N,cx-M:cx+M]-dark[cy-N:cy+N,cx-M:cx+M]+1)))
                if vmax > 0:
                    imgplot.set_clim(vmin,vmax)
                plt.title(counter)
                plt.axis('off')
                ind += 1
                counter += 1
        plt.subplots_adjust(wspace=wspace,hspace=hspace)
        plt.show()





        
