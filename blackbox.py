#!/usr/bin/env python
"""
Blackbox:
Python module containing tools that you can use blindly
Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.
 
UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""
# Blackbox
import numpy as np
import matplotlib.pyplot as plt
import sys
from scipy.sparse import coo_matrix

# Plotting constants
markers = np.array([x for x in 's+xo*^2.D,Hv<1>348phd'])
markers = np.hstack([markers] * 20)
colors = np.array([x for x in 'rmygcbkrmygcbkrmygcbkrmygcbk'])
colors = np.hstack([colors] * 20)

def boost():
    """
    Try calling boost::python function
    """
    #from .build.blackbox import blackboxCPP as bxc
    import blackboxCPP as bxc
    print bxc.greet()

# normalize columns of a matrix
def normc(V):
    numRow, numCol = V.shape
    for i in range(numCol):
        V[:,i] = V[:,i]/np.linalg.norm(V[:,i])
    return V

# normalize rows of a matrix
def normr(V):
    numRow, numCol = V.shape
    for i in range(numRow):
        V[i,:] = V[i,:]/np.linalg.norm(V[i,:])
    return V

# variance normalization, each column has variance 1 
def varNormc(V):
    mean = V.mean(0) # mean of each column
    var = V.var(0)   # variance of each column
    V = (V-mean)/np.sqrt(var)
    return V

# L2 norm
def L2_norm(A,B,df=0):
    """
    Calculate L2-norm between A and B matrices

    A is DxM matrix
    B is DxN matrix
    df - forces diagonal to be 0
    """

    if A.shape[0] != B.shape[0]:
        print 'Input matrices must have same number of rows'
        sys.exit()
    if not (np.isrealobj(A) and np.isrealobj(B)):
        print 'Input matrices must be real'
        sys.exit()
    print A.shape, B.shape
    if A.shape[0] == 1:
        A = np.append(A,np.zeros(A.shape),axis=0)
        B = np.append(B,np.zeros(B.shape),axis=0)
    aa=sum(A*A)
    print "Done aa"
    bb=sum(B*B)
    print "Done bb"
    ab=np.dot(A.transpose(),B)
    print "Done ab"
    d = np.sqrt(np.tile(np.matrix(aa).transpose(), (1,len(bb))) + np.tile(bb, (len(aa),1)) - 2*ab)
    print "Done d"
    # make sure result is real
    d = np.asarray(d.real)
    # force 0 on the diagonal
    if df == 1 and d.shape[0] == d.shape[1]:        
        d = d*(1.-np.eye(d.shape[0]))
    print "Done L2_norm"
    return d

# Donut mask
# Program that returns a donut mask with inner radius r and outer radius R in NxM image
# Good = 0
# Background = 1
def donutMask(N,M,R,r):
    """
    Calculate a donut mask.

    N,M - The height and width of the image
    R   - Maximum radius from center
    r   - Minimum radius from center
    
    """
    mask = np.ones((N,M))
    for i in range(N):
        xDist = i-N/2
        xDistSq = xDist**2
        for j in range(M):
            yDist = j-M/2
            yDistSq = yDist**2
            rSq = xDistSq+yDistSq
            if rSq <= R**2 and rSq >= r**2:
                mask[j,i] = 0
    return mask

# Wedge mask
# Program that returns a wedge mask with inner radius r and outer radius R between minTheta and maxTheta in NxM image
# Good = 0
# Background = 1
def wedgeMask(N,M,R,r,minTheta,maxTheta):
    """
    Calculate a donut mask.

    N,M - The height and width of the image
    R   - Maximum radius from center
    r   - Minimum radius from center
    minTheta maxTheta
    """
    import math
    mask = np.ones((N,M))
    for i in range(N):
        xDist = i-N/2.
        xDistSq = xDist**2
        for j in range(M):
            yDist = j-M/2.
            yDistSq = yDist**2
            rSq = xDistSq+yDistSq
            if rSq <= R**2 and rSq >= r**2:
                ang = math.atan2(i-N/2,j-M/2)
                if np.abs(ang)*180./3.14 <= maxTheta and np.abs(ang)*180./3.14 >= minTheta:
                    mask[j,i] = 0
    return mask

def pointsInRoi(pointsX,pointsY,roi):
    pts = []
    #print "p[0]:", points[0]
    #print "p[1]:", points[1]
    #print "roi[0]:", roi[0]
    #print "roi[1]:", roi[1]

    
    row = roi[0]
    col = roi[1]
    #print "roi x:", row
    #print "roi y:", col
    data = np.ones(len(row))
    myRoi = coo_matrix((data, (row,col)), shape=(1000,1000))
    
    if len(pointsX) == []:
        pass # empty list
    else:
        row = pointsX
        col = pointsY
        data = np.ones(len(row))
        #print "pts x:", row
        #print "pts y:", col
        myPts = coo_matrix((data, (row,col)), shape=(1000,1000))

        result = np.multiply(myRoi.todense(),myPts.todense())
        pts = np.where(result==1)
        #print "num pts: ", len(row)
        #print "result pts", np.sum(result)
        
    return pts

def rotate2d(pointsX,pointsY,angleCCW):
    rotX = np.empty([0])
    rotY = np.empty([0])
    for i in range(len(pointsX)):
                rotX = np.cos(float(angleCCW))*pointsX - np.sin(float(angleCCW))*pointsY
                rotY = np.sin(float(angleCCW))*pointsX + np.cos(float(angleCCW))*pointsY
    return rotX, rotY

def fft2(image):
    """
    fft2 returns shifted fft of image
    """
    from scipy import fftpack
    F1 = fftpack.fft2(image)
    F1 = fftpack.fftshift( F1 )
    return F1

def powerSpectrum(image):
    """
    powerSpectrum returns the power spectrum of the image
    """
    F1 = fft2(image)
    F1 = np.abs(F1)**2
    return F1

def powder2D(data):
    """
    POWDER2D obtains a 2D average of a 3D stack of diffraction patterns which
    corresponds to a virtual powder plot.
    NOTE: It is assumed that the patterns are stored with the first index as the
    column, the second as the row index and the last index as the pattern index.
    """
    if data.ndim == 3:
        return data.mean(axis=2)
    else:
        print "Input of POWDER2D has to be of dimension 3."

def azimuthalAverage(image, center=None, stddev=False, returnradii=False, return_nr=False, 
        binsize=0.5, weights=None, steps=False, interpnan=False, left=None, right=None):
    """
    Calculate the azimuthally averaged radial profile.

    image - The 2D image
    center - The [x,y] pixel coordinates used as the center. The default is 
             None, which then uses the center of the image (including 
             fractional pixels).
    stddev - if specified, return the azimuthal standard deviation instead of the average
    returnradii - if specified, return (radii_array,radial_profile)
    return_nr   - if specified, return number of pixels per radius *and* radius
    binsize - size of the averaging bin.  Can lead to strange results if
        non-binsize factors are used to specify the center and the binsize is
        too large
    weights - can do a weighted average instead of a simple average if this keyword parameter
        is set.  weights.shape must = image.shape.  weighted stddev is undefined, so don't
        set weights and stddev.
    steps - if specified, will return a double-length bin array and radial
        profile so you can plot a step-form radial profile (which more accurately
        represents what's going on)
    interpnan - Interpolate over NAN values, i.e. bins where there is no data?
        left,right - passed to interpnan; they set the extrapolated values

    If a bin contains NO DATA, it will have a NAN value because of the
    divide-by-sum-of-weights component.  I think this is a useful way to denote
    lack of data, but users let me know if an alternative is prefered...
    
    """
    # Calculate the indices from the image
    y, x = np.indices(image.shape)

    if center is None:
        center = np.array([(x.max()-x.min())/2.0, (y.max()-y.min())/2.0])

    r = np.hypot(x - center[0], y - center[1])

    if weights is None:
        weights = np.ones(image.shape)
    elif stddev:
        raise ValueError("Weighted standard deviation is not defined.")

    # the 'bins' as initially defined are lower/upper bounds for each bin
    # so that values will be in [lower,upper)  
    nbins = (np.round(r.max() / binsize)+1)
    maxbin = nbins * binsize
    bins = np.linspace(0,maxbin,nbins+1)
    # but we're probably more interested in the bin centers than their left or right sides...
    bin_centers = (bins[1:]+bins[:-1])/2.0

    # Find out which radial bin each point in the map belongs to
    whichbin = np.digitize(r.flat,bins)

    # how many per bin (i.e., histogram)?
    # there are never any in bin 0, because the lowest index returned by digitize is 1
    nr = np.bincount(whichbin)[1:]

    # recall that bins are from 1 to nbins (which is expressed in array terms by arange(nbins)+1 or xrange(1,nbins+1) )
    # radial_prof.shape = bin_centers.shape
    if stddev:
        radial_prof = np.array([image.flat[whichbin==b].std() for b in xrange(1,nbins+1)])
    else:
        radial_prof = np.array([(image*weights).flat[whichbin==b].sum() / weights.flat[whichbin==b].sum() for b in xrange(1,int(nbins+1))])

    #import pdb; pdb.set_trace()

    if interpnan:
        radial_prof = np.interp(bin_centers,bin_centers[radial_prof==radial_prof],radial_prof[radial_prof==radial_prof],left=left,right=right)

    if steps:
        xarr = np.array(zip(bins[:-1],bins[1:])).ravel() 
        yarr = np.array(zip(radial_prof,radial_prof)).ravel() 
        return xarr,yarr
    elif returnradii: 
        return bin_centers,radial_prof
    elif return_nr:
        return nr,bin_centers,radial_prof
    else:
        return radial_prof

def qSum(X,N,M,R,r):
    """
    Calculate the sum of intensities at a certain q-range.

    X - Image stack # shape (n_samples, n_features)
    R,r - Outer and inner radii defining the q-range
    N,M - Height,Width of image in number of pixels
    """
    # Apply Mask
    intArea = donutMask(N,M,R,r)
    intAreaL = np.reshape(intArea,(1,-1))
    indexPixel=np.where(intAreaL==0) # grab pixels
    X = X[:,indexPixel[1]]
    mySum = np.sum(X,axis=1)
    return mySum

def qFilter(X,thresh=1):
    """
    Filter hits based on threshold of certain q-range.

    X - array of intensity sums # shape (n_samples)
    thresh - threshold value    
    """
    hits = X > thresh
    return hits

def showSubset(myData,N,M,label=None,criteria=None,subDim=5,title='',vmin=0,vmax=0,wspace=0.001,hspace=0.1,rand=True):
    """
    Show subset of data.

    myData - intensity data # shape (n_samples, n_features)
    N,M - height and width of each image in myData
    label - labels of myData # shape (n_samples)
    criteria - one of the values in label array
    thresh - threshold value    
    """
    if label == None:
        numData = myData.shape[0]
        label = np.zeros(numData)
        criteria = 0
    labelInd = np.where(label == criteria)

    if rand == True:
        labelInd = np.random.permutation(labelInd[0]) # random subset
    else:
        labelInd = labelInd[0]

    plt.figure()
    fig = plt.gcf()
    fig.suptitle(title, fontsize=14)
    ind = 1
    counter = 0
    for i in range(subDim):
        for j in range(subDim):
            if counter < len(labelInd):
                plt.subplot(subDim,subDim,ind)
                img = np.reshape(myData[labelInd[counter],:], (N,M))
                imgplot = plt.imshow(img)
                if vmax > 0:
                    imgplot.set_clim(vmin,vmax)
                plt.title(labelInd[counter])
                plt.axis('off')
                ind += 1
                counter += 1
    plt.subplots_adjust(wspace=wspace,hspace=hspace)

def showCluster2D(spectral,y_pred,s=15):
    """
    showCluster2D takes the spectral object and predicted label
    then displays a 2D scatter plot.
    """
    plt.figure()
    for i in set(y_pred):
        labelInd = np.where(y_pred == i)
        clusterLabel = 'cluster %s' % i
        plt.scatter(spectral.maps_[labelInd, 0], spectral.maps_[labelInd, 1], color=colors[i].tolist(), s=s,
                    marker=str(markers[i].tolist()), label=clusterLabel)

def showCluster3D(spectral,y_pred):
    """
    showCluster3D takes the spectral object and predicted label
    then displays a 3D scatter plot.
    """
    from mpl_toolkits.mplot3d import Axes3D
    plt.figure()
    ax = plt.subplot(111, projection='3d')
    for i in set(y_pred):
        labelInd = np.where(y_pred == i)
        clusterLabel = 'cluster %s' % i
        ax.plot(np.squeeze(spectral.maps_[labelInd, 0]), np.squeeze(spectral.maps_[labelInd, 1]), np.squeeze(spectral.maps_[labelInd, 2]),
                str(markers[i].tolist()), color=colors[i], label=clusterLabel)
    plt.legend(loc='upper left', numpoints=2, ncol=3, bbox_to_anchor=(0, 0))

def diffusionKernel(X,eps,knn,D=None):
    from sklearn.neighbors import NearestNeighbors
    nbrs = NearestNeighbors(n_neighbors=knn, algorithm='ball_tree').fit(X)
    #if D == None:
    X = normr(X)    
    D=nbrs.kneighbors_graph(X,mode='distance')
    term = D.multiply(D)/-eps
    G = np.exp(term.toarray())
    G[np.where(G==1)]=0
    G = G + np.eye(G.shape[0])
    deg = np.sum(G,axis=0)
    term1 = G/deg
    P = np.matrix(term1)
    return P,D

def labelSpread(P,labels):
    # value 0 is reserved for unlabeled images
    sparseInd = np.argwhere(labels != 0)
    diffusion = P[sparseInd,:]
    diffusion = np.squeeze(diffusion) # was not needed for .mat data
    newLabels = np.argmax(diffusion,axis=0)
    newLabels = newLabels.T # was not needed for .mat data
    spreadLabels = np.squeeze(labels[sparseInd[newLabels]])
    return spreadLabels

def reci2real(dist,detector_pos,E=5.5):
    """
    RECI2REAL takes the a distance DIST on the detector at position DETECTOR_POS
    ('f' or 'b' for "front" or "back") and returns the correpsponding real space
    HALF PERIOD distance (which is relevant for a speckle). 
    E is the photon energy in keV with a standard value of
    5.5. The result is given in units of nm.
    """
    # sample-detector distances
    z_f = 1.321
    z_b = 2.8615
    # detector pixel size
    Delta_X = 50.e-6
    lambda_xray = 12.398/E*1.e-10

    if detector_pos == 'f':
        s = round((lambda_xray*z_f)/(Delta_X*dist)*1e9,2)
        print "Object size is %5.1f nm." %s
        return s
    elif detector_pos == 'b':
        s = round((lambda_xray*z_b)/(Delta_X*dist)*1e9,2)
        print "Object size is %5.1f nm." %s
        return s
    else: 
        print "Detector position has to be 'f' (front) or 'b' (back)."





