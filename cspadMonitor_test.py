#!/usr/bin/env python
"""
cspadMonitor:
A GUI for viewing/labeling diffraction patterns from xtc.

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.

UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pprint
import random
import wx
import sys
import h5py
import numpy as np
from scipy import sparse
import sacla as sc
import blackbox as bx
import thread
import zmq
import pickle
import cspad_ij_to_xy as geom

import threading

# The recommended way to use wx with mpl is with the WXAgg
# backend. 
#
import matplotlib as mpl
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
#mpl.use('WXAgg')
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

#---------------------------------------------------------------------------

# This is how you pre-establish a file filter so that the dialog
# only shows the extension(s) you want it to.
wildcard = "All files (*.*)|*.*|" \
           "Hierarchical Data Format (*.h5)|*.h5|"     \
           "Python source (*.py)|*.py|"     \
           "Compiled Python (*.pyc)|*.pyc|" \
           "Binary dat (*.dat)|*.dat"
           

#---------------------------------------------------------------------------

class mainFrame_params(wx.Frame):
    title = "LCLS CsPAD Monitor"
    det_numPixX = det_numPixY = 1750 # Hard coded for CSPAD
    numSensors = 8 # Hard coded for SACLA MPCCD front detector
    px = 512 # Hard coded for SACLA MPCCD front detector
    py = 1024 # Hard coded for SACLA MPCCD front detector
    numDP = 0 # Number of diffraction patterns loaded
    preassem = 0 # Default: load DataConvert3 preassembled detector geometry
    display_photons = 0 # Default: Display True False photon peaks
    hasAssemData = 0 # Preassembled data available
    hasUnassemData = 0 # Unassembled data available
    hasFront = 0 # Front detector data available
    hasBack = 0 # Back detector data available
    front = 1
    cmax = 0
    cmin = 0
    flags = wx.ALIGN_LEFT | wx.ALL | wx.ALIGN_CENTER_VERTICAL
    # my labels
    myLabel = []
    spreadLabels = []
    spreadLabel = []
    labels = {'miss':-1, 'no label':0, 'single':1, 'double':2, 'many':3}
    haveDark = False
    haveImage = False
    haveCM = False
    haveManifold = False
    calcD = False
    unagi = False
    P = D = [] # Propagation and distance
    # Front detector
    img = np.zeros((1,1))
    det = np.zeros((1,1))
    darkDet = np.zeros((1,1))
    dataStack = []
    appPixPosX = 0
    appPixPosY = 0
    # Back detector
    appPixPosXBack = 0
    appPixPosYBack = 0
    det_numPixXBack = 1200  # Hard coded for SACLA MPCCD back detector
    det_numPixYBack = 1100  # Hard coded for SACLA MPCCD back detector
    numSensorsBack = 2  # Hard coded for SACLA MPCCD back detector
    haveDarkBack = False
    haveImageBack = False
    haveCMBack = False
    imgBack = np.zeros((1,1))
    detBack = np.zeros((1,1))
    darkDetBack = np.zeros((1,1))
    eps = 10
    knn = 30


class mainFrame(mainFrame_params):
    """ The main frame of the application
    """
    def __init__(self, rec_ip, rec_port):
        self.init_zmq(rec_ip, rec_port)
        wx.Frame.__init__(self, None, wx.ID_ANY, self.title)
  
        # Display variables
        self.figure_event_id = None
        self.img_raw = None
        self.img     = None
        self.photons_list = [0]
        self.photons_list_cu = [0]

        # GUI Data 
        self.zmq_data = {}
        self.integration_depth = 3
        self.integration_depth_counter = 0
        self.integration_depth_counter_raw = 0
        self.cspad_photons = np.zeros((4, 8, 185, 388), dtype=np.int16)
        self.cspad_raw = np.zeros((4, 8, 185, 388), dtype=np.int64)
        self.cspad_photons_total = np.zeros((4, 8, 185, 388), dtype=np.int64)
        self.geom_fnam = '/home/amorgan/Downloads/cspad-cxia2514-taw1.geom'

        self.thread_lock = threading.Lock()

        self.create_menu()
        self.create_status_bar()
        self.create_main_panel()
        if self.liveCheckbox.GetValue() == False :
            self.draw_figure()
 
        # thread that checks for incomming data via zmq
        self.thread_pub = threading.Thread(target=self.zmq_sub_function)
        self.thread_pub.daemon=True
        self.thread_pub.start()

        # thread that periodically redraws the figures
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, lambda x: self.draw_figure_timer(x), self.timer)

    def draw_figure_timer(self, event):
        self.draw_figure()

    def draw_figure(self):
        """ Redraws the figure

        Get input from buttons then display data.
        Display the raw cspad data when self.img_raw != None and the raw button has been pressed.
        """        
        # vmin and vmax 
        caxisMin = self.caxisMinTextbox.GetValue()
        caxisMax = self.caxisMaxTextbox.GetValue()
        
        # Check sensible input
        vmin = vmax = None
        if not (caxisMin == '' or caxisMax == ''):
            if float(caxisMin) < float(caxisMax) :
                vmin = float(caxisMin)
                vmax = float(caxisMax)
        
        self.thread_lock.acquire()
        if self.display_photons and self.img != None :
            # draw the cspad photons
            self.ax.cla()
            self.ax.imshow(self.img, interpolation='none',cmap='Greys_r')
            self.ax.set_title('CSpad photons')

        elif self.img_raw != None :
            # draw the cspad raw data 
            if vmin == None and vmax == None :
                vmin = self.img_raw[np.where(self.img_raw != 0)].min()
            self.ax.cla()
            self.ax.imshow(self.img_raw, cmap='Greys_r', vmin = vmin, vmax = vmax)
            title = 'CSpad (min, max): (' + str(self.img_raw.min()) + ',' + str(self.img_raw.max()) + ')'
            self.ax.set_title(title)
            plt.draw()
        
        #
        # plot the current photon counts
        self.ax1.cla()
        self.ax1.plot(self.photons_list, 'k', linewidth=2, alpha = 0.7)
        self.ax1.set_xlabel('frame no.')
        self.ax1.set_ylabel('photon counts')
        #
        # plot the cumulative photon counts
        ax2 = self.ax1.twinx()
        ax2.cla()
        ax2.plot(self.photons_list_cu, 'g', linewidth=3, alpha = 0.7)
        ax2.set_ylabel('cumulative photon counts', color='g')
        ax2.set_title('Average photon counts: ' + str(self.photons_list_cu[-1] / float(len(self.photons_list_cu))))
        #
        self.canvas.draw()
        self.thread_lock.release()

    def init_zmq(self, rec_ip, rec_port):
        self.zmq_context = zmq.Context()
        self.zmq_subscribe = self.zmq_context.socket(zmq.SUB)
        print ('Listening at tcp://%s:%d' % (rec_ip, rec_port))
        self.zmq_subscribe.connect('tcp://%s:%d' % (rec_ip, rec_port))
        self.zmq_subscribe.setsockopt_string(zmq.SUBSCRIBE, u'cspaddata')

    def zmq_sub_function(self):
        while True:
            self.thread_lock.acquire()
            live = self.liveCheckbox.GetValue() 
            self.thread_lock.release()
            
            if live == True :
                header, tag, msg = self.zmq_subscribe.recv_multipart()
                # get the incomming data
                self.zmq_data = pickle.loads(msg)
                
                # process zmq_data
                photons     = None
                photons_tot = None
                if self.zmq_data.has_key('ijkl_peaks') :
                    if self.integration_depth_counter == self.integration_depth :
                        img = geom.apply_geom(self.geom_fnam, self.cspad_photons)
                        self.cspad_photons_total += self.cspad_photons
                        
                        # count the photons
                        photons     = np.sum(self.cspad_photons)
                        photons_tot = np.sum(self.cspad_photons_total)
                        
                        self.cspad_photons.fill(0)
                        self.cspad_photons[self.zmq_data['ijkl_peaks']] = 1
                        self.integration_depth_counter = 0

                    else :
                        self.cspad_photons[self.zmq_data['ijkl_peaks']] += 1
                        self.integration_depth_counter += 1
                        img = None

                if self.zmq_data.has_key('raw_data') :
                    if self.integration_depth_counter_raw == self.integration_depth :
                        img_raw = geom.apply_geom(self.geom_fnam, self.cspad_raw)
                        self.cspad_raw = self.zmq_data['raw_data']
                        self.integration_depth_counter_raw = 0
                    else :
                        self.cspad_raw += self.zmq_data['raw_data']
                        self.integration_depth_counter_raw += 1
                        img_raw = None
                    #

                # put into display variables
                self.thread_lock.acquire()
                self.img = None
                self.img_raw = None

                if self.zmq_data.has_key('ijkl_peaks') :
                    self.img = img
                    if photons != None :
                        self.photons_list.append(photons)
                        self.photons_list_cu.append(photons_tot)

                if self.zmq_data.has_key('raw_data') :
                    self.img_raw = img_raw

                self.thread_lock.release()

    def create_menu(self):
        self.menubar = wx.MenuBar()
        
        menu_file = wx.Menu()
        m_expt = menu_file.Append(-1, "&Save plot\tCtrl-S", "Save plot to file")
        self.Bind(wx.EVT_MENU, self.on_save_plot, m_expt)
        menu_file.AppendSeparator()
        m_exit = menu_file.Append(-1, "E&xit\tCtrl-X", "Exit")
        self.Bind(wx.EVT_MENU, self.on_exit, m_exit)
        
        menu_help = wx.Menu()
        m_about = menu_help.Append(-1, "&About\tF1", "About the demo")
        self.Bind(wx.EVT_MENU, self.on_about, m_about)
        
        self.menubar.Append(menu_file, "&File")
        self.menubar.Append(menu_help, "&Help")
        self.SetMenuBar(self.menubar)

    def create_main_panel(self):
        """ Creates the main panel with all the controls on it:
             * mpl canvas 
             * mpl navigation toolbar
             * Control panel for interaction
        """
        self.panel = wx.Panel(self)
        
        # Create the mpl Figure and FigCanvas objects. 
        # 8x8 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig, (self.ax, self.ax1) = plt.subplots(1, 2, figsize=(16.0, 8.0),dpi=self.dpi) # 2x1 subplot
        self.canvas = FigCanvas(self.panel, -1, self.fig)

        #--------------------- hbox1
        # Back
        self.backbutton = wx.Button(self.panel, -1, "Back")
        self.Bind(wx.EVT_BUTTON, self.on_back_button, self.backbutton)
        
        # Next
        self.nextbutton = wx.Button(self.panel, -1, "Next")
        self.Bind(wx.EVT_BUTTON, self.on_next_button, self.nextbutton)
        
        # Image number
        self.imageNumber_label = wx.StaticText(self.panel, -1, 
            "Image number: ") 
        self.imageNumberTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_imageNumber_enter, self.imageNumberTextbox)
        self.tag = 0
        self.imageNumberTextbox.SetValue(str(self.tag))
        self.numDP_label = wx.StaticText(self.panel, -1, 
            "num. images: %s" % self.numDP)
        
        # Load assembled
        self.assemCheckbox = wx.CheckBox(self.panel, -1, 
            "Load assembled",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_assemcheckbox_grid, self.assemCheckbox)
        
        # Display photons 
        self.display_photons_CheckBox = wx.CheckBox(self.panel, -1, 
            "Display cspad Photons",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_display_photons, self.display_photons_CheckBox)

        #--------------------- hbox2
        self.caxis_label = wx.StaticText(self.panel, -1, 
            "caxis (min,max): ")        
        self.caxisMinTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_caxisMin_enter, self.caxisMinTextbox)
        self.caxisMaxTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_caxisMax_enter, self.caxisMaxTextbox)
        # hbox3
        self.centre_label = wx.StaticText(self.panel, -1, 
            "detector centre (x,y): ") 
        self.centreXTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_centreX_enter, self.centreXTextbox)
        self.centreYTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_centreY_enter, self.centreYTextbox)
        self.width_label = wx.StaticText(self.panel, -1, 
            "width (x,y): ") 
        self.widthXTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_widthX_enter, self.widthXTextbox)
        self.widthYTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_widthY_enter, self.widthYTextbox)

        # integration depth
        self.integ_label = wx.StaticText(self.panel, -1, 
            "integration depth: ") 
        self.integ_Textbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_integ_enter, self.integ_Textbox)

        self.save = wx.Button(self.panel, -1, "Save label")
        self.Bind(wx.EVT_BUTTON, self.OnSave, self.save)

        self.load = wx.Button(self.panel, -1, "Load label")
        self.Bind(wx.EVT_BUTTON, self.OnLoad, self.load)

        self.openConfig = wx.Button(self.panel, -1, "Open psana config")
        self.Bind(wx.EVT_BUTTON, self.OnOpenConfig, self.openConfig)

        self.liveCheckbox = wx.CheckBox(self.panel, -1, 
            "live stream",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_liveCheckbox_enter, self.liveCheckbox)

        # Create the navigation toolbar, tied to the canvas
        #
        self.toolbar = NavigationToolbar(self.canvas)
        
        #
        # Layout with box sizers
        #

        # hbox1
        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.Add(self.backbutton, 0, border=3, flag=self.flags)
        self.hbox1.Add(self.nextbutton, 0, border=3, flag=self.flags)
        self.hbox1.Add(self.imageNumber_label, 0, border=3, flag=self.flags)
        self.hbox1.Add(self.imageNumberTextbox, 0, border=3, flag=self.flags)
        self.hbox1.Add(self.numDP_label, 0, flag=self.flags)
        self.hbox1.AddSpacer(20)
        self.hbox1.Add(self.assemCheckbox, 0, border=3, flag=self.flags)
        self.assemCheckbox.SetValue(self.preassem)
        self.hbox1.AddSpacer(20)
        self.hbox1.Add(self.display_photons_CheckBox, 0, border=3, flag=self.flags)
        self.display_photons_CheckBox.SetValue(self.preassem)
        
        # hbox2
        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.Add(self.caxis_label, 0, flag=self.flags)
        self.hbox2.AddSpacer(30)
        self.hbox2.Add(self.caxisMinTextbox, 0, border=3, flag=self.flags)
        self.hbox2.Add(self.caxisMaxTextbox, 0, border=3, flag=self.flags)

        # hbox3
        self.hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox3.Add(self.centre_label, 0, flag=self.flags)
        self.hbox3.Add(self.centreXTextbox, 0, border=3, flag=self.flags)
        self.hbox3.Add(self.centreYTextbox, 0, border=3, flag=self.flags)

        # hbox4
        self.hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox4.Add(self.width_label, 0, flag=self.flags)
        self.hbox4.AddSpacer(65)
        self.hbox4.Add(self.widthXTextbox, 0, border=3, flag=self.flags)
        self.hbox4.Add(self.widthYTextbox, 0, border=3, flag=self.flags)
        self.hbox4.AddSpacer(30)
        self.hbox4.Add(self.integ_label, 0, flag=self.flags)
        self.hbox4.AddSpacer(10)
        self.hbox4.Add(self.integ_Textbox, 0, border=3, flag=self.flags)

        # hbox6
        self.hbox6 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox6.Add(self.openConfig, 0, border=3, flag=self.flags)
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.save, 0, border=3, flag=self.flags)        
        self.hbox6.Add(self.load, 0, border=3, flag=self.flags) 
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.liveCheckbox, 0, border=3, flag=self.flags) 
        
        # vbox
        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)
        self.vbox.AddSpacer(10)
        self.vbox.Add(self.hbox1, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox2, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox3, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox4, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox6, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        
        self.panel.SetSizer(self.vbox)
        self.vbox.Fit(self)

    def OnOpenConfig(self, evt):
        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'open' dialog, and allows multitple
        # file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, this
        # dialog is set up to change the current working directory to the path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            # This returns a Python list of files that were selected.
            self.configFilename = dlg.GetPath()

        # Destroy the dialog. Don not do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()
        
    def OnSave(self, evt):
        #self.log.WriteText("CWD: %s\n" % os.getcwd())

        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'save' dialog.
        #
        # Unlike the 'open dialog' example found elsewhere, this example does NOT
        # force the current working directory to change if the user chooses a different
        # directory than the one initially set.
        dlg = wx.FileDialog(
            self, message="Save file as ...", defaultDir=os.getcwd(), 
            defaultFile="", wildcard=wildcard, style=wx.SAVE
            )

        # This sets the default filter that the user will initially see. Otherwise,
        # the first filter in the list will be used by default.
        dlg.SetFilterIndex(3)

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()

            if self.unagi:
                np.savetxt(path,self.spreadLabel,fmt='%d')
            else:
                np.savetxt(path,self.myLabel,fmt='%d')

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def OnLoad(self, evt):
        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'open' dialog, and allows multitple
        # file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, this
        # dialog is set up to change the current working directory to the path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # This sets the default filter that the user will initially see. Otherwise,
        # the first filter in the list will be used by default.
        dlg.SetFilterIndex(3)

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()

            self.myLabel = np.loadtxt(path)

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def on_back_button(self, event):
        self.flash_status_message('Non functional ...')

    def on_next_button(self, event):
        self.myLabel.append(self.labels['no label'])
        self.flash_status_message('Drawing image ...')
        # update tag value
        #if self.tag+1 <= self.numDP-1: # arbitrary limit
        self.tag = self.tag+1
        #else:
        #    self.tag = self.numDP-1
        # update textbox value
        self.imageNumberTextbox.SetValue(str(self.tag))
        if self.liveCheckbox.GetValue() == False :
            self.draw_figure()

    def on_imageNumber_enter(self, event):
        self.flash_status_message('Non functional ...')

    def on_assemcheckbox_grid(self, event):
        self.flash_status_message('Load already assembled images ...')
        self.preassem = self.assemCheckbox.GetValue()
        if self.liveCheckbox.GetValue() == False :
            self.draw_figure()

    def on_display_photons(self, event):
        self.display_photons = self.display_photons_CheckBox.GetValue()
        if self.display_photons :
            self.flash_status_message('Displaying photon peaks ...')
        else :
            self.flash_status_message('Displaying raw data ...')
        if self.liveCheckbox.GetValue() == False :
            self.draw_figure()
        
    def on_liveCheckbox_enter(self, event):
        if self.liveCheckbox.GetValue() == True:
            self.flash_status_message('Starting timer ...')
            self.timer.Start(1000)
        else: 
            self.flash_status_message('Stopping timer ...')
            self.timer.Stop()

    def on_caxisMin_enter(self, event):
        if self.liveCheckbox.GetValue() == False :
            self.flash_status_message('Drawing image ...')
            self.draw_figure()

    def on_caxisMax_enter(self, event):
        if self.liveCheckbox.GetValue() == False :
            self.flash_status_message('Drawing image ...')
            self.draw_figure()      

    def on_centreX_enter(self, event):
        if self.liveCheckbox.GetValue() == False :
            self.flash_status_message('Drawing image ...')
            self.draw_figure()

    def on_centreY_enter(self, event):
        if self.liveCheckbox.GetValue() == False :
            self.flash_status_message('Drawing image ...')
            self.draw_figure()

    def on_widthX_enter(self, event):
        if self.liveCheckbox.GetValue() == False :
            self.flash_status_message('Drawing image ...')
            self.draw_figure()

    def on_widthY_enter(self, event):
        if self.liveCheckbox.GetValue() == False :
            self.flash_status_message('Drawing image ...')
            self.draw_figure()

    def on_integ_enter(self, event):
        dep = self.integ_Textbox.GetValue() 
        try :
            dep = int(dep)
            self.flash_status_message('Updating integration depth ' + str(dep))
            self.integration_depth = dep
            self.integration_depth_counter = 0
            self.integration_depth_counter_raw = 0
        except :
            self.flash_status_message('Error: integration depth must be an int')
 
    def create_status_bar(self):
        self.statusbar = self.CreateStatusBar()

    def on_save_plot(self, event):
        file_choices = "PNG (*.png)|*.png"
        
        dlg = wx.FileDialog(
            self, 
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="dp.png",
            wildcard=file_choices,
            style=wx.SAVE)
        
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)
        
    def on_exit(self, event):
        self.Destroy()
        
    def on_about(self, event):
        msg = """ A GUI for viewing/labeling diffraction patterns.
        """
        dlg = wx.MessageDialog(self, msg, "About", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
    
    def flash_status_message(self, msg, flash_len_ms=500):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(
            wx.EVT_TIMER, 
            self.on_flash_status_off, 
            self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)
    
    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')


class MySplashScreen(wx.SplashScreen):
    def __init__(self, rec_ip, rec_port):
        bmp = wx.Image(opj("bitmaps/unagi.png")).ConvertToBitmap()
        wx.SplashScreen.__init__(self, bmp,
                                 wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT,
                                 3000, None, -1)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.fc = wx.FutureCall(1000, self.ShowMain)


    def OnClose(self, evt):
        # Make sure the default handler runs too so this window gets
        # destroyed
        evt.Skip()
        self.Hide()
        
        # if the timer is still running then go ahead and show the
        # main frame now
        if self.fc.IsRunning():
            self.fc.Stop()
            self.ShowMain()


    def ShowMain(self):
        frame = mainFrame(rec_ip, rec_port)
        frame.Show()


def opj(path):
    """Convert paths to the platform-specific separator"""
    st = apply(os.path.join, tuple(path.split('/')))
    # HACK: on Linux, a leading / gets lost...
    if path.startswith('/'):
        st = '/' + st
    return st


if __name__ == '__main__':
    rec_ip = sys.argv[1]
    rec_port = int(sys.argv[2])
    app = wx.App()
    splash = MySplashScreen(rec_ip, rec_port)
    splash.Show()
    app.MainLoop()
