#!/usr/bin/env python
"""
vmiViewer:
A GUI for monitoring Velocity-Map-Imaging (VMI).

Unagi is a Japanese word for 'total state of awareness'
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.

UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import time
import datetime
import math
import numpy 
import scipy.ndimage
import zmq
import pickle
import signal
import pyqtgraph
import pyqtgraph.exporters
import collections
import PyQt4.QtGui
import PyQt4.QtCore
from scipy.sparse import coo_matrix
import copy
import os

class opal:
    det_numPix = 0 # pixel dimension
    det_cx = 0 # centre pixel
    numSensors = 1
    cold = 0
    hot = 0
    data = numpy.zeros((1,1))        # camera data

    def __init__(self, name):
        self.name = name
        if name == 'opal1k':
            self.det_numPix = 1024
            self.det_cx = 512
        elif name == 'opal2k':
            self.det_numPix = 2000
            self.det_cx = 1000
        elif name == 'opal4k':
            self.det_numPix = 4000
            self.det_cx = 2000
        else:
            print 'This detector does not exist %s' % name
    
    def displayName(self):
        print self.name


class ZMQListener(PyQt4.QtCore.QObject):

    zmqmessage = PyQt4.QtCore.pyqtSignal(int, dict)
    zmqmessage_monitorparams = PyQt4.QtCore.pyqtSignal(dict)
    
    def __init__(self, curr_rec_ip, curr_rec_port, det_numPix):
        
        PyQt4.QtCore.QObject.__init__(self)

        self.rec_ip = curr_rec_ip
        self.rec_port = curr_rec_port
        self.det_numPix  = det_numPix
        self.zmq_context = zmq.Context()

        print ('Listening at tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe = self.zmq_context.socket(zmq.SUB)
        self.zmq_subscribe.set_hwm(1)
        self.zmq_subscribe.connect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe.setsockopt_string(zmq.SUBSCRIBE, u'vmidata')


        self.zmq_subscribe_monitorparams = self.zmq_context.socket(zmq.SUB)
        self.zmq_subscribe_monitorparams.connect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe_monitorparams.setsockopt_string(zmq.SUBSCRIBE, u'monitorparams')

        self.zmq_poller = zmq.Poller()
        self.zmq_poller.register(self.zmq_subscribe, zmq.POLLIN)
        self.zmq_poller.register(self.zmq_subscribe_monitorparams, zmq.POLLIN)

        self.gui_integration_size = 1

        self.accumulator = {}
        for tag in range(0,8):
            self.accumulator[tag] = {}
            self.accumulator[tag]['raw_data'] = numpy.zeros((self.det_numPix, self.det_numPix))
            self.accumulator[tag]['x_peaks'] = []
            self.accumulator[tag]['y_peaks'] = []
            self.accumulator[tag]['frame_counter'] = 0
            self.accumulator[tag]['extra_stuff'] = None

        self.listening_timer = PyQt4.QtCore.QTimer()
        self.listening_timer.timeout.connect(self.listen)


    def update_gui_integration_size(self, size):
        print 'GUI integration size updated'
        self.gui_integration_size = size
        for tag in range(0,8):
            self.accumulator[tag] = {}
            self.accumulator[tag]['raw_data'] = numpy.zeros((self.det_numPix, self.det_numPix))
            self.accumulator[tag]['x_peaks'] = []
            self.accumulator[tag]['y_peaks'] = []
            self.accumulator[tag]['frame_counter'] = 0


    def start_listening(self):
        self.listening_timer.start()


    def stop_listening(self):
        self.listening_timer.stop()
        print ('Disconnecting from tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe.disconnect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe_monitorparams.disconnect('tcp://%s:%d' % (self.rec_ip, self.rec_port))


    def listen(self):
        socks = dict(self.zmq_poller.poll(0))
        if self.zmq_subscribe in socks and socks[self.zmq_subscribe] == zmq.POLLIN:
            header, tag, msg = self.zmq_subscribe.recv_multipart()
            zmq_dict = pickle.loads(msg)
            recv_tag =int(tag)
            self.accumulator[recv_tag]['x_peaks'].extend(zmq_dict['x_peaks'])
            self.accumulator[recv_tag]['y_peaks'].extend(zmq_dict['y_peaks'])
            if 'raw_data' in zmq_dict.keys():
                self.accumulator[recv_tag]['raw_data'] += zmq_dict['raw_data']
            self.accumulator[recv_tag]['frame_counter'] += 1
            self.accumulator[recv_tag]['extra_stuff'] = zmq_dict['extra_stuff']
            if self.accumulator[recv_tag]['frame_counter'] == self.gui_integration_size:
                self.accumulator[recv_tag]['raw_data'] /= self.accumulator[recv_tag]['frame_counter']
                self.zmqmessage.emit(recv_tag, self.accumulator[recv_tag])
                self.accumulator[recv_tag]['raw_data'] = numpy.zeros((self.det_numPix, self.det_numPix))
                self.accumulator[recv_tag]['x_peaks'] = []
                self.accumulator[recv_tag]['y_peaks'] = []
                self.accumulator[recv_tag]['frame_counter'] = 0
                self.accumulator[recv_tag]['extra_stuff'] = None


        if self.zmq_subscribe_monitorparams in socks and socks[self.zmq_subscribe_monitorparams] == zmq.POLLIN:
            header, msg = self.zmq_subscribe_monitorparams.recv_multipart()
            params_dict = pickle.loads(msg)
            self.zmqmessage_monitorparams.emit(params_dict)


class MainFrame(PyQt4.QtGui.QWidget):
    """
    The main frame of the application
    """
    gui_integration_size_change = PyQt4.QtCore.pyqtSignal(int)
    listening_thread_start_processing = PyQt4.QtCore.pyqtSignal()
    listening_thread_stop_processing = PyQt4.QtCore.pyqtSignal()

    def __init__(self, arg_list):
        super(MainFrame, self).__init__()
        
        self.title = 'LCLS VMI Monitor'
        myOpal = opal('opal1k')
        self.det_numPix = myOpal.det_numPix
        self.det_cx = myOpal.det_cx
        self.img = numpy.zeros((self.det_numPix,self.det_numPix))
        self.roi = numpy.zeros((self.det_numPix,self.det_numPix))
        self.roiInd = ([],[])
        
        self.xInd = numpy.empty([1])
        self.yInd = numpy.empty([1])

        self.rec_ip = '127.0.0.1'
        self.rec_port = 14747
        self.snd_ip = None
        self.snd_port = None

        if len(arg_list) == 3:
            self.rec_ip = str(arg_list[1])
            self.rec_port = int(arg_list[2])

        self.costheta_data = collections.deque()
        self.costheta_history = collections.deque()
        self.costheta_history_size = 1000
        self.running_average_data = collections.deque()
        self.running_average_size = 10
        self.running_average_history = collections.deque()
        self.running_average_history_size = 1000

        self.backgroundnpeaks = 0
        self.backgroundavgcostheta = 0.0
  
        for c in range(0,self.running_average_size):
            self.running_average_data.append(0)

        for c in range(0,self.running_average_size):
            self.costheta_data.append(0)

        self.livestream = False
        self.integration_size = 1
        self.gui_integration_size = 1
        self.peak_threshold = 100
        self.cenX = self.det_cx
        self.cenY = self.det_cx
        self.rot = 0
        self.c = 1.2 # constant
        self.tof = 5e-6 # time of flight
        self.velroimin = 50
        self.velroimax = 170
        self.angleroimin = 0
        self.angleroimax = 180
        self.radii = str(self.velroimin)+','+str(self.velroimax)
        self.col = numpy.array([0,0,0])
        self.tag = 0
        self.data = {}
        for tag in range(0,8):
            self.data[tag] = {}
            self.data[tag]['raw_data'] = numpy.zeros((self.det_numPix,self.det_numPix))
            self.data[tag]['x_peaks'] = []
            self.data[tag]['y_peaks'] = []
            self.data[tag]['extra_stuff'] = None

        self.local_data = self.data[self.tag]
        self.init_zmq()
        self.init_listening_thread()
        self.initUI()
        self.init_timer()
        self.update_integration_size()
        self.update_center_rotation()
        self.update_c_tof()
        self.update_roi()
        self.update_image()




    def init_zmq(self):
        self.zmq_controlling_context = zmq.Context()
        self.zmq_control = self.zmq_controlling_context.socket(zmq.REQ)          
        if self.snd_ip != None:
            sip = self.snd_ip  
        else:
            sip = self.rec_ip  
        if self.snd_port != None:
            sport = self.snd_port
        else:
            sport = self.rec_port+1
        self.curr_snd_ip = sip
        self.curr_snd_port = sport
        self.zmq_control.connect('tcp://%s:%d' % (sip, sport))
        return


    def init_listening_thread(self):
        self.zeromq_listener_thread = PyQt4.QtCore.QThread()
        self.zeromq_listener = ZMQListener(self.rec_ip, self.rec_port, self.det_numPix)
        self.zeromq_listener.moveToThread(self.zeromq_listener_thread)
        self.zeromq_listener.zmqmessage.connect(self.data_received)
        self.zeromq_listener.zmqmessage_monitorparams.connect(self.parameter_changed)
        self.gui_integration_size_change.connect(self.zeromq_listener.update_gui_integration_size)
        self.listening_thread_start_processing.connect(self.zeromq_listener.start_listening)
        self.listening_thread_stop_processing.connect(self.zeromq_listener.stop_listening)
        self.zeromq_listener_thread.start()
        self.listening_thread_start_processing.emit()

                 
    def init_timer(self):
        self.refresh_timer = PyQt4.QtCore.QTimer()
        self.refresh_timer.timeout.connect(self.update_image)


    def initUI(self):
        pyqtgraph.setConfigOption('background', 0.2)

        self.tick_list_pixels = []
        for tick in range(0,1200,200):
            self.tick_list_pixels.append((tick, str(tick)))

        self.intregex = PyQt4.QtCore.QRegExp('[0-9]+')
        self.floatregex = PyQt4.QtCore.QRegExp('[0-9\.]+')
        self.ipregex = PyQt4.QtCore.QRegExp('[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')
        
        self.qtintvalidator = PyQt4.QtGui.QRegExpValidator()
        self.qtintvalidator.setRegExp(self.intregex)
        self.qtfloatvalidator = PyQt4.QtGui.QRegExpValidator()
        self.qtfloatvalidator.setRegExp(self.floatregex)
        self.qtipvalidator = PyQt4.QtGui.QRegExpValidator()
        self.qtipvalidator.setRegExp(self.ipregex)

        self.ring_pen = pyqtgraph.mkPen('r', width=2)
        self.center_pen = pyqtgraph.mkPen('g')
        
        self.imagew = pyqtgraph.ImageView(view = pyqtgraph.PlotItem())
        self.imagew.ui.normBtn.hide()
        self.imagew.ui.roiBtn.hide()
        self.ring_canvas = pyqtgraph.ScatterPlotItem()
        self.imagew.getView().addItem(self.ring_canvas)
        self.peak_canvas = pyqtgraph.ScatterPlotItem()
        self.imagew.getView().addItem(self.peak_canvas)
        self.center_canvas = pyqtgraph.ScatterPlotItem()
        self.imagew.getView().addItem(self.center_canvas)
        self.imagew.getView().setAspectLocked(lock = True, ratio = 1)
        self.imagew.getView().invertY()
        self.left_axis = self.imagew.getView().getAxis('left')
        self.left_axis.setLabel('Y-axis (pixels)')
        #self.left_axis.setTicks([self.tick_list_pixels])
        self.bottom_axis = self.imagew.getView().getAxis('bottom')
        self.bottom_axis.setLabel('X-axis (pixels)')
        #self.bottom_axis.setTicks([self.tick_list_pixels])
        self.right_axis = self.imagew.getView().getAxis('right')
        self.right_axis.setLabel('Velocity Y-axis (m/s)')
        self.top_axis = self.imagew.getView().getAxis('top')
        self.top_axis.setLabel('Velocity X-axis (m/s)')
        self.imagew.getView().showAxis('right')
        self.imagew.getView().showAxis('top')

        self.plot_plotwidget = pyqtgraph.PlotWidget(self)
        self.plot_plotwidget.setTitle('Running Avg. Cos^2 Theta vs. Number Of Shots (Background-corrected)')
        self.plot_plotwidget.setLabel('bottom', text = 'Integrated events')
        self.plot_plotwidget.setLabel('left', text = 'cos^2 theta')
        self.plot_left_axis = self.plot_plotwidget.getAxis('left')
        self.plot_plotwidget.enableAutoRange(axis=pyqtgraph.ViewBox.YAxis, enable=False)
        self.plot_plotwidget.showGrid(True,True)
        self.historyplot = self.plot_plotwidget.plot(self.costheta_history)


        self.runavgplot_plotwidget = pyqtgraph.PlotWidget(self)
        self.runavgplot_plotwidget.setTitle('Running Avg. Num Peaks Per Shot (Background-corrected)')
        self.runavgplot_plotwidget.setLabel('bottom', text = 'Integrated events')
        self.runavgplot_plotwidget.setLabel('left', text = 'Avg. num peaks per shot')
        self.runavgplot_left_axis = self.runavgplot_plotwidget.getAxis('left')
        self.runavgplot_plotwidget.showGrid(True,True)
        self.runavg_historyplot = self.runavgplot_plotwidget.plot(self.running_average_history)


        self.runavgsize_label = PyQt4.QtGui.QLabel(self)
        self.runavgsize_label.setText('Moving window size: ')
        self.runavgsize_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.runavgsize_lineedit.setText(str(self.running_average_size))
        self.runavgsize_lineedit.setValidator(self.qtintvalidator)
        self.runavgsize_lineedit.editingFinished.connect(self.update_running_average_size)

        self.col = numpy.array([0,0,0])
        self.xray_button = PyQt4.QtGui.QPushButton('X-ray', self)
        self.xray_button.setCheckable(True)
        self.xray_button.move(10, 10)
        self.xray_button.clicked[bool].connect(self.setLaser)
        self.align_button = PyQt4.QtGui.QPushButton('Align', self)
        self.align_button.setCheckable(True)
        self.align_button.move(10, 10)
        self.align_button.clicked[bool].connect(self.setLaser)
        self.frag_button = PyQt4.QtGui.QPushButton('Frag', self)
        self.frag_button.setCheckable(True)
        self.frag_button.move(10, 10)
        self.frag_button.clicked[bool].connect(self.setLaser)

        self.integrate_label = PyQt4.QtGui.QLabel(self)
        self.integrate_label.setText('<b>Integrate images:</b>')
        self.integrate_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.integrate_lineedit.setValidator(self.qtintvalidator)
        self.integrate_lineedit.setText(str(self.integration_size))
        self.integrate_lineedit.editingFinished.connect(self.update_integration_size)

        self.threshold_label = PyQt4.QtGui.QLabel(self)
        self.threshold_label.setText('<b>Peak Detection Threshold:</b>')
        self.threshold_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.threshold_lineedit.setValidator(self.qtfloatvalidator)
        self.threshold_lineedit.setText(str(self.peak_threshold))
        self.threshold_lineedit.editingFinished.connect(self.update_peak_threshold)

        #self.integrate_gui_label = PyQt4.QtGui.QLabel(self)
        #self.integrate_gui_label.setText('Integrate GUI images:')
        #self.integrate_gui_lineedit = PyQt4.QtGui.QLineEdit(self)
        #self.integrate_gui_lineedit.setValidator(self.qtintvalidator)
        #self.integrate_gui_lineedit.setText(str(self.integration_size))
        #self.integrate_gui_lineedit.editingFinished.connect(self.update_gui_integration_size)
        #self.integrate_gui_lineedit.editingFinished.connect(self.update_gui_integration_size)

        self.peaks_checkbox = PyQt4.QtGui.QCheckBox('Draw Peaks', self)
        self.peaks_checkbox.setChecked(True)

        self.hist2d_checkbox = PyQt4.QtGui.QCheckBox('Draw 2d histogram', self)
        self.hist2d_checkbox.setChecked(False)

        self.numpeaks_label = PyQt4.QtGui.QLabel(self)
        self.numpeaks_label.setText('Num Peaks in ROI: -')
        self.theta_label = PyQt4.QtGui.QLabel(self)
        self.theta_label.setText('Theta, <cos^2 Theta>:  - , -')
  
        self.center_label = PyQt4.QtGui.QLabel(self)
        self.center_label.setText('Center (x, y):')
        self.centerX_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.centerX_lineedit.setValidator(self.qtintvalidator)
        self.centerX_lineedit.setText(str(self.cenX))
        self.centerY_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.centerY_lineedit.setValidator(self.qtintvalidator)
        self.centerY_lineedit.setText(str(self.cenY))
        self.centerX_lineedit.editingFinished.connect(self.update_center_rotation)
        self.centerY_lineedit.editingFinished.connect(self.update_center_rotation)
 
        self.rotate_label = PyQt4.QtGui.QLabel(self)
        self.rotate_label.setText('Rotate (ccw):')
        self.rotate_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.rotate_lineedit.setValidator(self.qtfloatvalidator)
        self.rotate_lineedit.setText(str(self.rot))
        self.rotate_lineedit.editingFinished.connect(self.update_center_rotation)
 
        self.c_label = PyQt4.QtGui.QLabel(self)
        self.c_label.setText('C:')
        self.c_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.c_lineedit.setValidator(self.qtfloatvalidator)
        self.c_lineedit.setText(str(self.c))
        self.c_lineedit.editingFinished.connect(self.update_c_tof)
         
        self.tof_label = PyQt4.QtGui.QLabel(self)
        self.tof_label.setText('TOF:')
        self.tof_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.tof_lineedit.setValidator(self.qtfloatvalidator)
        self.tof_lineedit.setText(str(self.tof))
        self.tof_lineedit.editingFinished.connect(self.update_c_tof)
                
        self.velocityroi_label = PyQt4.QtGui.QLabel(self)
        self.velocityroi_label.setText('Velocity ROI (Min,Max):')
        self.velocityroimin_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.velocityroimin_lineedit.setValidator(self.qtfloatvalidator)
        self.velocityroimin_lineedit.setText(str(self.velroimin))
        self.velocityroimin_lineedit.editingFinished.connect(self.update_roi)
        self.velocityroimax_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.velocityroimax_lineedit.setValidator(self.qtfloatvalidator)
        self.velocityroimax_lineedit.setText(str(self.velroimax))
        self.velocityroimax_lineedit.editingFinished.connect(self.update_roi)
 
        self.angleroi_label = PyQt4.QtGui.QLabel(self)
        self.angleroi_label.setText('Angle ROI (Min,Max):')
        self.angleroimin_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.angleroimin_lineedit.setValidator(self.qtfloatvalidator)
        self.angleroimin_lineedit.setText(str(self.angleroimin))
        self.angleroimin_lineedit.editingFinished.connect(self.update_roi)
        self.angleroimax_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.angleroimax_lineedit.setValidator(self.qtfloatvalidator)
        self.angleroimax_lineedit.setText(str(self.angleroimax))
        self.angleroimax_lineedit.editingFinished.connect(self.update_roi)
 
        self.rings_label = PyQt4.QtGui.QLabel(self)
        self.rings_label.setText('Draw Rings (Comma-separated radii):')
        self.rings_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.rings_lineedit.setText(str(self.radii))
        self.rings_lineedit.editingFinished.connect(self.update_rings)
        self.update_rings()

        self.livestream_checkbox = PyQt4.QtGui.QCheckBox('Live Stream', self)
        self.livestream_checkbox.stateChanged.connect(self.update_livestream)
        
        self.reset_history_button = PyQt4.QtGui.QPushButton('Reset Plots', self)
        self.reset_history_button.clicked.connect(self.reset_history_plots)

        self.backgroundnpeaks_label = PyQt4.QtGui.QLabel(self)
        self.backgroundnpeaks_label.setText('Average # of backgr. peaks:')
        self.backgroundnpeaks_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.backgroundnpeaks_lineedit.setText(str(self.backgroundnpeaks))
        self.backgroundnpeaks_lineedit.setValidator(self.qtintvalidator)
        self.backgroundnpeaks_lineedit.editingFinished.connect(self.update_costhetabackround)

        self.backgroundavgcostheta_label = PyQt4.QtGui.QLabel(self)
        self.backgroundavgcostheta_label.setText('Average background costheta:')
        self.backgroundavgcostheta_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.backgroundavgcostheta_lineedit.setText(str(self.backgroundavgcostheta))
        self.backgroundavgcostheta_lineedit.setValidator(self.qtfloatvalidator)
        self.backgroundavgcostheta_lineedit.editingFinished.connect(self.update_costhetabackround)

        self.connect_label = PyQt4.QtGui.QLabel(self)
        self.connect_label.setText('Connect To (IP, Port):')
        self.connectip_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.connectip_lineedit.setText(self.rec_ip)
        self.connectip_lineedit.setValidator(self.qtipvalidator)
        self.connectip_lineedit.editingFinished.connect(self.update_connection)
        self.connectport_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.connectport_lineedit.setText(str(self.rec_port))
        self.connectport_lineedit.setValidator(self.qtintvalidator)
        self.connectport_lineedit.editingFinished.connect(self.update_connection)

        vlayout0 = PyQt4.QtGui.QVBoxLayout()

        vsplitter1 = PyQt4.QtGui.QSplitter(PyQt4.QtCore.Qt.Vertical)
        vsplitter1.addWidget(self.plot_plotwidget)
        vsplitter1.addWidget(self.runavgplot_plotwidget)

        hsplitter = PyQt4.QtGui.QSplitter(PyQt4.QtCore.Qt.Horizontal)
        hsplitter.addWidget(self.imagew)
        hsplitter.addWidget(vsplitter1)
 
        vsplitter2 = PyQt4.QtGui.QSplitter(PyQt4.QtCore.Qt.Vertical)
        vsplitter2.addWidget(hsplitter)

        self.savestate_pushbutton = PyQt4.QtGui.QPushButton('Save State', self)
        self.savestate_pushbutton.clicked.connect(self.save_state)

        self.loadstate_pushbutton = PyQt4.QtGui.QPushButton('Load State', self)
        self.loadstate_pushbutton.clicked.connect(self.load_state)

        self.delay_label = PyQt4.QtGui.QLabel(self)
        self.delay_label.setText('Extimated delay: -')

        hlayout1 = PyQt4.QtGui.QHBoxLayout()
        hlayout1.addWidget(self.xray_button)
        hlayout1.addWidget(self.align_button)
        hlayout1.addWidget(self.frag_button)

        hlayout2 = PyQt4.QtGui.QHBoxLayout()
        hlayout2.addWidget(self.theta_label)
        hlayout2.addWidget(self.numpeaks_label)

        hlayout3 = PyQt4.QtGui.QHBoxLayout()
        hlayout3.addWidget(self.integrate_label)
        hlayout3.addWidget(self.integrate_lineedit)
        hlayout3.addWidget(self.runavgsize_label)
        hlayout3.addWidget(self.runavgsize_lineedit)

        hlayout4 = PyQt4.QtGui.QHBoxLayout()
        hlayout4.addWidget(self.threshold_label)
        hlayout4.addWidget(self.threshold_lineedit)
        hlayout4.addStretch()
        hlayout4.addWidget(self.peaks_checkbox)
        hlayout4.addWidget(self.hist2d_checkbox)
 
        hlayout5 = PyQt4.QtGui.QHBoxLayout()
        hlayout5.addWidget(self.center_label)
        hlayout5.addWidget(self.centerX_lineedit)
        hlayout5.addWidget(self.centerY_lineedit)
        hlayout5.addWidget(self.rotate_label)
        hlayout5.addWidget(self.rotate_lineedit)
        hlayout5.addStretch()
 
        hlayout6 = PyQt4.QtGui.QHBoxLayout()
        hlayout6.addWidget(self.c_label)
        hlayout6.addWidget(self.c_lineedit)
        hlayout6.addWidget(self.tof_label)
        hlayout6.addWidget(self.tof_lineedit)
        hlayout6.addStretch()
        hlayout6.addStretch()
 
        hlayout7 = PyQt4.QtGui.QHBoxLayout()
        hlayout7.addWidget(self.velocityroi_label)
        hlayout7.addWidget(self.velocityroimin_lineedit)
        hlayout7.addWidget(self.velocityroimax_lineedit)
        hlayout7.addWidget(self.angleroi_label)
        hlayout7.addWidget(self.angleroimin_lineedit)
        hlayout7.addWidget(self.angleroimax_lineedit)
        hlayout7.addStretch()
         
        hlayout8 = PyQt4.QtGui.QHBoxLayout()
        hlayout8.addWidget(self.rings_label)
        hlayout8.addWidget(self.rings_lineedit)
        hlayout8.addStretch()
        
        hlayout9 = PyQt4.QtGui.QHBoxLayout()
        hlayout9.addWidget(self.livestream_checkbox)
        hlayout9.addStretch()
        hlayout9.addWidget(self.reset_history_button)
        hlayout9.addStretch()
        hlayout9.addStretch()
        hlayout9.addWidget(self.backgroundnpeaks_label)
        hlayout9.addWidget(self.backgroundnpeaks_lineedit)
        hlayout9.addWidget(self.backgroundavgcostheta_label)
        hlayout9.addWidget(self.backgroundavgcostheta_lineedit)

        hlayout10 = PyQt4.QtGui.QHBoxLayout()
        hlayout10.addWidget(self.connect_label)
        hlayout10.addWidget(self.connectip_lineedit)
        hlayout10.addWidget(self.connectport_lineedit)

        hlayout11 = PyQt4.QtGui.QHBoxLayout()
        hlayout11.addWidget(self.savestate_pushbutton)
        hlayout11.addWidget(self.loadstate_pushbutton)

        hlayout12 = PyQt4.QtGui.QHBoxLayout()
        hlayout12.addWidget(self.delay_label)
        hlayout12.addStretch()
        hlayout12.addStretch()

        vlayout0.addWidget(vsplitter2)
        vlayout0.addLayout(hlayout1)
        vlayout0.addLayout(hlayout2)
        vlayout0.addLayout(hlayout3)
        vlayout0.addLayout(hlayout4)
        vlayout0.addLayout(hlayout5)
        vlayout0.addLayout(hlayout6)
        vlayout0.addLayout(hlayout7)
        vlayout0.addLayout(hlayout8)
        vlayout0.addLayout(hlayout9)
        vlayout0.addLayout(hlayout10)
        vlayout0.addLayout(hlayout11)
        vlayout0.addLayout(hlayout12)

        self.setLayout(vlayout0)
        self.resize(1200,800)
        self.show()


    def save_state(self):
        filename = PyQt4.QtGui.QFileDialog.getSaveFileName(self, 'Save State In File', os.getenv('HOME'))
        str_filename = str(filename)
        fh = open(str_filename,'w')
        pickler = pickle.Pickler(fh)
        pickler.dump(self.costheta_data)
        pickler.dump(self.costheta_history)
        pickler.dump(self.running_average_data)
        pickler.dump(self.running_average_history)
        pickler.dump(self.tag)
        pickler.dump(self.integration_size)
        pickler.dump(self.running_average_size)
        pickler.dump(self.peak_threshold)
        pickler.dump(self.cenX)
        pickler.dump(self.cenY)
        pickler.dump(self.rot)
        pickler.dump(self.c)
        pickler.dump(self.tof)
        pickler.dump(self.velroimin)
        pickler.dump(self.velroimax)
        pickler.dump(self.angleroimin)
        pickler.dump(self.angleroimax)
        radii_to_save = [int(r/2) for r in self.radii]
        pickler.dump(radii_to_save)
        pickler.dump(self.livestream)
        pickler.dump(self.backgroundnpeaks)
        pickler.dump(self.backgroundavgcostheta)
        pickler.dump(self.peaks_checkbox.isChecked())
        pickler.dump(self.hist2d_checkbox.isChecked())
        fh.close()

    def load_state(self):
        filename = PyQt4.QtGui.QFileDialog.getOpenFileName(self, 'Load State From File', os.getenv('HOME'))
        str_filename = str(filename)
        fh = open(str_filename,'r')
        unpickler = pickle.Unpickler(fh)
        self.cos_theta_data = unpickler.load()
        self.costheta_history = unpickler.load()
        self.running_average_data = unpickler.load()
        self.running_average_history = unpickler.load()
        self.tag = unpickler.load()
        tag_bin = bin(self.tag)[2:]
        tag_bin_zfill = tag_bin.zfill(3)
        self.col = numpy.array([int(tag_bin_zfill[0]), int(tag_bin_zfill[1]), int(tag_bin_zfill[2])])

        self.xray_button.setChecked(int(self.col[2]))
        font = self.xray_button.font()
        font.setBold(int(self.col[2]))
        self.xray_button.setFont(font)

        self.align_button.setChecked(int(self.col[1]))
        font = self.align_button.font()
        font.setBold(int(self.col[1]))
        self.align_button.setFont(font)

        self.frag_button.setChecked(int(self.col[0]))
        font = self.align_button.font()
        font.setBold(int(self.col[0]))
        self.frag_button.setFont(font)

        integration_size = unpickler.load()
        self.integrate_lineedit.setText(str(integration_size))
        running_average_size = unpickler.load()
        self.runavgsize_lineedit.setText(str(running_average_size))
        peak_threshold = unpickler.load()
        self.threshold_lineedit.setText(str(peak_threshold))
        self.update_peak_threshold()
        cenX = unpickler.load()
        self.centerX_lineedit.setText(str(cenX))
        cenY = unpickler.load()
        self.centerY_lineedit.setText(str(cenY))
        rot = unpickler.load()
        self.rotate_lineedit.setText(str(rot))
        self.update_center_rotation()
        c = unpickler.load()
        self.c_lineedit.setText(str(c))
        tof = unpickler.load()
        self.tof_lineedit.setText(str(tof))
        self.update_c_tof()
        velroimin = unpickler.load()
        self.velocityroimin_lineedit.setText(str(velroimin))
        velroimax = unpickler.load()
        self.velocityroimax_lineedit.setText(str(velroimax))
        angleroimin = unpickler.load()
        self.angleroimin_lineedit.setText(str(angleroimin))
        angleroimax = unpickler.load()
        self.angleroimax_lineedit.setText(str(angleroimax))
        self.update_roi()
        self.update_integration_size()
        self.update_running_average_size()
        radii = unpickler.load()
        print radii
        self.rings_lineedit.setText(','.join([str(x) for x in radii]))
        self.update_rings()
        livestream = unpickler.load()
        self.livestream_checkbox.setChecked(livestream)
        if self.livestream == True:
            self.refresh_timer.start(250)
        else:
            self.refresh_timer.stop()
        backgroundnpeaks = unpickler.load()
        self.backgroundnpeaks_lineedit.setText(str(backgroundnpeaks))
        backgroundavgcostheta = unpickler.load()
        self.backgroundavgcostheta_lineedit.setText(str(backgroundavgcostheta))
        self.update_costhetabackround()
        peaks_checkbox_state = unpickler.load()
        self.peaks_checkbox.setChecked(peaks_checkbox_state)
        hist2d_checkbox_state =  unpickler.load()
        self.hist2d_checkbox.setChecked(hist2d_checkbox_state)
        fh.close()


    def data_received(self, tag, datdict):
        self.data[tag] = copy.deepcopy(datdict)


    def parameter_changed(self, di):
        if 'vmi_integration_size' in di.keys():
            if di['vmi_integration_size'] != self.integration_size:
                self.integrate_lineedit.setText(str(di['vmi_integration_size']))
                self.dialog = PyQt4.QtGui.QMessageBox(self)
                self.dialog.setStandardButtons(PyQt4.QtGui.QMessageBox.Ok)
                self.dialog.setIcon(PyQt4.QtGui.QMessageBox.Warning)                
                self.dialog.setWindowTitle('Monitor Parameter Changed')
                self.dialog.setText('WARNING!\n\nOne of the monitor parameters has been changed from another GUI')
                self.dialog.setModal(False)
                self.dialog.show()


    def setLaser(self, pressed):
        source = self.sender()

        font = source.font()
        if pressed:
            val = 1
            font.setBold(True);
        else: 
            val = 0
            font.setBold(False)
                        
        source.setFont(font)
                        
        if source.text() == "X-ray":
            self.col[2] = val
        elif source.text() == "Align":
            self.col[1] = val
        else:
            self.col[0] = val

        self.tag = numpy.sum( self.col * numpy.array([4, 2, 1]) )


    def update_integration_size(self):
        new_integration_size = int(self.integrate_lineedit.text())
        if new_integration_size != self.integration_size:
            self.integration_size = new_integration_size
            print 'Sending command to %s:%d' % (self.curr_snd_ip, self.curr_snd_port)
            dict_to_send = {}
            dict_to_send['vmi_integration_size'] = self.integration_size
            self.zmq_control.send_pyobj(dict_to_send)
            self.zmq_control.recv()
            print 'Command received'


    def update_gui_integration_size(self):
        new_gui_integration_size = int(self.integrate_gui_lineedit.text())
        if new_gui_integration_size != self.gui_integration_size:
            self.gui_integration_size = new_gui_integration_size
            self.gui_integration_size_change.emit(new_gui_integration_size)


    def update_peak_threshold(self):
        new_update_peak_threshold = float(self.threshold_lineedit.text())
        if new_update_peak_threshold != self.peak_threshold:
            self.peak_threshold = new_update_peak_threshold            
            print 'Sending command to %s:%d' % (self.curr_snd_ip, self.curr_snd_port)
            dict_to_send = {}
            dict_to_send['vmi_threshold'] = self.peak_threshold
            self.zmq_control.send_pyobj(dict_to_send)
            self.zmq_control.recv()
            print 'Command received'
        
        
    def update_center_rotation(self):
        self.cenY = int(self.centerY_lineedit.text())
        self.cenX = int(self.centerX_lineedit.text())
        self.rot = int(self.rotate_lineedit.text())
        self.update_image()          

        
    def update_c_tof(self):
        self.c = float(self.c_lineedit.text())
        self.tof = float(self.tof_lineedit.text())
        self.c_tof_scale = [ (x[0], str('%.2e' % int(round((x[0]-500)/(self.c*self.tof),0)))) for x in self.tick_list_pixels ]
        self.right_axis.setTicks([self.c_tof_scale])
        self.top_axis.setTicks([self.c_tof_scale])
        self.update_image()


    def update_roi(self):
        self.velroimin = float(self.velocityroimin_lineedit.text())
        self.velroimax = float(self.velocityroimax_lineedit.text())
        self.angleroimin = float(self.angleroimin_lineedit.text())
        self.angleroimax = float(self.angleroimax_lineedit.text())
        self.roi = wedgeMask(self.det_numPix,self.det_numPix,self.velroimax,self.velroimin,self.angleroimin,self.angleroimax)
        self.update_image()       


    def update_rings(self):
        self.radii = [2*int(x) for x in self.rings_lineedit.text().split(',')]
        self.update_image()          


    def update_connection(self):
        new_rec_ip = str(self.connectip_lineedit.text())
        new_rec_port = int(self.connectport_lineedit.text())
        if new_rec_ip != self.rec_ip or new_rec_port != self.rec_port:
            self.rec_ip = new_rec_ip
            self.rec_port = new_rec_port
            self.listening_thread_stop_processing.emit()
            self.zeromq_listener_thread.quit()
            self.zeromq_listener_thread.wait()
            self.init_listening_thread()
            self.zmq_control.disconnect('tcp://%s:%d' % (self.curr_snd_ip, self.curr_snd_port))
            self.init_zmq()
        

    def update_livestream(self, state):
        if state == PyQt4.QtCore.Qt.Checked:
            self.livestream = True
            self.refresh_timer.start(250)
        else:
            self.livestream = False
            self.refresh_timer.stop()                                       


    def update_costhetabackround(self):
        self.backgroundavgcostheta = float(self.backgroundavgcostheta_lineedit.text())
        self.backgroundnpeaks = float(self.backgroundnpeaks_lineedit.text())


    def update_running_average_size(self):
        new_running_average_size = int(self.runavgsize_lineedit.text())
        if new_running_average_size <= 0:
            self.runavgsize_lineedit.setText('1')
            new_running_average_size = 1
        if new_running_average_size != self.running_average_size:
            if new_running_average_size < self.running_average_size:
                for i in range(self.running_average_size-new_running_average_size):
                    self.running_average_data.popleft()            
                    self.costheta_data.popleft()
            self.running_average_size = new_running_average_size 


    def reset_history_plots(self):
        self.costheta_history = collections.deque()
        self.running_average_history = collections.deque()


    def update_image(self): 

        PyQt4.QtGui.QApplication.processEvents()

        if len(self.data.keys()) != 0:

            if self.livestream == True:
                self.local_data = self.data[self.tag]
                self.data = {}

            if 'raw_data' in self.local_data.keys():
                self.img = self.local_data['raw_data']
            else:
                self.img = numpy.zeros((self.det_numPix,self.det_numPix))
                self.img[(self.local_data['x_peaks'],self.local_data['y_peaks'])] = 100

            try:
                self.img = numpy.roll(self.img, (self.det_cx-self.cenX), axis=0) # +ve: right
                # shift x_peaks
                self.xInd = numpy.asarray(self.local_data['x_peaks']).reshape(-1)+(self.det_cx-self.cenX)
            except ValueError:
                pass
            
            try:
                #pass
                self.img = numpy.roll(self.img, (self.det_cx-self.cenY), axis=1) # +ve: down
                # shift y_peaks
                self.yInd = numpy.asarray(self.local_data['y_peaks']).reshape(-1)+(self.det_cx-self.cenY)
            except ValueError:
                pass
            
            # Apply ccw rotation
            try:
                self.img = scipy.ndimage.interpolation.rotate(self.img,
                                                              float(self.rot),
                                                              reshape=False)
                # Apply rotation to xy_peaks
                self.xInd -= self.det_cx
                self.yInd -= self.det_cx
                angleCCW = numpy.radians(self.rot)
                [self.xInd,self.yInd] = rotate2d(self.xInd,self.yInd,angleCCW)
                self.xInd += self.det_cx
                self.yInd += self.det_cx
            except ValueError:
                pass    

            self.roiIndOri = numpy.where(self.roi == 0)
            self.img[self.roiIndOri] += 50
            self.pts = pointsInRoi(self.xInd,self.yInd, self.roiIndOri, self.det_numPix)

            if self.local_data['extra_stuff'] != None:
                eventid = self.local_data['extra_stuff']
                vector = eventid.split('vector=')[1].split(',')[0]
                self.plot_plotwidget.setTitle('Running Avg. Cos^2 Theta vs. Number Of Shots (Background-corrected) - ' + vector)
            else:
                self.plot_plotwidget.setTitle('Running Avg. Cos^2 Theta vs. Number Of Shots (Background-corrected)')

            PyQt4.QtGui.QApplication.processEvents()

            self.center_of_mass_x = numpy.mean(self.pts[0])
            self.center_of_mass_y = numpy.mean(self.pts[1])
            
            if len(numpy.asarray(self.pts[0]).reshape(-1)) > 0:
                # peak position wrt (det_cx,det_cx)
                px = numpy.asarray(self.pts[0]).reshape(-1) - self.det_cx
                py = numpy.asarray(self.pts[1]).reshape(-1) - self.det_cx

                self.center_of_mass_x = numpy.mean(self.pts[0])
                self.center_of_mass_y = numpy.mean(self.pts[1])

                # calculate theta wrt (det_cx,det_cx)
                theta = numpy.zeros(shape=(len(px)))
                counter = 0
                for i in range(len(px)):
                    ang = math.atan2(px[i],py[i]) # ccw angle between +ve y-axis (-pi:pi)
                    #print "px,py: ", px[i], py[i]
                    #print "angle wrt +ve y-axis: ", numpy.degrees(ang)
                    theta[counter] = ang
                    counter += 1
                
                # calculate cos2theta
                self.cos2theta = 0.0
                self.bckgcorr_cos2theta = 0.0
                for i in theta:
                    self.cos2theta += numpy.cos(i)**2
                    self.bckgcorr_cos2theta += numpy.cos(i)**2

                self.cos2theta /= len(theta)

                self.bckgcorr_cos2theta -= self.integration_size*self.backgroundnpeaks*self.backgroundavgcostheta

                self.corr_len_theta = len(theta)-self.integration_size*self.backgroundnpeaks

                if self.corr_len_theta != 0:
                    self.bckgcorr_cos2theta /= (len(theta)-self.integration_size*self.backgroundnpeaks)
                else:
                    self.bckgcorr_cos2theta = 0.0

                if self.backgroundnpeaks != 0:
                    self.numpeaks_label.setText('Num Peaks in ROI: %i -  Background-corrected: %i' % (len(px), len(px)-self.integration_size*self.backgroundnpeaks) )
                    self.theta_label.setText('Theta, <cos^2 theta>: ' +
                                            str("%.3f" % (numpy.mean(theta))) +
                                            ', ' + "%.3f" % (self.cos2theta) +
                                            ' - Background corrected <cos^2 theta>:  %.3f' % (self.bckgcorr_cos2theta) )
                else:
                    self.numpeaks_label.setText('Num Peaks in ROI: %i' % len(px))
                    self.theta_label.setText('Theta, <cos^2 theta>: ' +
                                            str("%.3f" % (numpy.mean(theta))) +
                                            ', '+ str("%.3f" % (self.cos2theta)))

                self.corr_avg_peaks_shot = float(self.corr_len_theta)/float(self.integration_size) 

                self.running_average_data.append(self.corr_avg_peaks_shot)
                if len(self.running_average_data) > self.running_average_size:
                   self.running_average_data.popleft()

                # centred peak positions to velocity
                vx = px / (self.c * self.tof)
                vy = py / (self.c * self.tof)

                phi = numpy.zeros(shape=(len(vx)))
                for i in range(len(vx)):
                    phi[i] = math.atan2(vy[i],vx[i])            

                self.costheta_data.append(self.bckgcorr_cos2theta)
                if len(self.costheta_data) > self.running_average_size:
                    self.costheta_data.popleft()
                
            else:

                self.running_average_data.append(0)
                if len(self.running_average_data) > self.running_average_size:
                    self.running_average_data.popleft()
                self.costheta_data.append(0.0)
                if len(self.costheta_data) > self.running_average_size:
                    self.costheta_data.popleft()

                self.numpeaks_label.setText('Num Peaks in ROI: 0')
                self.theta_label.setText('Theta, <cos^2 Theta>:  0.0 , 0.0')

            self.running_average = numpy.mean(self.running_average_data)
            self.running_average_history.append(self.running_average)
            if len(self.running_average_history) > self.running_average_history_size:
                self.running_average_history.popleft()

            self.costheta_average = numpy.mean(self.costheta_data)
            self.costheta_history.append(self.costheta_average)
            if len(self.costheta_history) > self.costheta_history_size:
                self.costheta_history.popleft()

            PyQt4.QtGui.QApplication.processEvents()
            if self.hist2d_checkbox.isChecked():
                self.draw_hist2d()
            else:
                self.imagew.setImage(self.img, autoRange=False, autoLevels=True)
            self.historyplot.setData(self.costheta_history)
            self.runavg_historyplot.setData(self.running_average_history)
            self.draw_rings()

            if self.peaks_checkbox.isChecked():
                self.draw_peaks()
            else:
                self.peak_canvas.setData()
             
            if self.local_data['extra_stuff'] != None:
                eventid = self.local_data['extra_stuff']
                timestring = eventid.split('time=')[1].split(',')[0]
                timestamp = time.strptime(timestring[:-6],'%Y-%m-%d %H:%M:%S.%f')
                timestamp = datetime.datetime.fromtimestamp(time.mktime(timestamp))
                timenow = datetime.datetime.now()
                self.delay_label.setText(str((timenow-timestamp).seconds)+'.'+str((timenow-timestamp).microseconds)[0:3] + ' seconds')

    
    def draw_rings(self):
        if self.radii != 0:
            cen = [self.det_cx]*len(self.radii)    
            self.ring_canvas.setData(cen, cen, symbol = 'o', size = self.radii,
                                     brush=(255,255,255,0), pen = self.ring_pen,
                                     pxMode = False)


    def draw_peaks(self):
        if len(self.xInd) != 0:   
            self.peak_canvas.setData(self.xInd, self.yInd, symbol = 'o', size = [5]*len(self.xInd),
                                     brush=(255,255,255,0), pen = self.ring_pen,
                                     pxMode = False)
            self.center_canvas.setData([self.center_of_mass_x], [self.center_of_mass_y], brush=(255,255,255,0), size=[5], 
                                       symbol = 'o', pen = self.center_pen, pxmode=False)

    def draw_hist2d(self):
        if len(self.xInd) != 0:
            binFactor = 5.
            edges = numpy.arange(0,round(self.det_numPix/binFactor))
            H, xedges, yedges = numpy.histogram2d(self.yInd/binFactor, self.xInd/binFactor, bins=(edges,edges))    
            self.imagew.setImage(H, autoRange=False, autoLevels=True)

def wedgeMask(N,M,R,r,minTheta,maxTheta):
    """
    Calculate a donut mask.

    N,M - The height and width of the image
    R   - Maximum radius from center
    r   - Minimum radius from center
    minTheta maxTheta
    """
    import math
    mask = numpy.ones((N,M))
    for i in range(N):
        xDist = i-N/2.
        xDistSq = xDist**2
        for j in range(M):
            yDist = j-M/2.
            yDistSq = yDist**2
            rSq = xDistSq+yDistSq
            if rSq <= R**2 and rSq >= r**2:
                ang = math.atan2(i-N/2,j-M/2)
                if numpy.abs(ang)*180./3.14 <= maxTheta and numpy.abs(ang)*180./3.14 >= minTheta:
                    mask[j,i] = 0
    return mask


def rotate2d(pointsX,pointsY,angleCCW):
    rotX = numpy.empty([0])
    rotY = numpy.empty([0])
    for i in range(len(pointsX)):
                rotX = numpy.cos(float(angleCCW))*pointsX - numpy.sin(float(angleCCW))*pointsY
                rotY = numpy.sin(float(angleCCW))*pointsX + numpy.cos(float(angleCCW))*pointsY
    return rotX, rotY


def pointsInRoi(pointsX,pointsY,roi, det_numPix):
    pts = []
    #print "p[0]:", points[0]
    #print "p[1]:", points[1]
    #print "roi[0]:", roi[0]
    #print "roi[1]:", roi[1]


    row = roi[0]
    col = roi[1]
    #print "roi x:", row
    #print "roi y:", col
    data = numpy.ones(len(row))
    myRoi = coo_matrix((data, (row,col)), shape=(det_numPix, det_numPix))

    #tempRow = []
    #tempCol = []
    #tempData = []
    #print "len", len(pointsX)
    if len(pointsX) == []:
        pass # empty list
    else:
        row = pointsX
        col = pointsY
        data = numpy.ones(len(row))
        #print "pts x:", row
        #print "pts y:", col
        r = numpy.array(row)
        c = numpy.array(col)
        m = ((r % det_numPix) == r) * ((c % det_numPix) == c)
        myPts = coo_matrix((data[numpy.where(m)], (r[numpy.where(m)], c[numpy.where(m)])), shape=(det_numPix, det_numPix))
        #except:
        #    pass
#            for i in range(len(row)):
#                if row[i] > 0 and row[i] < det_numPix:
#                    if col[i] > 0 and col[i] < det_numPix:
#                        tempRow.append(row[i])
#                        tempcol.append(col[i])
#                        tempData.append(data[i])
#            myPts = coo_matrix(tempData,(tempRow,tempCol))
#            print "myPts:", myPts
#            print "err:", (row, col)            
        result = numpy.multiply(myRoi.todense(),myPts.todense())
        pts = numpy.where(result==1)
        #print "num pts: ", len(row)
        #print "result pts", np.sum(result)

    return pts


def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)    
    app = PyQt4.QtGui.QApplication(sys.argv)
    ex = MainFrame(sys.argv)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()    
