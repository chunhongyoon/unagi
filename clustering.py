#!/usr/bin/env python
"""
clustering.py:
Performs spectral clustering and displays embedding
Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.
 
UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""
import os.path
import sys
import numpy as np, h5py
import matplotlib.pyplot as plt
import blackbox as bx
import numpy.ma as ma
import spectralClustering as sc
from sklearn import manifold, datasets
from pylab import *
from sklearn.neighbors import kneighbors_graph
from sklearn.utils.sparsetools import connected_components
from sklearn.utils.graph import graph_laplacian
from sklearn.utils.arpack import eigsh
from sklearn.utils import check_random_state
from sklearn.cluster.k_means_ import k_means
from scipy import sparse
from sklearn.preprocessing import StandardScaler
from sklearn import cluster
from sklearn.metrics import euclidean_distances

def clusterStack( stackName, hits, n_clusters, knn, outfilename='labelList.txt' ):
    plt.ion()

    print 'Loading data ...'
    X = np.load(stackName)
    numImages,numPix = X.shape
    X = X[hits,:]

    # feature variance normalization
    X = StandardScaler().fit_transform(X)

    print 'Embedding ...'
    spectral = sc.SpectralClustering(n_clusters=n_clusters, eigen_solver='arpack',
                                     affinity="nearest_neighbors", n_neighbors=knn)

    for algorithm in [spectral]:
        algorithm.fit(X)
        if hasattr(algorithm, 'labels_'):
            y_pred = algorithm.labels_.astype(np.int)
        else:
            y_pred = algorithm.predict(X)

    y = np.ones(numImages)*-1 # misses are labeled at -1
    y[hits] = y_pred

    print 'Saving labels to file ...'
    np.savetxt(outfilename,y_pred,fmt='%d')

    return spectral, y

def cluster( filename, location, hitList, validR, validr, n_clusters, knn, outfilename ):
    plt.ion()

    print 'Loading data ...'
    f = h5py.File(filename,'r') 
    myData = f.get(location) # shape (n_samples, n_features)
    if not hitList == None:
        hits = np.loadtxt(hitList)
        myData = myData[hits.astype(bool),:]
    myData = np.array(myData) # For converting to numpy array
    numData, numPix = myData.shape
    width = int(np.sqrt(numPix))

    print 'Preprocessing data ...'
    # Apply Mask
    mask = bx.donutMask(width,width,validR,validr)
    maskL = np.reshape(mask,(1,-1))
    indL=np.where(maskL==0)
    X = myData[:,indL[1]]
    indL=np.where(maskL==1)
    myData[:,indL[1]] = 0

    if 0:
        for i in range(50):#(numData):
            img = np.reshape(myData[i,:], (width,width))
            imgplot = plt.imshow(img)
            plt.title(i)
            plt.pause(0.5)
        plt.close() 
      
    # feature variance normalization
    X = StandardScaler().fit_transform(X)

    print 'Embedding ...'
    spectral = sc.SpectralClustering(n_clusters=n_clusters, eigen_solver='arpack',
                                     affinity="nearest_neighbors", n_neighbors=knn)

    for algorithm in [spectral]:
        algorithm.fit(X)
        if hasattr(algorithm, 'labels_'):
            y_pred = algorithm.labels_.astype(np.int)
        else:
            y_pred = algorithm.predict(X)

    print 'Done. Displaying data ...'

    # showSubset for each cluster
    for pickCluster in range(n_clusters):
        title = 'cluster %s' % pickCluster
        bx.showSubset(myData,width,width,y_pred,criteria=pickCluster,subDim=5,title=title,vmin=0,vmax=100)


    bx.showCluster2D(spectral,y_pred)
    bx.showCluster3D(spectral,y_pred)

    plt.show(block=True)

    print 'Saving labels to file ...'
    np.savetxt(outfilename,y_pred,fmt='%d')

    return spectral, y_pred

def valid_file(x):
    if not os.path.exists(x):
        raise argparse.ArgumentError("{0} does not exist".format(x))
    return x

def valid_name(x):
    # Should check the outfile should be overwritten
    if os.path.exists(x):
        var = raw_input("Overwrite %s? [y/N]: " %x)
        if var == 'Y' or var == 'y':
            return x
        else:
            sys.exit(0)
    else:
        return x

def valid_hitList(x):
    # Should check whether the list exists
    if os.path.exists(x):
        return x
    else:
        print 'Could not find specified hit list'
        sys.exit(0)

def valid_array(x):
    # Should check whether 2 values are entered
    return x

# inputs to the script
if __name__ == '__main__':
    import argparse
    counter = 0
    parser = argparse.ArgumentParser(description='Clustering.')
    parser.add_argument("-i", "--input", dest="filename", required=True,
                        help='input hdf5 filename', metavar="FILE",
                        type=valid_file)
    parser.add_argument("-g", "--groupname", dest="groupname", required=False, default='myData',
                        help='input hdf5 groupname', metavar="FILE",
                        type=valid_name)
    parser.add_argument("-l", "--list", dest="hits", required=False,
                        help='input list of hits', metavar="FILE", default=None,
                        type=valid_hitList)
    parser.add_argument("-o", "--output", dest="outfilename", required=False,
                        help='output text filename', metavar="FILE",
                        type=valid_name, default="labelList.txt")
    parser.add_argument("-r","--rings", type=valid_array, nargs='*',
                        help='integers for defining q regions')
    parser.add_argument("-c","--n_clusters", type=int, default=5,
                        help='number of clusters to find from embedding')    
    parser.add_argument("-k","--knn", type=int, default=30,
                        help='k-nearest neighbors')
    args = parser.parse_args()
    validR = int(args.rings[0])
    validr = int(args.rings[1])
    cluster( args.filename, args.groupname, args.hits, validR, validr,
             args.n_clusters, args.knn, args.outfilename )








