#!/usr/bin/env python
"""
hitFinder.py:
Performs hit finding on a stack of image given in h5 format
Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.
 
UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""

import os.path
import sys
import numpy as np, h5py
import matplotlib.pyplot as plt
import blackbox as bx
from scipy import ndimage
#from skimage import data
#from skimage.filter import canny
#from skimage import morphology

def indexBatch(numData,batch):
    if batch >= numData:
        startIndices = np.zeros(1)
        batchSizes = np.ones(1)*numData
        return 1, startIndices, batchSizes
    else:
        numBatches = numData / batch + 1
        lastBatchSize = numData % batch
        batchSizes = np.tile(batch,(1,numBatches-1))
        batchSizes = np.append(batchSizes, lastBatchSize)
        startIndices = np.zeros_like(batchSizes)
        for i in range(numBatches):
            if i == 0:
                startIndices[i] = 0
            else:
                startIndices[i] = startIndices[i-1]+batchSizes[i-1]
        return numBatches, startIndices, batchSizes

def hitFindStack(stackName,thresh=None,outfilename='hitList.txt'):
    plt.ion()
    stack = np.load(stackName)
    mySum = np.sum(stack,axis=1)
    sortedSum = np.sort(mySum)
    if thresh == None:
        plt.plot(sortedSum,'rx-')
    if thresh:
        hits = mySum >= thresh
        print 'Done'
        numHits = np.sum(hits==True)
        hitRate = float(numHits)/len(hits)*100.
        print 'hit rate: %s %%' %hitRate

        print 'Saving hit list to file ...'
        np.savetxt(outfilename,hits,fmt='%d')

    plt.show(block=True)

def hitFind(filename, integR, integr, thresh, outfilename, batch):
    plt.ion()

    f = h5py.File(filename,'r')
    myData = f.get('myData') # shape (n_samples, n_features)
    numData, numPix = myData.shape
    width = int(np.sqrt(numPix))
    mySum = np.zeros(numData)
    hits = np.zeros(numData)
    if batch == 0:
        print 'full mode'
        # full mode
        myData = np.array(myData)
        # qFilter
        mySum = bx.qSum(myData,width,width,integR,integr)#35,20
        hits = bx.qFilter(mySum,thresh=thresh)
    else:
        # batch mode
        numBatches,startIndices,batchSizes = indexBatch(numData,batch)
        print numData,numBatches,startIndices,batchSizes
        for i in range(numBatches):
            print 'batch %s' %i
            startInd = startIndices[i]
            endInd = startInd+batchSizes[i]
            myBatch = np.array(myData[startInd:endInd,:])
            # qFilter
            batch_qSum = bx.qSum(myBatch,width,width,integR,integr)
            mySum[startInd:endInd] = batch_qSum
            hits[startInd:endInd] = bx.qFilter(batch_qSum,thresh=thresh)

    print 'Done. Displaying results ...'
    sortedSum = np.sort(mySum)
    plt.plot(sortedSum,'rx-')
    numHits = np.count_nonzero(hits)
    numMisses = numData-numHits
    hit_title='hits %s' % numHits
    miss_title='misses %s' % numMisses
    bx.showSubset(myData,width,width,hits,criteria=True,subDim=8,title=hit_title,vmin=0,vmax=100)
    bx.showSubset(myData,width,width,hits,criteria=False,subDim=8,title=miss_title,vmin=0,vmax=100)

    numHits = np.sum(hits==True)
    hitRate = float(numHits)/len(hits)*100.
    print 'hit rate: %s %%' %hitRate

    print 'Saving hit list to file ...'
    np.savetxt(outfilename,hits,fmt='%d')

    plt.show(block=True)

def valid_file(x):
    if not os.path.exists(x):
        raise argparse.ArgumentError("{0} does not exist".format(x))
    return x

def valid_name(x):
    # Should check the outfile should be overwritten
    if os.path.exists(x):
        var = raw_input("Overwrite %s? [y/N]: " %x)
        if var == 'Y' or var == 'y':
            return x
        else:
            sys.exit(0)
    else:
        return x

def valid_array(x):
    # Should check whether 2 values are entered
    return x

def valid_int(x):
    # Should check whether image per batch is positive
    try:
        if int(x) and int(x) > 0:
            return int(x)
        else:
            print 'Batch must be positive integer'
            sys.exit(0)
    except ValueError:
        print 'Batch must be positive integer'
        sys.exit(0)

# inputs to the script
if __name__ == '__main__':
    import argparse
    counter = 0
    parser = argparse.ArgumentParser(description='Hit finder.')
    parser.add_argument("-i", "--input", dest="filename", required=True,
                        help='input hdf5 filename', metavar="FILE",
                        type=valid_file)
    parser.add_argument("-o", "--output", dest="outfilename", required=False,
                        help='output text filename', metavar="FILE",
                        type=valid_name, default="hitList.txt")
    parser.add_argument("-r","--rings", type=valid_array, nargs='*',
                        help='integers for defining q regions')
    parser.add_argument("-t","--thresh", type=float, nargs='+',
                        help='threshold summed intensity in q region')
    parser.add_argument("-b","--batch", type=valid_int, required=False, default=0,
                        help='number of images to process per batch')    
    args = parser.parse_args()
    integR = int(args.rings[0])
    integr = int(args.rings[1])
    hitFind( args.filename, integR, integr, args.thresh, args.outfilename, args.batch )

