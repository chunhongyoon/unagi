"""
Convert ij pixel coordinates to xy coordinates
"""

import numpy as np

cspad_psana_shape = (4, 8, 185, 388)
cspad_geom_shape  = (1480, 1552)


def read_geometry_file(fnam, verbose = False):
    # get everything from the file
    f = open(fnam, 'r')
    f_lines = []
    for line in f:
        f_lines.append(line)
    
    # define the data we want
    shape = (4, 16)                                           # (quadrants, asics)
    min_fs   = np.zeros(shape, np.int16)  
    min_ss   = np.zeros(shape, np.int16)
    max_fs   = np.zeros(shape, np.int16)
    max_ss   = np.zeros(shape, np.int16)
    fs       = np.zeros((shape[0], shape[1], 2), np.float64)  # (quadrant, asic, [y, x])
    ss       = np.zeros((shape[0], shape[1], 2), np.float64)  # (quadrant, asic, [y, x])
    corner_x = np.zeros((shape[0], shape[1]), np.float64)     # (quadrant, asic)
    corner_y = np.zeros((shape[0], shape[1]), np.float64)     # (quadrant, asic)
    
    # get the values from e.g. q3a6/min_fs = 1164
    for q in range(4):
        for a in range(16):
            # define the strings to search for 
            qa_string = 'q' + str(q) + 'a' + str(a) + '/'
            min_fs_string = qa_string + 'min_fs'
            min_ss_string = qa_string + 'min_ss'
            max_fs_string = qa_string + 'max_fs'
            max_ss_string = qa_string + 'max_ss'
            fs_string = qa_string + 'fs'
            ss_string = qa_string + 'ss'
            corner_x_string = qa_string + 'corner_x'
            corner_y_string = qa_string + 'corner_y'

            # find the line containing the desired string
            line_min_fs = [l for l in f_lines if min_fs_string in l][0]
            line_min_ss = [l for l in f_lines if min_ss_string in l][0]
            line_max_fs = [l for l in f_lines if max_fs_string in l][0]
            line_max_ss = [l for l in f_lines if max_ss_string in l][0]
            line_fs = [l for l in f_lines if fs_string in l][0]
            line_ss = [l for l in f_lines if ss_string in l][0]
            line_corner_x = [l for l in f_lines if corner_x_string in l][0]
            line_corner_y = [l for l in f_lines if corner_y_string in l][0]
            
            # get the values
            min_fs[q, a] = int( line_min_fs.rsplit()[2] )
            min_ss[q, a] = int( line_min_ss.rsplit()[2] )
            max_fs[q, a] = int( line_max_fs.rsplit()[2] )
            max_ss[q, a] = int( line_max_ss.rsplit()[2] )
            fs[q, a, 0] = float( line_fs.rsplit()[3][:-1] )
            fs[q, a, 1] = float( line_fs.rsplit()[2][:-1] )
            ss[q, a, 0] = float( line_ss.rsplit()[3][:-1] )
            ss[q, a, 1] = float( line_ss.rsplit()[2][:-1] )
            corner_x[q, a] = float( line_corner_x.rsplit()[2] )
            corner_y[q, a] = float( line_corner_y.rsplit()[2] )

            # print if desired
            if verbose == 'v':
                print ''
                print '#------------- ', qa_string 
                print min_fs_string, min_fs[q, a]
                print min_ss_string, min_ss[q, a]
                print max_fs_string, max_fs[q, a]
                print max_ss_string, max_ss[q, a]
                print fs_string, fs[q, a]
                print ss_string, ss[q, a]
                print corner_x_string, corner_x[q, a]
                print corner_y_string, corner_y[q, a]

    return min_fs, min_ss, max_fs, max_ss, fs, ss, corner_x, corner_y 

def make_yx_from_4_8_16_185_388(geom_fnam):
    # now I want
    x = np.zeros(cspad_psana_shape, dtype=np.float32)
    y = np.zeros(cspad_psana_shape, dtype=np.float32)

    # read from the geometry file
    min_fs, min_ss, max_fs, max_ss, fs, ss, corner_x, corner_y = read_geometry_file(geom_fnam)

    for q in range(4):
        for a_8 in range(8):
            for a_2 in range(2):
                a = a_8 * 2 + a_2 
                # get the pixel coords for this asic
                i, j = np.meshgrid( np.arange(max_ss[q, a] - min_ss[q, a] + 1), np.arange(max_fs[q, a] - min_fs[q, a] + 1), indexing='ij')
                
                # 
                # make the y-x ( ss, fs ) vectors, using complex notation
                dx  = fs[q, a][0] + 1J * fs[q, a][1]
                dy  = ss[q, a][0] + 1J * ss[q, a][1]
                r_0 = corner_y[q, a] + 1J * corner_x[q, a] 
                #
                r   = i * dy + j * dx + r_0
                #
                y[q, a_8, :, a_2 * 194 : (a_2+1)*194] = r.real
                x[q, a_8, :, a_2 * 194 : (a_2+1)*194] = r.imag
    return y, x

def make_yx_from_1480_1552(geom_fnam):
    # now I want
    x = np.zeros(cspad_geom_shape, dtype=np.float32)
    y = np.zeros(cspad_geom_shape, dtype=np.float32)

    # read from the geometry file
    min_fs, min_ss, max_fs, max_ss, fs, ss, corner_x, corner_y = read_geometry_file(geom_fnam)

    for q in range(4):
        for a in range(16):
            # get the pixel coords for this asic
            i, j = np.meshgrid( np.arange(max_ss[q, a] - min_ss[q, a] + 1), np.arange(max_fs[q, a] - min_fs[q, a] + 1), indexing='ij')
            
            # 
            # make the y-x ( ss, fs ) vectors, using complex notation
            dx  = fs[q, a][0] + 1J * fs[q, a][1]
            dy  = ss[q, a][0] + 1J * ss[q, a][1]
            r_0 = corner_y[q, a] + 1J * corner_x[q, a] 
            #
            r   = i * dy + j * dx + r_0
            #
            y[min_ss[q, a]: max_ss[q, a] + 1, min_fs[q, a]: max_fs[q, a]+1] = r.real
            x[min_ss[q, a]: max_ss[q, a] + 1, min_fs[q, a]: max_fs[q, a]+1] = r.imag
    return y, x

def apply_geom_ij_yx(yx, cspad_np):
    """
    Apply the geometry from yx to reshape raw cspad data for display.

    Nearest neighbour interpolation is used to place the pixels on 
    the larger canvas (to save time). That is, the x-y values in 
    the tuple yx are converted to integer pixel coordinates after 
    shifting.

    e.g. if the cspad has a shape of (i, j, k) then
    cspad_np.shape = (i, j, k, l)
    yx.shape       = (y, x)     
    y.shape        = (i, j, k, l)  # y-values of each pixel
    x.shape        = (i, j, k, l)  # x-values of each pixel

    Parameters
    ----------
    yx : 2-D tuple, containing float numpy arrays 
         The x-y values corresponding to each pixel in cspad_np. 
         Each numpy array in the yx tuple must have the same shape
         as cspad_np. yx values are interpreted in units of pixels.

    cspad_np : numpy array
               A numpy array of any shape containing the pixel
               values of the cspad detector.
    Returns
    -------
    cspad_geom : cspad_np.dtype, 2-D numpy array
                 The geometry corrected cspad values. The origin
                 (y=0, x=0) is placed in the centre of the array
                 (N / 2 - 1, M / 2 - 1).
    """
    y = yx[0]
    x = yx[1]
    
    # find the smallest size of cspad_geom that contains all 
    # xy values but is symmetric about the origin
    N = 2 * int(max(abs(y.max()), abs(y.min()))) + 2
    M = 2 * int(max(abs(x.max()), abs(x.min()))) + 2

    cspad_geom = np.zeros((N, M), dtype=cspad_np.dtype)

    # convert y x values to i j values
    i = np.array(y, dtype=np.int) + N/2 - 1 
    j = np.array(x, dtype=np.int) + M/2 - 1 

    # apply geometry
    cspad_geom[i.flatten(), j.flatten()] = cspad_np.flatten()
    return cspad_geom

def apply_geom(geom_fnam, cspad_np):
    """
    Apply the cspad geometry provided in the file geom_fnam to reshape cspad_np data for display.

    Nearest neighbour interpolation is used to place the pixels on 
    the larger canvas (to save time). Only works on "psana" shaped
    arrays, e.g. (4, 8, 185, 388) or "CrystFel" shape arrays 
    e.g. (1480, 1552).

    Parameters
    ----------
    geom_fnam : string
                The file name of a geometry file used by CrystFel.

    cspad_np : numpy array, shape = (4, 8, 185, 388) or (1480, 1552)
               A numpy array containing the pixel values of the 
               cspad detector. 
    Returns
    -------
    cspad_geom : cspad_np.dtype, 2-D numpy array
                 The geometry corrected cspad values. The origin
                 (y=0, x=0) is placed in the centre of the array
                 (N / 2 - 1, M / 2 - 1).
    """
    # check if cspad is "psana" shaped or "CrystFel" shaped
    if cspad_np.shape == (4, 8, 185, 388) :
        y, x = make_yx_from_4_8_16_185_388(geom_fnam)
    
    elif cspad_np.shape == (1480, 1552) :
        y, x = make_yx_from_1480_1552(geom_fnam)
    
    cspad_geom = apply_geom_ij_yx((y, x), cspad_np)
    
    return cspad_geom 

def get_ij_psana_shaped(geom_fnam):
    """ Example:

    ij, NM  = get_ij_psana_shaped('/home/amorgan/Downloads/cspad-cxia2514-taw1.geom')
    cspad_geom = np.zeros(NM, dtype=np.int16)
    cspad_geom[ij[0], ij[1]] = cspad_np.flatten()
    """
    y, x = make_yx_from_4_8_16_185_388(geom_fnam)
    
    # find the smallest size of cspad_geom that contains all 
    # xy values but is symmetric about the origin
    N = 2 * int(max(abs(y.max()), abs(y.min()))) + 2
    M = 2 * int(max(abs(x.max()), abs(x.min()))) + 2

    # convert y x values to i j values
    i = np.array(y, dtype=np.int) + N/2 - 1 
    j = np.array(x, dtype=np.int) + M/2 - 1 

    ij = (i.flatten(), j.flatten())
    cspad_geom_shape = (N, M)
    return ij, cspad_geom_shape


def get_ij_slab_shaped(geom_fnam):
    """ Example:

    ij, NM  = get_ij_slab_shaped('/home/amorgan/Downloads/cspad-cxia2514-taw1.geom')
    cspad_geom = np.zeros(NM, dtype=np.int16)
    cspad_geom[ij[0], ij[1]] = cspad_np.flatten()
    """
    y, x = make_yx_from_1480_1552(geom_fnam)
    
    # find the smallest size of cspad_geom that contains all 
    # xy values but is symmetric about the origin
    N = 2 * int(max(abs(y.max()), abs(y.min()))) + 2
    M = 2 * int(max(abs(x.max()), abs(x.min()))) + 2

    # convert y x values to i j values
    i = np.array(y, dtype=np.int) + N/2 - 1 
    j = np.array(x, dtype=np.int) + M/2 - 1 

    ij = (i.flatten(), j.flatten())
    cspad_geom_shape = (N, M)
    return ij, cspad_geom_shape

if __name__ == '__main__':
    # load test data of the shape (4, 8, 185, 388)
    cspad_np = np.fromfile('/home/amorgan/Physics/git_repos/psana-online-monitor/dark_cal/cspad_modes_int16.npy', dtype=np.int16).reshape(cspad_geom_shape)
    
    geom_fnam = '/home/amorgan/Downloads/cspad-cxia2514-taw1.geom'

    cspad_geom = apply_geom(geom_fnam, cspad_np)

