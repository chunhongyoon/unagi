#!/usr/bin/env python
"""
UNAGI:
A GUI for viewing/labeling diffraction patterns.

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.

UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pprint
import random
import wx
import sys
import h5py
import numpy as np
from scipy import sparse
import sacla as sc
import blackbox as bx
import thread

# The recommended way to use wx with mpl is with the WXAgg
# backend. 
#
import matplotlib as mpl
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
mpl.use('WXAgg')
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

#---------------------------------------------------------------------------

# This is how you pre-establish a file filter so that the dialog
# only shows the extension(s) you want it to.
wildcard = "Hierarchical Data Format (*.h5)|*.h5|"     \
           "Python source (*.py)|*.py|"     \
           "Compiled Python (*.pyc)|*.pyc|" \
           "Binary dat (*.dat)|*.dat|" \
           "All files (*.*)|*.*"

#---------------------------------------------------------------------------

class mainFrame(wx.Frame):
    """ The main frame of the application
    """
    title = "UNAGI: Coherent Diffraction Viewer"
    det_numPixX = det_numPixY = 2399 # Hard coded for SACLA MPCCD
    numSensors = 8 # Hard coded for SACLA MPCCD front detector
    px = 512 # Hard coded for SACLA MPCCD front detector
    py = 1024 # Hard coded for SACLA MPCCD front detector
    numDP = 0 # Number of diffraction patterns loaded
    preassem = 1 # Default: load DataConvert3 preassembled detector geometry
    hasAssemData = 0 # Preassembled data available
    hasUnassemData = 0 # Unassembled data available
    hasFront = 0 # Front detector data available
    hasBack = 0 # Back detector data available
    front = 1
    cmax = 0
    cmin = 0
    flags = wx.ALIGN_LEFT | wx.ALL | wx.ALIGN_CENTER_VERTICAL
    # my labels
    myLabel = []
    spreadLabels = []
    spreadLabel = []
    labels = {'miss':-1, 'no label':0, 'single':1, 'double':2, 'many':3}
    haveDark = False
    haveImage = False
    haveCM = False
    haveManifold = False
    calcD = False
    unagi = False
    P = D = [] # Propagation and distance
    # Front detector
    img = np.zeros((1,1))
    det = np.zeros((1,1))
    darkDet = np.zeros((1,1))
    dataStack = []
    appPixPosX = 0
    appPixPosY = 0
    # Back detector
    appPixPosXBack = 0
    appPixPosYBack = 0
    det_numPixXBack = 1200  # Hard coded for SACLA MPCCD back detector
    det_numPixYBack = 1100  # Hard coded for SACLA MPCCD back detector
    numSensorsBack = 2  # Hard coded for SACLA MPCCD back detector
    haveDarkBack = False
    haveImageBack = False
    haveCMBack = False
    imgBack = np.zeros((1,1))
    detBack = np.zeros((1,1))
    darkDetBack = np.zeros((1,1))
    eps = 10
    knn = 30
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, self.title)
  
        self.create_menu()
        self.create_status_bar()
        self.create_main_panel()
        self.draw_figure()

    def create_menu(self):
        self.menubar = wx.MenuBar()
        
        menu_file = wx.Menu()
        m_expt = menu_file.Append(-1, "&Save plot\tCtrl-S", "Save plot to file")
        self.Bind(wx.EVT_MENU, self.on_save_plot, m_expt)
        menu_file.AppendSeparator()
        m_exit = menu_file.Append(-1, "E&xit\tCtrl-X", "Exit")
        self.Bind(wx.EVT_MENU, self.on_exit, m_exit)
        
        menu_help = wx.Menu()
        m_about = menu_help.Append(-1, "&About\tF1", "About the demo")
        self.Bind(wx.EVT_MENU, self.on_about, m_about)
        
        self.menubar.Append(menu_file, "&File")
        self.menubar.Append(menu_help, "&Help")
        self.SetMenuBar(self.menubar)

    def create_main_panel(self):
        """ Creates the main panel with all the controls on it:
             * mpl canvas 
             * mpl navigation toolbar
             * Control panel for interaction
        """
        self.panel = wx.Panel(self)
        
        # Create the mpl Figure and FigCanvas objects. 
        # 8x8 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig = plt.Figure((8.0, 8.0), dpi=self.dpi) # figsize,dpi
        self.canvas = FigCanvas(self.panel, -1, self.fig)
        
        # hbox1
        self.backbutton = wx.Button(self.panel, -1, "Back")
        self.Bind(wx.EVT_BUTTON, self.on_back_button, self.backbutton)
        self.nextbutton = wx.Button(self.panel, -1, "Next")
        self.Bind(wx.EVT_BUTTON, self.on_next_button, self.nextbutton)
        self.imageNumber_label = wx.StaticText(self.panel, -1, 
            "Image number: ") 
        self.imageNumberTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_imageNumber_enter, self.imageNumberTextbox)
        self.tag = 0
        self.imageNumberTextbox.SetValue(str(self.tag))
        self.numDP_label = wx.StaticText(self.panel, -1, 
            "num. images: %s" % self.numDP)
        self.assemCheckbox = wx.CheckBox(self.panel, -1, 
            "Load assembled",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_assemcheckbox_grid, self.assemCheckbox)
        self.fbCheckbox = wx.CheckBox(self.panel, -1, 
            "Back detector",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_fbcheckbox_grid, self.fbCheckbox)
        # hbox2
        self.caxis_label = wx.StaticText(self.panel, -1, 
            "caxis (min,max): ")        
        self.caxisMinTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_caxisMin_enter, self.caxisMinTextbox)
        self.caxisMaxTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_caxisMax_enter, self.caxisMaxTextbox)
        self.logCheckbox = wx.CheckBox(self.panel, -1, 
            "Log scale",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_logcheckbox_grid, self.logCheckbox)
        self.cmax_label = wx.StaticText(self.panel, -1, 
            "max: %s" % self.cmax)
        self.cmin_label = wx.StaticText(self.panel, -1, 
            "min: %s" % self.cmin)
        # hbox3
        self.centre_label = wx.StaticText(self.panel, -1, 
            "detector centre (x,y): ") 
        self.centreXTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_centreX_enter, self.centreXTextbox)
        self.centreYTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_centreY_enter, self.centreYTextbox)
        self.width_label = wx.StaticText(self.panel, -1, 
            "width (x,y): ") 
        self.widthXTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_widthX_enter, self.widthXTextbox)
        self.widthYTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_widthY_enter, self.widthYTextbox)
        self.singlebutton = wx.Button(self.panel, -1, "Single Hit")
        self.Bind(wx.EVT_BUTTON, self.on_single_button, self.singlebutton)
        self.missbutton = wx.Button(self.panel, -1, "Miss")
        self.Bind(wx.EVT_BUTTON, self.on_miss_button, self.missbutton)
        self.doublebutton = wx.Button(self.panel, -1, "Double Hit")
        self.Bind(wx.EVT_BUTTON, self.on_double_button, self.doublebutton)
        self.manybutton = wx.Button(self.panel, -1, "Many Hit")
        self.Bind(wx.EVT_BUTTON, self.on_many_button, self.manybutton)

        self.open = wx.Button(self.panel, -1, "Open hdf5")
        self.Bind(wx.EVT_BUTTON, self.OnOpen, self.open)

        self.save = wx.Button(self.panel, -1, "Save label")
        self.Bind(wx.EVT_BUTTON, self.OnSave, self.save)

        self.load = wx.Button(self.panel, -1, "Load label")
        self.Bind(wx.EVT_BUTTON, self.OnLoad, self.load)

        self.openDark = wx.Button(self.panel, -1, "Open dark hdf5")
        self.Bind(wx.EVT_BUTTON, self.OnOpenDark, self.openDark)

        self.unagiCheckbox = wx.CheckBox(self.panel, -1, 
            "Unagi",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_unagicheckbox_grid, self.unagiCheckbox)
        self.eps_label = wx.StaticText(self.panel, -1, 
            "eps: ") 
        self.epsTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_eps_enter, self.epsTextbox)
        self.epsTextbox.SetValue(str(self.eps))
        self.knn_label = wx.StaticText(self.panel, -1, 
            "knn: ") 
        self.knnTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_knn_enter, self.knnTextbox)
        self.knnTextbox.SetValue(str(self.knn))
        
        # Create Keyboard shortcuts
        back_id = wx.NewId()
        self.Bind(wx.EVT_MENU, self.on_back_button, id=back_id)
        next_id = wx.NewId()
        self.Bind(wx.EVT_MENU, self.on_next_button, id=next_id)
        single_id = wx.NewId()
        self.Bind(wx.EVT_MENU, self.on_single_button, id=single_id)
        miss_id = wx.NewId()
        self.Bind(wx.EVT_MENU, self.on_miss_button, id=miss_id)
        double_id = wx.NewId()
        self.Bind(wx.EVT_MENU, self.on_double_button, id=double_id)
        many_id = wx.NewId()
        self.Bind(wx.EVT_MENU, self.on_many_button, id=many_id)
        self.accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL, ord('B'), back_id),
                                              (wx.ACCEL_CTRL, ord('N'), next_id),
                                              (wx.ACCEL_CTRL, ord('0'), miss_id),
                                              (wx.ACCEL_CTRL, ord('1'), single_id),
                                              (wx.ACCEL_CTRL, ord('2'), double_id),
                                              (wx.ACCEL_CTRL, ord('3'), many_id),
                                             ])
        self.SetAcceleratorTable(self.accel_tbl)
        
        # Create the navigation toolbar, tied to the canvas
        #
        self.toolbar = NavigationToolbar(self.canvas)
        
        #
        # Layout with box sizers
        #

        # hbox1
        self.hbox1 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox1.Add(self.backbutton, 0, border=3, flag=self.flags)
        self.hbox1.Add(self.nextbutton, 0, border=3, flag=self.flags)
        self.hbox1.Add(self.imageNumber_label, 0, border=3, flag=self.flags)
        self.hbox1.Add(self.imageNumberTextbox, 0, border=3, flag=self.flags)
        self.hbox1.Add(self.numDP_label, 0, flag=self.flags)
        self.hbox1.AddSpacer(20)
        self.hbox1.Add(self.assemCheckbox, 0, border=3, flag=self.flags)
        self.hbox1.Add(self.fbCheckbox, 0, border=3, flag=self.flags)
        self.assemCheckbox.SetValue(self.preassem)
        
        # hbox2
        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.Add(self.caxis_label, 0, flag=self.flags)
        self.hbox2.AddSpacer(30)
        self.hbox2.Add(self.caxisMinTextbox, 0, border=3, flag=self.flags)
        self.hbox2.Add(self.caxisMaxTextbox, 0, border=3, flag=self.flags)
        self.hbox2.Add(self.logCheckbox, 0, border=3, flag=self.flags)
        self.logCheckbox.SetValue(1)
        self.hbox2.Add(self.cmax_label, 0, flag=self.flags)
        self.hbox2.AddSpacer(70)
        self.hbox2.Add(self.cmin_label, 0, flag=self.flags)

        # hbox3
        self.hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox3.Add(self.centre_label, 0, flag=self.flags)
        self.hbox3.Add(self.centreXTextbox, 0, border=3, flag=self.flags)
        self.hbox3.Add(self.centreYTextbox, 0, border=3, flag=self.flags)

        # hbox4
        self.hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox4.Add(self.width_label, 0, flag=self.flags)
        self.hbox4.AddSpacer(65)
        self.hbox4.Add(self.widthXTextbox, 0, border=3, flag=self.flags)
        self.hbox4.Add(self.widthYTextbox, 0, border=3, flag=self.flags)

        # hbox5
        self.hbox5 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox5.Add(self.missbutton, 0, border=3, flag=self.flags)
        self.hbox5.Add(self.singlebutton, 0, border=3, flag=self.flags)
        self.hbox5.Add(self.doublebutton, 0, border=3, flag=self.flags)
        self.hbox5.Add(self.manybutton, 0, border=3, flag=self.flags)

        # hbox6
        self.hbox6 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox6.Add(self.open, 0, border=3, flag=self.flags)
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.openDark, 0, border=3, flag=self.flags)
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.save, 0, border=3, flag=self.flags)        
        self.hbox6.Add(self.load, 0, border=3, flag=self.flags) 

        # hboxA
        self.hboxA = wx.BoxSizer(wx.HORIZONTAL)
        self.hboxA.Add(self.unagiCheckbox, 0, border=3, flag=self.flags)
        self.hboxA.Add(self.eps_label, 0, flag=self.flags)
        self.hboxA.Add(self.epsTextbox, 0, border=3, flag=self.flags)
        self.hboxA.Add(self.knn_label, 0, flag=self.flags)
        self.hboxA.Add(self.knnTextbox, 0, border=3, flag=self.flags)
        
        # vbox
        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)
        self.vbox.AddSpacer(10)
        self.vbox.Add(self.hbox1, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox2, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox3, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox4, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox5, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox6, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hboxA, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        
        self.panel.SetSizer(self.vbox)
        self.vbox.Fit(self)

    def OnOpen(self, evt):
        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'open' dialog, and allows multitple
        # file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, this
        # dialog is set up to change the current working directory to the path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            # This returns a Python list of files that were selected.
            self.filename = dlg.GetPath()

            self.flash_status_message('Drawing image ...')
            # Read in diffraction pattern
            self.myRun, self.myDet_2d, self.myTag, self.numDP, self.hasAssemData, self.hasUnassemData, self.hasFront, self.hasBack = sc.initMPCCD( self.filename )
            if self.hasAssemData == 0 and self.preassem == 1: 
                self.preassem = 0
                self.flash_status_message('This file does not contain any assembled data...')
            self.assemCheckbox.SetValue(self.preassem)
            
            self.numDP_label.SetLabel("num. images: %s" % self.numDP)
            self.myLabel = np.zeros(self.numDP)

            # Using our unassembled data, let's assemble own geometry 
            if self.hasUnassemData and not self.preassem:
                # get appPixPos of front and back
                if self.hasFront:
                    self.appPixPosX, self.appPixPosY = sc.pixPosMPCCD( self.filename, self.myRun, self.myDet_2d, self.myTag, self.px, self.py, self.numSensors, 1 )
                if self.hasBack:
                    self.appPixPosXBack, self.appPixPosYBack = sc.pixPosMPCCD( self.filename, self.myRun, self.myDet_2d, self.myTag, self.px, self.py, self.numSensorsBack, 0 )
            # Get the image and display
            self.get_image()
            self.draw_figure()
            self.haveImage = True
            ###########################
            # NEED DATA STACK FOR UNAGI
            #stackName = '/home/yoon/feltoolbox/python/scripts/stack.npy'
            #self.dataStack = np.load(stackName)
            #self.calcD = True
            ###########################
        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def get_image(self):
        if self.preassem: # Load assembled data
            if self.hasAssemData: # Do we have assembled data?
                if self.front:
                    if self.hasFront:
                        self.img = sc.loadAssembledMPCCD( self.filename, self.myRun, self.myTag, self.tag, 1)
                else:
                    if self.hasBack:
                        self.imgBack = sc.loadAssembledMPCCD( self.filename, self.myRun, self.myTag, self.tag, 0)
            if self.haveDark: # Do we have dark data?
                # load assembled dark and subtract from img
                if self.hasAssemData:
                    if self.front:
                        self.img -= self.darkDet
                    else:
                        self.imgBack -= self.darkDetBack
                # correct for common mode here (if you want to)
                pass
        else: # Load unassembled data using our geometry
            if self.hasUnassemData:
                if self.front: # generate front detector
                    if self.hasFront:
                        self.det = sc.getImgMPCCD( self.filename, self.myRun, self.myDet_2d, self.myTag, self.tag,
                                                   self.appPixPosX, self.appPixPosY, self.det_numPixX, self.det_numPixY,
                                                   self.px, self.py, self.numSensors )
                        if self.haveDark:
                            self.det -= self.darkDet
                        # correct for common mode here
                        #if self.haveCM:
                            #self.det -= self.cm
                        #else:
                            #self.cm = sc.getCommonModeMPCCD(self.det)
                            #self.haveCM = True
                            #self.det -= self.cm
                        #print 'plot cm'
                        #plt.figure()
                        #plt.plot(self.cm[200,:,0])
                        #plt.show()
                        #plt.figure()
                        #   plt.plot(self.det[200,:,0])
                        #plt.show()
                        self.img = sc.assembleMPCCD( self.det, self.appPixPosX, self.appPixPosY,
                                                     self.det_numPixX, self.det_numPixY,
                                                     self.px, self.py, self.numSensors )
                else: # # generate back detector
                    if self.hasBack:
                        self.detBack = sc.getImgMPCCD( self.filename, self.myRun, self.myDet_2d, self.myTag, self.tag,
                                                       self.appPixPosXBack, self.appPixPosYBack, self.det_numPixX, self.det_numPixY,
                                                       self.px, self.py, self.numSensorsBack, self.front )
                
                        if self.haveDarkBack:
                            self.detBack -= self.darkDetBack
                        # correct for common mode here
                        self.imgBack = sc.assembleMPCCD( self.detBack, self.appPixPosXBack, self.appPixPosYBack,
                                                         self.det_numPixXBack, self.det_numPixYBack,
                                                         self.px, self.py, self.numSensorsBack)
                    
    def OnOpenDark(self, evt):
        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'open' dialog, and allows multitple
        # file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, this
        # dialog is set up to change the current working directory to the path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            # This returns a Python list of files that were selected.
            self.darkFilename = dlg.GetPath()

            self.flash_status_message('Loading dark image ...')
            # Read in diffraction pattern
            myRun, myDet_2d, myTag, numDP, hasAssemData, hasUnassemData, hasFront, hasBack = sc.initMPCCD( self.darkFilename )
            
            if self.preassem: # Load preassembled data
                if hasAssemData:
                    if hasFront:
                        self.darkDet, self.haveDark = sc.getAssemDarkMPCCD( self.darkFilename, myRun, myTag, 1)
                    if hasBack:    
                        self.darkDetBack, self.haveDarkBack = sc.getAssemDarkMPCCD( self.darkFilename, myRun, myTag, 0)
                else:
                    self.flash_status_message('Assembled data not found ...')
            else: # Load unassembled data with our geometry
                if hadUnassemData:
                    if hasFront:
                        appPixPosX, appPixPosY = sc.pixPosMPCCD( self.darkFilename, myRun, myDet_2d, myTag, self.px, self.py, self.numSensors )
                        # Read in dark
                        self.darkDet, self.haveDark = sc.getAvgDarkMPCCD( self.darkFilename, appPixPosX, appPixPosY,
                                                                          self.det_numPixX, self.det_numPixY, self.px, self.py, self.numSensors )
                    if hasBack:
                        appPixPosXBack, appPixPosYBack = sc.pixPosMPCCD( self.darkFilename, myRun, myDet_2d, myTag, self.px, self.py, self.numSensorsBack, 0 )
                        # Read in dark
                        self.darkDetBack, self.haveDarkBack = sc.getAvgDarkMPCCD( self.darkFilename, appPixPosXBack, appPixPosYBack,
                                                                                  self.det_numPixXBack, self.det_numPixYBack, self.px, self.py,
                                                                                  self.numSensorsBack, 0 )
                else:
                    self.flash_status_message('Unassembled data not found ...')
            # Get the image and display
            if self.haveImage:
                self.get_image()
                self.draw_figure()
        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()
        
    def OnSave(self, evt):
        #self.log.WriteText("CWD: %s\n" % os.getcwd())

        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'save' dialog.
        #
        # Unlike the 'open dialog' example found elsewhere, this example does NOT
        # force the current working directory to change if the user chooses a different
        # directory than the one initially set.
        dlg = wx.FileDialog(
            self, message="Save file as ...", defaultDir=os.getcwd(), 
            defaultFile="", wildcard=wildcard, style=wx.SAVE
            )

        # This sets the default filter that the user will initially see. Otherwise,
        # the first filter in the list will be used by default.
        dlg.SetFilterIndex(3)

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()

            if self.unagi:
                np.savetxt(path,self.spreadLabel,fmt='%d')
            else:
                np.savetxt(path,self.myLabel,fmt='%d')

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def OnLoad(self, evt):
        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'open' dialog, and allows multitple
        # file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, this
        # dialog is set up to change the current working directory to the path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # This sets the default filter that the user will initially see. Otherwise,
        # the first filter in the list will be used by default.
        dlg.SetFilterIndex(3)

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()

            self.myLabel = np.loadtxt(path)

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def on_back_button(self, event):
        self.flash_status_message('Drawing image ...')
        # update tag value
        if self.tag-1 >= 0:
            self.tag = self.tag-1
        else:
            self.tag = 0
        # update textbox value
        self.imageNumberTextbox.SetValue(str(self.tag))
        self.get_image()
        self.draw_figure()

    def on_next_button(self, event):
        self.flash_status_message('Drawing image ...')
        # update tag value
        if self.tag+1 <= self.numDP-1: # arbitrary limit
            self.tag = self.tag+1
        else:
            self.tag = self.numDP-1
        # update textbox value
        self.imageNumberTextbox.SetValue(str(self.tag))
        self.get_image()
        self.draw_figure()

    def on_imageNumber_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.tag = int(self.imageNumberTextbox.GetValue())
        # check for sensible value
        if self.tag > self.numDP-1: # upper limit
            self.tag = self.numDP-1
            self.imageNumberTextbox.SetValue(str(self.tag))
        elif self.tag < 0:          # lower limit
            self.tag = 0
            self.imageNumberTextbox.SetValue(str(self.tag))
        self.get_image()
        self.draw_figure()

    def on_assemcheckbox_grid(self, event):
        self.flash_status_message('Load already assembled images ...')
        self.preassem = self.assemCheckbox.GetValue()
        self.get_image()
        self.draw_figure()

    def on_fbcheckbox_grid(self, event):
        self.front = not self.fbCheckbox.GetValue()
        if self.front:
            self.flash_status_message('View front detector...')
        else:
            self.flash_status_message('View back detector...')
        self.get_image()
        self.draw_figure()

    def on_unagicheckbox_grid(self, event):
        temp = self.unagiCheckbox.GetValue()
        if self.unagi == temp:
            pass
        else:
            self.calcD = True
        self.unagi = temp 
        if self.unagi:
            self.flash_status_message('Unagi waking up ...')
            if self.calcD:
                #if self.haveManifold:# and self.dataStack:
                #    self.P,self.D = bx.diffusionKernel(self.dataStack,self.eps,self.knn,self.D)
                #else:
                self.P,self.D = bx.diffusionKernel(self.dataStack,self.eps,self.knn)
                #thread.start_new_thread(self.updateDiffusion,(self.dataStack,self.eps,self.knn,))
                #self.haveManifold = True
                self.calcD = False
            #if self.haveManifold:
            if self.P == []:
                pass
            else:
                self.spreadLabel = bx.labelSpread(self.P,self.myLabel)
        else:
            self.flash_status_message('Unagi going to sleep ...')

    def updateDiffusion(self,dataStack,eps,knn):
        self.P,self.D = bx.diffusionKernel(dataStack,eps,knn)
        self.spreadLabel = bx.labelSpread(self.P,self.myLabel)
        
    def on_single_button(self, event):
        self.myLabel[self.tag] = self.labels['single']
        self.on_unagicheckbox_grid(event)
        self.flash_status_message('Drawing image ...')
        # update tag value
        if self.tag+1 <= self.numDP-1: # arbitrary limit
            self.tag = self.tag+1
        else:
            self.tag = self.numDP-1
        # update textbox value
        self.imageNumberTextbox.SetValue(str(self.tag))
        self.get_image()
        self.draw_figure()

    def on_double_button(self, event):
        self.myLabel[self.tag] = self.labels['double']
        self.on_unagicheckbox_grid(event)
        self.flash_status_message('Drawing image ...')
        # update tag value
        if self.tag+1 <= self.numDP-1: # arbitrary limit
            self.tag = self.tag+1
        else:
            self.tag = self.numDP-1
        # update textbox value
        self.imageNumberTextbox.SetValue(str(self.tag))
        self.get_image()
        self.draw_figure()

    def on_many_button(self, event):
        self.myLabel[self.tag] = self.labels['many']
        self.on_unagicheckbox_grid(event)
        self.flash_status_message('Drawing image ...')
        # update tag value
        if self.tag+1 <= self.numDP-1: # arbitrary limit
            self.tag = self.tag+1
        else:
            self.tag = self.numDP-1
        # update textbox value
        self.imageNumberTextbox.SetValue(str(self.tag))
        self.get_image()
        self.draw_figure()
        
    def on_miss_button(self, event):
        self.myLabel[self.tag] = self.labels['miss']
        self.on_unagicheckbox_grid(event)
        self.flash_status_message('Drawing image ...')
        # update tag value
        if self.tag+1 <= self.numDP-1: # arbitrary limit
            self.tag = self.tag+1
        else:
            self.tag = self.numDP-1
        # update textbox value
        self.imageNumberTextbox.SetValue(str(self.tag))
        self.get_image()
        self.draw_figure()

    def on_logcheckbox_grid(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_eps_enter(self, event):
        temp = self.epsTextbox.GetValue()
        if not self.eps == temp: # new value received
            self.eps = temp
            self.calcD = True 

    def on_knn_enter(self, event):
        temp = self.knnTextbox.GetValue()
        if not self.knn == temp: # new value received
            self.knn = temp
            self.calcD = True
        
    def on_caxisMin_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_caxisMax_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()      

    def on_centreX_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_centreY_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_widthX_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_widthY_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()
 
    def create_status_bar(self):
        self.statusbar = self.CreateStatusBar()

    def draw_figure(self):
        """ Redraws the figure
        """        
        caxisMin = self.caxisMinTextbox.GetValue()
        caxisMax = self.caxisMaxTextbox.GetValue()
        centreX = self.centreXTextbox.GetValue() # must be int for now
        centreY = self.centreYTextbox.GetValue()
        widthX = self.widthXTextbox.GetValue() # must be int for now
        widthY = self.widthYTextbox.GetValue()
        logscale = self.logCheckbox.GetValue()
        imageNumber = self.imageNumberTextbox.GetValue()
        
        if self.front:
            img = self.img # init display image
        else:
            img = self.imgBack

        # Check sensible input
        if not (caxisMin == '' or caxisMax == ''):
            vmin = caxisMin
            vmax = caxisMax
        else:
            vmin = vmax = None

        if not (centreX == '' or centreY == ''):
            cx = int(centreX)
            cy = int(centreY)
        else:
            cx = cy = 1200

        if not (widthX == '' or widthY == ''):
            wx = int(widthX)
            wy = int(widthY)
        else:
            wx = self.det_numPixX
            wy = self.det_numPixY
            
        # Check cropping criteria
        if cy-wy >= 0:
            sy=cy-wy
        else:
            sy=0
        if self.front:
            if cy+wy <= self.det_numPixY:
                ey=cy+wy
            else:
                ey=self.det_numPixY
            if cx-wx >= 0:
                sx=cx-wx
            else:
                sx=0
            if cx+wx <= self.det_numPixX:
                ex=cx+wx
            else:
                ex=self.det_numPixX
        else:
            if cy+wy <= self.det_numPixYBack:
                ey=cy+wy
            else:
                ey=self.det_numPixYBack
            if cx-wx >= 0:
                sx=cx-wx
            else:
                sx=0
            if cx+wx <= self.det_numPixXBack:
                ex=cx+wx
            else:
                ex=self.det_numPixXBack
        img = img[sy:ey,sx:ex] # crop image

        self.cmax = np.max(img)
        self.cmin = np.min(img)
        self.cmax_label.SetLabel("max: %6.1f" % self.cmax)
        self.cmin_label.SetLabel("min: %6.1f" % self.cmin)

        if logscale == True:
            eps = 1e-18
            img = np.log10(np.absolute(img)+ eps)

        self.fig.clf()
        ax = self.fig.add_subplot(111, axisbg='r')
        imgplot = ax.imshow(img,cmap='jet',vmin=vmin,vmax=vmax)
        if not len(self.myLabel) == 0:
            if self.unagi:
                # display active learning labels
                print self.labels.keys()
                print self.labels.values()
                print self.spreadLabel
                print self.spreadLabel[self.tag]
                print self.labels.keys()[self.labels.values().index(self.spreadLabel[self.tag])]
                print "spreadLabel-1: %s" %np.where(self.spreadLabel==-1)
                print "spreadLabel1: %s" %np.where(self.spreadLabel==1)
                print "spreadLabel2: %s" %np.where(self.spreadLabel==2)
                print "spreadLabel3: %s" %np.where(self.spreadLabel==3)
                print "myLabel-1: %s" %np.where(self.myLabel==-1)
                print "myLabel1: %s" %np.where(self.myLabel==1)
                print "myLabel2: %s" %np.where(self.myLabel==2)
                print "myLabel3: %s" %np.where(self.myLabel==3)
                ax.set_title('Label: %s' % self.labels.keys()[self.labels.values().index(self.spreadLabel[self.tag])])
            else:
                ax.set_title('Label: %s' % self.labels.keys()[self.labels.values().index(self.myLabel[self.tag])])
        self.canvas.draw()
    
    def on_save_plot(self, event):
        file_choices = "PNG (*.png)|*.png"
        
        dlg = wx.FileDialog(
            self, 
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="dp.png",
            wildcard=file_choices,
            style=wx.SAVE)
        
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)
        
    def on_exit(self, event):
        self.Destroy()
        
    def on_about(self, event):
        msg = """ A GUI for viewing/labeling diffraction patterns.
        """
        dlg = wx.MessageDialog(self, msg, "About", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
    
    def flash_status_message(self, msg, flash_len_ms=500):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(
            wx.EVT_TIMER, 
            self.on_flash_status_off, 
            self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)
    
    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')

class MySplashScreen(wx.SplashScreen):
    def __init__(self):
        bmp = wx.Image(opj("bitmaps/unagi.png")).ConvertToBitmap()
        wx.SplashScreen.__init__(self, bmp,
                                 wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT,
                                 3000, None, -1)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.fc = wx.FutureCall(1000, self.ShowMain)


    def OnClose(self, evt):
        # Make sure the default handler runs too so this window gets
        # destroyed
        evt.Skip()
        self.Hide()
        
        # if the timer is still running then go ahead and show the
        # main frame now
        if self.fc.IsRunning():
            self.fc.Stop()
            self.ShowMain()


    def ShowMain(self):
        frame = mainFrame()
        frame.Show()

def opj(path):
    """Convert paths to the platform-specific separator"""
    st = apply(os.path.join, tuple(path.split('/')))
    # HACK: on Linux, a leading / gets lost...
    if path.startswith('/'):
        st = '/' + st
    return st

if __name__ == '__main__':
    app = wx.App()
    splash = MySplashScreen()
    splash.Show()
    app.MainLoop()
