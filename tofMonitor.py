#!/usr/bin/env python
"""
tofMonitor:
A GUI for monitoring Time-of-Flight (ToF).

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.

UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pprint
import random
import wx
import sys
import h5py
import numpy as np
import scipy.ndimage
import lcls as lc
import blackbox as bx
import thread
import math
import zmq
import threading

import pickle

# The recommended way to use wx with mpl is with the WXAgg
# backend. 
#
import matplotlib as mpl
#mpl.use('WXAgg')
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

#---------------------------------------------------------------------------

# This is how you pre-establish a file filter so that the dialog
# only shows the extension(s) you want it to.
wildcard = "Text file (*.txt)|*.txt|" \
           "Hierarchical Data Format (*.h5)|*.h5|"     \
           "Python source (*.py)|*.py|"     \
           "Compiled Python (*.pyc)|*.pyc|" \
           "Binary dat (*.dat)|*.dat|" \
           "All files (*.*)|*.*"

#---------------------------------------------------------------------------

class mainFrame(wx.Frame):
    """ The main frame of the application
    """
    title = "LCLS ToF Monitor"
    cold = 20 
    hot = 2300
    det_numPixX = lc.acqiris().det_numPixX
    numSensors = lc.acqiris().numSensors
    numDP = 0 # Number of diffraction patterns loaded
    myFiles = []
    maskKeyPressed = 0
    unmaskKeyPressed = 0
    flags = wx.ALIGN_LEFT | wx.ALL | wx.ALIGN_CENTER_VERTICAL
    # my labels
    haveTrace = False
    time = None
    potential = None
    data = []

    def __init__(self, rec_ip, rec_port):
        self.init_zmq(rec_ip, rec_port)
        wx.Frame.__init__(self, None, wx.ID_ANY, self.title)
  
        self.thread_lock = threading.Lock()
        self.thread_pub = threading.Thread(target=self.zmq_sub_function)
        self.thread_pub.daemon=True
        self.thread_pub.start()
  
        self.create_menu()
        self.create_status_bar()
        self.create_main_panel()
        self.draw_figure()
        
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.draw_figure, self.timer)
        
    def init_zmq(self, rec_ip, rec_port):
        self.zmq_context = zmq.Context()
        self.zmq_subscribe = self.zmq_context.socket(zmq.SUB)
        print ('Listening at tcp://%s:%d' % (rec_ip, rec_port))
        self.zmq_subscribe.connect('tcp://%s:%d' % (rec_ip, rec_port))
        self.zmq_subscribe.setsockopt_string(zmq.SUBSCRIBE, u'tofdata')

    def zmq_sub_function(self):
        while True:
            header, tag, msg = self.zmq_subscribe.recv_multipart()
            self.thread_lock.acquire()
            self.data = pickle.loads(msg)
            #
            # get the trace and time axis 
            self.potential = self.data['tof_data'][0]
            self.time = np.arange(self.potential.shape[0], dtype=np.float64)
            self.thread_lock.release()

    def create_menu(self):
        self.menubar = wx.MenuBar()
        
        menu_file = wx.Menu()
        m_expt = menu_file.Append(-1, "&Save plot\tCtrl-S", "Save plot to file")
        self.Bind(wx.EVT_MENU, self.on_save_plot, m_expt)
        menu_file.AppendSeparator()
        m_exit = menu_file.Append(-1, "E&xit\tCtrl-X", "Exit")
        self.Bind(wx.EVT_MENU, self.on_exit, m_exit)
        
        menu_help = wx.Menu()
        m_about = menu_help.Append(-1, "&About\tF1", "About the demo")
        self.Bind(wx.EVT_MENU, self.on_about, m_about)
        
        self.menubar.Append(menu_file, "&File")
        self.menubar.Append(menu_help, "&Help")
        self.SetMenuBar(self.menubar)

    def create_main_panel(self):
        """ Creates the main panel with all the controls on it:
             * mpl canvas 
             * mpl navigation toolbar
             * Control panel for interaction
        """
        self.panel = wx.Panel(self)
        
        # Create the mpl Figure and FigCanvas objects. 
        # 8x8 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig, (self.ax, self.ax1) = plt.subplots(2, 1,figsize=(8.0, 8.0),dpi=self.dpi) # 2x1 subplot
        self.canvas = FigCanvas(self.panel, -1, self.fig)
        
        # hbox2
        self.t0_label = wx.StaticText(self.panel, -1, 
            "Initial time t0 (us): ")        
        self.t0_Textbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_t0_enter, 
                  self.t0_Textbox)

        # hbox3
        self.m1_label = wx.StaticText(self.panel, -1, 
            "Mass m1: ") 
        self.m1_Textbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_m1_enter, self.m1_Textbox)
        self.t1_label = wx.StaticText(self.panel, -1, 
            "ToF t1 (us): ")
        self.t1_Textbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_t1_enter, self.t1_Textbox)

        # hbox4
        self.m2_label = wx.StaticText(self.panel, -1, 
            "Mass m2: ") 
        self.m2_Textbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_m2_enter, self.m2_Textbox)
        self.t2_label = wx.StaticText(self.panel, -1, 
            "ToF t2 (us): ")
        self.t2_Textbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_t2_enter, self.t2_Textbox)

        self.liveCheckbox = wx.CheckBox(self.panel, -1, 
            "live stream",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_liveCheckbox_enter, self.liveCheckbox)

        self.open = wx.Button(self.panel, -1, "Load trace")
        self.Bind(wx.EVT_BUTTON, self.OnOpen, self.open)

        self.save = wx.Button(self.panel, -1, "Save trace")
        self.Bind(wx.EVT_BUTTON, self.OnSave, self.save)
        
        # Create the navigation toolbar, tied to the canvas
        #
        self.toolbar = NavigationToolbar(self.canvas)
        
        #
        # Layout with box sizers
        #
        
        # hbox2
        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.Add(self.t0_label, 0, flag=self.flags)
        self.hbox2.AddSpacer(20)
        self.hbox2.Add(self.t0_Textbox, 0, border=3, flag=self.flags)

        # hbox3
        self.hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox3.Add(self.m1_label, 0, flag=self.flags)
        self.hbox3.AddSpacer(10)
        self.hbox3.Add(self.m1_Textbox, 0, border=3, flag=self.flags)
        self.hbox3.AddSpacer(10)
        self.hbox3.Add(self.t1_label, 0, flag=self.flags)
        self.hbox3.AddSpacer(10)
        self.hbox3.Add(self.t1_Textbox, 0, border=3, flag=self.flags)

        # hbox4
        self.hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox4.Add(self.m2_label, 0, flag=self.flags)
        self.hbox4.AddSpacer(10)
        self.hbox4.Add(self.m2_Textbox, 0, border=3, flag=self.flags)
        self.hbox4.AddSpacer(10)
        self.hbox4.Add(self.t2_label, 0, flag=self.flags)
        self.hbox4.AddSpacer(10)
        self.hbox4.Add(self.t2_Textbox, 0, border=3, flag=self.flags)

        # hbox6
        self.hbox6 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox6.Add(self.open, 0, border=3, flag=self.flags)
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.save, 0, border=3, flag=self.flags)
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.liveCheckbox, 0, border=3, flag=self.flags)        
    
        # vbox
        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)
        self.vbox.AddSpacer(10)
        self.vbox.Add(self.hbox2, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox3, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox4, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox6, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        
        self.panel.SetSizer(self.vbox)
        self.vbox.Fit(self)

    def OnOpen(self, evt):
        # Create the dialog. In this case the current directory is forced 
        # as the starting directory for the dialog, and no default file name is 
        # forced. This can easilly be changed in your program. This is an 'open' 
        # dialog, and allows multitple file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, 
        # this dialog is set up to change the current working directory to the 
        # path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # Show the dialog and retrieve the user response. If it is the OK 
        # response, process the data.
        if dlg.ShowModal() == wx.ID_OK:
            # This returns a Python list of files that were selected.
            self.filename = dlg.GetPath()

            self.flash_status_message('Drawing image ...')

            # Get the trace and display
            
            self.get_trace()
            self.flash_status_message('Opening trace ...')
            #self.mask = np.ones(np.shape(self.img))
            #self.maskAsicEdge = np.ones(np.shape(self.img))
            #self.maskDead = np.ones(np.shape(self.img))
            #self.initMask = 1
            self.haveTrace = True
            print self.time
            print self.potential
            self.draw_figure()
            

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def get_trace(self):
        trace = np.loadtxt( self.filename )
        self.time = trace[:,0] * 1e6 #(us)
        self.potential = trace[:,1]

    def OnSave(self, evt):
        #self.log.WriteText("CWD: %s\n" % os.getcwd())

        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'save' dialog.
        #
        # Unlike the 'open dialog' example found elsewhere, this example does NOT
        # force the current working directory to change if the user chooses a different
        # directory than the one initially set.
        dlg = wx.FileDialog(
            self, message="Save file as ...", 
            defaultDir=os.getcwd(), 
            defaultFile="ToF.txt",
            wildcard=wildcard,
            style=wx.SAVE
            )

        # This sets the default filter that the user will initially see. Otherwise,
        # the first filter in the list will be used by default.
        dlg.SetFilterIndex(3)

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()

            np.savetxt( path, self.trace )

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def on_t0_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()  

    def on_m1_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_t1_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_m2_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_t2_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_liveCheckbox_enter(self, event):
        self.flash_status_message('Drawing image ...')
        if self.liveCheckbox.GetValue() == True:
            self.timer.Start(1000)
        else: 
            self.timer.Stop()
 
    def create_status_bar(self):
        self.statusbar = self.CreateStatusBar()

    def tick_function(self, V):
        return ["%.2f" % z for z in V]

    def draw_figure(self, event = None):
        """ Redraws the figure
        """        
        myT0 = self.t0_Textbox.GetValue()
        myM1 = self.m1_Textbox.GetValue()
        myT1 = self.t1_Textbox.GetValue()
        myM2 = self.m2_Textbox.GetValue()
        myT2 = self.t2_Textbox.GetValue()
        
        nbins = 10

        self.ax.cla()
        self.ax.plot(self.time, self.potential,'b-',picker=5)
        self.ax.locator_params(axis = 'x',nbins=nbins)
        self.ax.autoscale(tight=True)
        self.ax.set_xlabel('Measured ToF (us)')
        self.ax.set_ylabel('Voltage (V)')

        self.fig.canvas.mpl_connect('axes_enter_event', self.enter_axes)
        self.fig.canvas.mpl_connect('axes_leave_event', self.leave_axes)
        self.fig.canvas.mpl_connect('pick_event', self.onpick)
        self.fig.canvas.mpl_connect('key_press_event', self.on_press)
        self.fig.canvas.mpl_connect('key_release_event', self.on_release)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)

        self.canvas.draw()

        #################################
## Peak finder should find these
#peakPositions = np.array([0.932, 2.144, 2.545, 3.185, 4.35, 4.393, 5.67, 5.828, 5.95, 6.01, 8.036])
#peakPotentials = np.array([-0.06, -0.0265, -0.0145, -0.0072, -0.009, -0.008, -0.008, -0.012, -0.011, -0.014, -0.0328])
        # GUI input
        myM1 = 1. # must convert to float
        myT1 = 0.932
        myM2 = 240. # must convert to float
        myT2 = 8.036
        #################################

        # Check values before use
        try:
            myM1 = float(myM1)
            myT1 = float(myT1)
            myM2 = float(myM2)
            myT2 = float(myT2)
        except ValueError:
            return
        
        if self.time == None:
            return

        t0 = 0
        try:
            t0 = float(myT0) # Manual override of initial time
        except ValueError:
            t0 = (np.sqrt(myM1/myM2)*myT2-myT1) / (np.sqrt(myM1/myM2)-1)
        print "t0: %.5f" % t0
        
        t1_real = myT1-t0
        print "t1_real: %.5f" % t1_real
        myMasses = []
        for t in self.time:
            myMasses.append( myM1 * pow((t-t0)/t1_real , 2) )
        
        self.ax1.cla()
        self.ax1.plot(myMasses,self.potential,'r-',picker=5)
        self.ax1.autoscale(tight=True)
        self.ax1.set_xlabel('Mass/Charge')
        self.ax1.set_ylabel('Voltage (V)')
        self.ax1.locator_params(axis = 'x',nbins=15)

        self.canvas.draw()

        
    def enter_axes(self,event):
        event.inaxes.patch.set_facecolor('yellow')
        event.canvas.draw()

    def leave_axes(self,event):
        event.inaxes.patch.set_facecolor('white')
        event.canvas.draw()

    def onpick(self,event):
        thisline = event.artist
        xdata = thisline.get_xdata()
        ydata = thisline.get_ydata()
        ind = event.ind
        print 'onpick points:', zip(xdata[ind], ydata[ind])
    
    def on_save_plot(self, event):
        file_choices = "PNG (*.png)|*.png"
        
        dlg = wx.FileDialog(
            self, 
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="plot.png",
            wildcard=file_choices,
            style=wx.SAVE)
        
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)
        
    def on_exit(self, event):
        self.Destroy()
        
    def on_about(self, event):
        msg = """ A GUI for viewing/labeling diffraction patterns.
        """
        dlg = wx.MessageDialog(self, msg, "About", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
    
    def flash_status_message(self, msg, flash_len_ms=500):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(
            wx.EVT_TIMER, 
            self.on_flash_status_off, 
            self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)
    
    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')

    # Mouse and key events
    def on_click(self, event):
        #print event.inaxes
        if self.maskKeyPressed:
            # insert vertical line
            lines = event.inaxes.axvline(x=event.xdata,color='k',linestyle='--')
            self.canvas.draw()
        elif self.unmaskKeyPressed:
            pass
        else:
            pass

    def on_press(self, event):
        if event.key == 'm':
            self.maskKeyPressed = 1
            #print('In masking mode: ', event.key)
        elif event.key == 'u':
            self.unmaskKeyPressed = 1
            #print('In unmasking mode: ', event.key)

    def on_release(self, event):
        if event.key == 'm':
            self.maskKeyPressed = 0
            #print('Exiting masking mode: ', event.key)
        elif event.key == 'u':
            self.unmaskKeyPressed = 0
            #print('Exiting masking mode: ', event.key)

#----------------------------------------------------------------------
class MySplashScreen(wx.SplashScreen):
    def __init__(self, rec_ip, rec_port):
        self.rec_ip = rec_ip
        self.rec_port = rec_port
        bmp = wx.Image(opj("bitmaps/unagi.png")).ConvertToBitmap()
        wx.SplashScreen.__init__(self, bmp,
                                 wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT,
                                 30, None, -1) # 3000
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.fc = wx.FutureCall(10, self.ShowMain) # 1000


    def OnClose(self, evt):
        # Make sure the default handler runs too so this window gets
        # destroyed
        evt.Skip()
        self.Hide()
        
        # if the timer is still running then go ahead and show the
        # main frame now
        if self.fc.IsRunning():
            self.fc.Stop()
            self.ShowMain()


    def ShowMain(self):
        frame = mainFrame(self.rec_ip, self.rec_port)
        frame.Show()


def opj(path):
    """Convert paths to the platform-specific separator"""
    st = apply(os.path.join, tuple(path.split('/')))
    # HACK: on Linux, a leading / gets lost...
    if path.startswith('/'):
        st = '/' + st
    return st


#----------------------------------------------------------------------
if __name__ == '__main__':
    rec_ip = sys.argv[1]
    rec_port = int(sys.argv[2])
    app = wx.App()
    splash = MySplashScreen(rec_ip, rec_port)
    splash.Show()
    app.MainLoop()

