#!/usr/bin/env python
"""
vmiViewer:
A GUI for monitoring Velocity-Map-Imaging (VMI).

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.

UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pprint
import random
import wx
import sys
import h5py
import numpy as np
import scipy.ndimage
import lcls as lc
import blackbox as bx
import thread
import math
import zmq
import threading
import pickle
import copy

# The recommended way to use wx with mpl is with the WXAgg
# backend. 
#
import matplotlib as mpl
#mpl.use('WXAgg')
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

#---------------------------------------------------------------------------

# This is how you pre-establish a file filter so that the dialog
# only shows the extension(s) you want it to.
wildcard = "Text file (*.txt)|*.txt|" \
           "Hierarchical Data Format (*.h5)|*.h5|"     \
           "Python source (*.py)|*.py|"     \
           "Compiled Python (*.pyc)|*.pyc|" \
           "Binary dat (*.dat)|*.dat|" \
           "All files (*.*)|*.*"

#---------------------------------------------------------------------------
class mainFrame(wx.Frame):
    """ The main frame of the application
    """
    title = "LCLS VMI Monitor"
    opalName = 'opal1k'
    cold = 20 
    hot = 2300
    det_numPixX = det_numPixY = lc.opal(opalName).det_numPixX
    numSensors = lc.opal(opalName).numSensors
    numDP = 0 # Number of diffraction patterns loaded
    cmax = 0
    cmin = 0
    myFiles = []
    maskKeyPressed = 0
    unmaskKeyPressed = 0
    flags = wx.ALIGN_LEFT | wx.ALL | wx.ALIGN_CENTER_VERTICAL
    # my labels
    haveImage = False
    # Front detector
    img = np.zeros((1,1))
    data = {}
    data['xy_peaks'] = ([],[])
    imgOriginal = np.zeros((1,1))
    rec_ip = "0.0.0.0"
    rec_port = 0

    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, self.title)
   
        self.create_menu()
        self.create_status_bar()
        self.create_main_panel()
        self.draw_figure()

        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.update_plot, self.timer)

    def update_plot(self, event):
        self.draw_figure()

    def init_zmq(self, rec_ip, rec_port):
        self.zmq_context = zmq.Context()
        self.zmq_subscribe = self.zmq_context.socket(zmq.SUB)
        print ('Listening at tcp://%s:%d' % (rec_ip, rec_port))
        self.zmq_subscribe.connect('tcp://%s:%d' % (rec_ip, rec_port))
        self.zmq_subscribe.setsockopt_string(zmq.SUBSCRIBE, u'vmidata')

    def zmq_sub_function(self):
        while True:
            header, tag, msg = self.zmq_subscribe.recv_multipart()
            self.thread_lock.acquire()
            self.data = pickle.loads(msg)
            self.thread_lock.release()

    def create_menu(self):
        self.menubar = wx.MenuBar()
        
        menu_file = wx.Menu()
        m_expt = menu_file.Append(-1, "&Save plot\tCtrl-S", "Save plot to file")
        self.Bind(wx.EVT_MENU, self.on_save_plot, m_expt)
        menu_file.AppendSeparator()
        m_exit = menu_file.Append(-1, "E&xit\tCtrl-X", "Exit")
        self.Bind(wx.EVT_MENU, self.on_exit, m_exit)
        
        menu_help = wx.Menu()
        m_about = menu_help.Append(-1, "&About\tF1", "About the demo")
        self.Bind(wx.EVT_MENU, self.on_about, m_about)
        
        self.menubar.Append(menu_file, "&File")
        self.menubar.Append(menu_help, "&Help")
        self.SetMenuBar(self.menubar)

    def create_main_panel(self):
        """ Creates the main panel with all the controls on it:
             * mpl canvas 
             * mpl navigation toolbar
             * Control panel for interaction
        """
        self.panel = wx.Panel(self)
        
        # Create the mpl Figure and FigCanvas objects. 
        # 8x8 inches, 100 dots-per-inch
        #
        self.dpi = 100
        self.fig = plt.Figure((8.0, 8.0), dpi=self.dpi) # figsize,dpi
        self.canvas = FigCanvas(self.panel, -1, self.fig)
        
        # hbox2
        self.integrate_label = wx.StaticText(self.panel, -1, 
            "Integrate images: ")        
        self.integrateTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_integrate_enter, 
                  self.integrateTextbox)

        self.cosCheckbox = wx.CheckBox(self.panel, -1, 
            "<cos^2 theta>",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_cosCheckbox_enter, self.cosCheckbox)
        # hbox3
        self.centre_label = wx.StaticText(self.panel, -1, 
            "Centre (rows,cols): ") 
        self.centreRowsTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_centreRows_enter, self.centreRowsTextbox)
        self.centreColsTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_centreCols_enter, self.centreColsTextbox)

        self.rotate_label = wx.StaticText(self.panel, -1, 
            "Rotate (ccw): ") 
        self.rotateTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_rotate_enter, self.rotateTextbox)
        
        # hbox3a
        self.velocityConst_label = wx.StaticText(self.panel, -1, 
            "c: ") 
        self.velocityConstTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_velocityConst_enter, self.velocityConstTextbox)
        
        self.velocityTof_label = wx.StaticText(self.panel, -1, 
            "tof: ") 
        self.velocityTofTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_velocityTof_enter, self.velocityTofTextbox)

        # hbox4
        self.roiV_label = wx.StaticText(self.panel, -1, 
            "Velocity ROI (min,max): ") 
        self.roiVmaxTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_roiVmax_enter, self.roiVmaxTextbox)
        self.roiVminTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_roiVmin_enter, self.roiVminTextbox)

        self.roiTheta_label = wx.StaticText(self.panel, -1, 
            "Angle ROI (min,max): ")
        self.roiThetaMaxTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_roiThetaMax_enter, self.roiThetaMaxTextbox)
        self.roiThetaMinTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_roiThetaMin_enter, self.roiThetaMinTextbox)

        self.rings_label = wx.StaticText(self.panel, -1, 
            "Display rings (use comma): ") 
        self.ringsTextbox = wx.TextCtrl(
            self.panel, 
            size=(200,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_rings_enter, self.ringsTextbox)

        self.open = wx.Button(self.panel, -1, "Load image")
        self.Bind(wx.EVT_BUTTON, self.OnOpen, self.open)

        self.save = wx.Button(self.panel, -1, "Save image")
        self.Bind(wx.EVT_BUTTON, self.OnSave, self.save)

        self.liveCheckbox = wx.CheckBox(self.panel, -1, 
            "live stream",
            style=wx.ALIGN_RIGHT)
        self.Bind(wx.EVT_CHECKBOX, self.on_liveCheckbox_enter, self.liveCheckbox)

        self.connect_label = wx.StaticText(self.panel, -1, 
            "Connect (ip,port): ") 
        self.connectIpTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_connectIp_enter, self.connectIpTextbox)
        self.connectPortTextbox = wx.TextCtrl(
            self.panel, 
            size=(100,-1),
            style=wx.TE_PROCESS_ENTER)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_connectPort_enter, self.connectPortTextbox)
        
        # Create Keyboard shortcuts
        #back_id = wx.NewId()
        #self.Bind(wx.EVT_MENU, self.on_back_button, id=back_id)
        #next_id = wx.NewId()
        #self.Bind(wx.EVT_MENU, self.on_next_button, id=next_id)
        #single_id = wx.NewId()
        #self.Bind(wx.EVT_MENU, self.on_single_button, id=single_id)
        #miss_id = wx.NewId()
        #self.Bind(wx.EVT_MENU, self.on_miss_button, id=miss_id)
        #double_id = wx.NewId()
        #self.Bind(wx.EVT_MENU, self.on_double_button, id=double_id)
        #many_id = wx.NewId()
        #self.Bind(wx.EVT_MENU, self.on_many_button, id=many_id)
        self.accel_tbl = wx.AcceleratorTable(
                                   [#(wx.ACCEL_CTRL, ord('B'), back_id),
                                    #(wx.ACCEL_CTRL, ord('N'), next_id),
                                    #(wx.ACCEL_CTRL, ord('0'), miss_id),
                                    #(wx.ACCEL_CTRL, ord('1'), single_id),
                                    #(wx.ACCEL_CTRL, ord('2'), double_id),
                                    #(wx.ACCEL_CTRL, ord('3'), many_id),
                                   ])
        self.SetAcceleratorTable(self.accel_tbl)
        
        # Create the navigation toolbar, tied to the canvas
        #
        self.toolbar = NavigationToolbar(self.canvas)
        
        #
        # Layout with box sizers
        #
        
        # hbox2
        self.hbox2 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox2.Add(self.integrate_label, 0, flag=self.flags)
        self.hbox2.AddSpacer(20)
        self.hbox2.Add(self.integrateTextbox, 0, border=3, flag=self.flags)
        self.hbox2.AddSpacer(10)
        self.hbox2.Add(self.cosCheckbox, 0, border=3, flag=self.flags)
        self.cosCheckbox.SetValue(0)

        # hbox3
        self.hbox3 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox3.Add(self.centre_label, 0, flag=self.flags)
        self.hbox3.AddSpacer(10)
        self.hbox3.Add(self.centreRowsTextbox, 0, border=3, flag=self.flags)
        self.hbox3.Add(self.centreColsTextbox, 0, border=3, flag=self.flags)
        self.hbox3.AddSpacer(10)
        self.hbox3.Add(self.rotate_label, 0, flag=self.flags)
        self.hbox3.AddSpacer(20)
        self.hbox3.Add(self.rotateTextbox, 0, border=3, flag=self.flags)

        # hbox3a
        self.hbox3a = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox3a.Add(self.velocityConst_label, 0, flag=self.flags)
        self.hbox3a.AddSpacer(10)
        self.hbox3a.Add(self.velocityConstTextbox, 0, border=3, flag=self.flags)
        self.hbox3a.AddSpacer(10)
        self.hbox3a.Add(self.velocityTof_label, 0, flag=self.flags)
        self.hbox3a.AddSpacer(20)
        self.hbox3a.Add(self.velocityTofTextbox, 0, border=3, flag=self.flags)

        # hbox4
        self.hbox4 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox4.Add(self.roiV_label, 0, flag=self.flags)
        self.hbox4.AddSpacer(20)
        self.hbox4.Add(self.roiVminTextbox, 0, border=3, flag=self.flags)
        self.hbox4.Add(self.roiVmaxTextbox, 0, border=3, flag=self.flags)
        self.hbox4.AddSpacer(10)
        self.hbox4.Add(self.roiTheta_label, 0, flag=self.flags)
        self.hbox4.AddSpacer(20)
        self.hbox4.Add(self.roiThetaMinTextbox, 0, border=3, flag=self.flags)
        self.hbox4.Add(self.roiThetaMaxTextbox, 0, border=3, flag=self.flags)

        # hbox4a
        self.hbox4a = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox4a.Add(self.rings_label, 0, flag=self.flags)
        self.hbox4a.AddSpacer(20)
        self.hbox4a.Add(self.ringsTextbox, 0, border=3, flag=self.flags)

        # hbox6
        self.hbox6 = wx.BoxSizer(wx.HORIZONTAL)
        self.hbox6.Add(self.open, 0, border=3, flag=self.flags)
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.save, 0, border=3, flag=self.flags)
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.liveCheckbox, 0, border=3, flag=self.flags)        
        self.hbox6.AddSpacer(10) 
        self.hbox6.Add(self.connect_label, 0, flag=self.flags)
        self.hbox6.AddSpacer(20)
        self.hbox6.Add(self.connectIpTextbox, 0, border=3, flag=self.flags)
        self.hbox6.Add(self.connectPortTextbox, 0, border=3, flag=self.flags)
    
        # vbox
        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.vbox.Add(self.toolbar, 0, wx.EXPAND)
        self.vbox.AddSpacer(10)
        self.vbox.Add(self.hbox2, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox3, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox3a, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox4, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox4a, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        self.vbox.Add(self.hbox6, 0, flag = wx.ALIGN_LEFT | wx.TOP)
        
        self.panel.SetSizer(self.vbox)
        self.vbox.Fit(self)

    def OnOpen(self, evt):
        # Create the dialog. In this case the current directory is forced 
        # as the starting directory for the dialog, and no default file name is 
        # forced. This can easilly be changed in your program. This is an 'open' 
        # dialog, and allows multitple file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, 
        # this dialog is set up to change the current working directory to the 
        # path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # Show the dialog and retrieve the user response. If it is the OK 
        # response, process the data.
        if dlg.ShowModal() == wx.ID_OK:
            # This returns a Python list of files that were selected.
            self.filename = dlg.GetPath()

            self.flash_status_message('Drawing image ...')
            # Read in Opal1K
            # Get the image and display
            
            self.get_image()
            self.flash_status_message('Resetting mask ...')
            #self.mask = np.ones(np.shape(self.img))
            #self.maskAsicEdge = np.ones(np.shape(self.img))
            #self.maskDead = np.ones(np.shape(self.img))
            #self.initMask = 1
            self.haveImage = True
            self.draw_figure()
            

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def get_image(self):
        self.img = np.loadtxt( self.filename )
        self.imgOriginal = self.img
        
    def OnSave(self, evt):
        #self.log.WriteText("CWD: %s\n" % os.getcwd())

        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'save' dialog.
        #
        # Unlike the 'open dialog' example found elsewhere, this example does NOT
        # force the current working directory to change if the user chooses a different
        # directory than the one initially set.
        dlg = wx.FileDialog(
            self, message="Save file as ...", 
            defaultDir=os.getcwd(), 
            defaultFile="vmi.txt",
            wildcard=wildcard,
            style=wx.SAVE
            )

        # This sets the default filter that the user will initially see. Otherwise,
        # the first filter in the list will be used by default.
        dlg.SetFilterIndex(3)

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()

            np.savetxt( path, self.img )

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def OnLoad(self, evt):
        # Create the dialog. In this case the current directory is forced as the starting
        # directory for the dialog, and no default file name is forced. This can easilly
        # be changed in your program. This is an 'open' dialog, and allows multitple
        # file selections as well.
        #
        # Finally, if the directory is changed in the process of getting files, this
        # dialog is set up to change the current working directory to the path chosen.
        dlg = wx.FileDialog(
            self, message="Choose a file",
            defaultDir=os.getcwd(), 
            defaultFile="",
            wildcard=wildcard,
            style=wx.OPEN | wx.CHANGE_DIR #| wx.MULTIPLE 
            )

        # This sets the default filter that the user will initially see. Otherwise,
        # the first filter in the list will be used by default.
        dlg.SetFilterIndex(3)

        # Show the dialog and retrieve the user response. If it is the OK response, 
        # process the data.
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()

            self.myLabel = np.loadtxt(path)

        # Destroy the dialog. Don't do this until you are done with it!
        # BAD things can happen otherwise!
        dlg.Destroy()

    def on_integrate_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()  

    def on_centreRows_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_centreCols_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_rotate_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_maskColsEnd_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_rings_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_maskDeadHot_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_cosCheckbox_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_roiVmax_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_roiVmin_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_roiThetaMax_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_roiThetaMin_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_velocityConst_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_velocityTof_enter(self, event):
        self.flash_status_message('Drawing image ...')
        self.draw_figure()

    def on_liveCheckbox_enter(self, event):
        self.flash_status_message('Reading from live data stream ...')

        if self.rec_ip != 0 and self.rec_port != 0:
            self.init_zmq(self.rec_ip, self.rec_port)
            self.thread_lock = threading.Lock()
            self.thread_pub = threading.Thread(target=self.zmq_sub_function)
            self.thread_pub.daemon=True
            self.thread_pub.start()
            if self.liveCheckbox.GetValue() == True:
                self.timer.Start(1000)
            else: 
                self.timer.Stop()

    def on_connectIp_enter(self, event):
        self.flash_status_message('Setting ip address ...')
        self.rec_ip = self.connectIpTextbox.GetValue()

    def on_connectPort_enter(self, event):
        self.flash_status_message('Setting port number ...')
        self.rec_port = int(self.connectPortTextbox.GetValue())

    def create_status_bar(self):
        self.statusbar = self.CreateStatusBar()

    def draw_figure(self):
        """
        Redraws the figure
        """        
        myIntegrate = self.integrateTextbox.GetValue()
        myCentreRows = self.centreRowsTextbox.GetValue()
        myCentreCols = self.centreColsTextbox.GetValue()
        myRotation = self.rotateTextbox.GetValue() 
        myRings = self.ringsTextbox.GetValue()
        calcCos2theta = self.cosCheckbox.GetValue()
        c = self.velocityConstTextbox.GetValue()
        tof = self.velocityTofTextbox.GetValue()

        img = self.imgOriginal

        if self.liveCheckbox.GetValue() == True:
            self.thread_lock.acquire()
            local_data = copy.deepcopy(self.data)
            self.thread_lock.release()
        
        try:
            cenx = int(myCentreRows)
        except:
            cenx = 500
        try:
            ceny = int(myCentreCols)
        except:
            ceny = 500
         
        self.fig.clf()
        ax = self.fig.add_subplot(111, axisbg='w')

        if self.liveCheckbox.GetValue() == True:
            if 'raw_data' in local_data.keys():
                img = local_data['raw_data']
            else:
                img = np.zeros((1000,1000)) 
                img[local_data['xy_peaks']] = 100

        ## Apply changes to img
        # Apply shift
        try:
            img = np.roll(img, 500-cenx, axis=0) # +ve: down
        except ValueError:
            pass
        try:
            img = np.roll(img, 500-ceny, axis=1) # +ve: right
        except ValueError:
            pass
        # Apply rotation
        try:
            img = scipy.ndimage.interpolation.rotate(img,float(myRotation),reshape=False)
        except ValueError:
            pass
        self.img = img

           
        if calcCos2theta == True:
            # test peak positions
            ax.plot(peakListX, peakListY,'rx')

            peakListX = local_data['xy_peaks'][0]
            peakListY = local_data['xy_peaks'][1]
            
            print "myCenters: ", np.mean(peakListX), np.mean(peakListY)

            # centre of mass
            # Shift origin to centre

            px = 500
            py = 500
            centredPeaks=zip(px,py)
            # calculate theta
            theta = np.zeros(shape=(len(peakListX)))
            counter = 0
            for p in centredPeaks:
                ang = math.atan2(p[0],p[1]) # ccw angle between +ve y-axis (-pi:pi)   
                theta[counter] = ang
                counter += 1
            # calculate cos2theta
            self.cos2theta = 0
            for i in theta:
                self.cos2theta += np.cos(i)**2
            self.cos2theta /= len(theta)
            ax.set_title('theta, <cos^2 theta> = ' + str("%.2f" % np.mean(theta)) +', '+ str("%.2f" %self.cos2theta))

            # centred peak positions to velocity
            c = 1.2 # const
            tof = 5e-6 # time of flight
            vx = px / (c * tof)
            vy = py / (c * tof)
            #print vx, vy
            v = np.sqrt(vx**2+vy**2)
            phi = np.zeros(shape=(len(vx)))
            for i in range(len(vx)):
                phi[i] = math.atan2(vy[i],vx[i])
            #print v, phi
        #if self.initMask:  
        self.fig.canvas.mpl_connect('key_press_event', self.on_press)
        self.fig.canvas.mpl_connect('key_release_event', self.on_release)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)

        imgplot = ax.imshow(self.img, interpolation='None', cmap = 'Greys_r')

        ax.set_xlabel('Columns')
        ax.set_ylabel('Rows')

        if not myRings == '':
            rings = [float(x) for x in myRings.split(',') if myRings.strip()]
            pts = np.arange(0, 2.1*np.pi, 0.05)
            centre = lc.opal(self.opalName).det_numPixX/2
            for radius in rings:
                sx = radius*np.sin(pts)+centre
                cx = radius*np.cos(pts)+centre
                ax.plot(sx,cx,'r-')
            ax.plot(centre,centre,'ro')
            ax.autoscale(tight=True)

        self.canvas.draw()
    
    def on_save_plot(self, event):
        file_choices = "PNG (*.png)|*.png"
        
        dlg = wx.FileDialog(
            self, 
            message="Save plot as...",
            defaultDir=os.getcwd(),
            defaultFile="dp.png",
            wildcard=file_choices,
            style=wx.SAVE)
        
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.canvas.print_figure(path, dpi=self.dpi)
            self.flash_status_message("Saved to %s" % path)
        
    def on_exit(self, event):
        self.Destroy()
        
    def on_about(self, event):
        msg = """ A GUI for viewing/labeling diffraction patterns.
        """
        dlg = wx.MessageDialog(self, msg, "About", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()
    
    def flash_status_message(self, msg, flash_len_ms=500):
        self.statusbar.SetStatusText(msg)
        self.timeroff = wx.Timer(self)
        self.Bind(
            wx.EVT_TIMER, 
            self.on_flash_status_off, 
            self.timeroff)
        self.timeroff.Start(flash_len_ms, oneShot=True)
    
    def on_flash_status_off(self, event):
        self.statusbar.SetStatusText('')

    # Mouse and key events
    def on_click(self, event):
        if self.maskKeyPressed:
            self.mask[round(event.ydata),round(event.xdata)] = 0
            print "Masking img(",round(event.ydata),",",round(event.xdata),"): ", self.img[round(event.ydata),round(event.xdata)]
            self.draw_figure()
        elif self.unmaskKeyPressed:
            self.mask[round(event.ydata),round(event.xdata)] = 1
            print "img:",self.img[round(event.ydata),round(event.xdata)]
            print "orig:",self.imgOriginal[round(event.ydata),round(event.xdata)]
            self.img[round(event.ydata),round(event.xdata)] = self.imgOriginal[round(event.ydata),round(event.xdata)]
            print "Unmasking img(",round(event.ydata),",",round(event.xdata),"): ", self.imgOriginal[round(event.ydata),round(event.xdata)]
            self.draw_figure()
        else:
            print "Current pos(",round(event.ydata),",",round(event.xdata),"): ", self.img[round(event.ydata),round(event.xdata)]

    def on_press(self, event):
        if event.key == 'm':
            self.maskKeyPressed = 1
            #print('In masking mode: ', event.key)
        elif event.key == 'u':
            self.unmaskKeyPressed = 1
            #print('In unmasking mode: ', event.key)

    def on_release(self, event):
        if event.key == 'm':
            self.maskKeyPressed = 0
            #print('Exiting masking mode: ', event.key)
        elif event.key == 'u':
            self.unmaskKeyPressed = 0
            #print('Exiting masking mode: ', event.key)
#----------------------------------------------------------------------
class MySplashScreen(wx.SplashScreen):
    def __init__(self):
        bmp = wx.Image(opj("bitmaps/unagi.png")).ConvertToBitmap()
        wx.SplashScreen.__init__(self, bmp,
                                 wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT,
                                 3000, None, -1) # 3000
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.fc = wx.FutureCall(10, self.ShowMain) # 1000


    def OnClose(self, evt):
        # Make sure the default handler runs too so this window gets
        # destroyed
        evt.Skip()
        self.Hide()
        
        # if the timer is still running then go ahead and show the
        # main frame now
        if self.fc.IsRunning():
            self.fc.Stop()
            self.ShowMain()


    def ShowMain(self):
        frame = mainFrame()
        frame.Show()

def opj(path):
    """Convert paths to the platform-specific separator"""
    st = apply(os.path.join, tuple(path.split('/')))
    # HACK: on Linux, a leading / gets lost...
    if path.startswith('/'):
        st = '/' + st
    return st

#----------------------------------------------------------------------
if __name__ == '__main__':
    app = wx.App()    
    splash = MySplashScreen()
    splash.Show()
    app.MainLoop()

