import sys
import blackbox as bx
import sacla as sc
import os.path
import multiprocessing
import numpy as np

def init(path,darkFileNumber,fileNumber):
    darkFilename = path+ '/r'+darkFileNumber+'_rec' + '_bg.h5'
    print "Loading dark file: ", darkFilename
    # Read in dark diffraction pattern
    myRun, myDet_2d, myTag, numDP, hasAssemData, hasUnassemData, hasFront, hasBack = sc.initMPCCD( darkFilename )
    if hasAssemData:
        if hasFront:
            darkDet, haveDark = sc.getAssemDarkMPCCD( darkFilename, myRun, myTag, 1)
        if hasBack:
            darkDetBack, haveDarkBack = sc.getAssemDarkMPCCD( darkFilename, myRun, myTag, 0)
    else:
        print 'Assembled data not found ...'
    # Init diffraction pattern
    filename = path + '/r'+fileNumber+'_rec.h5'
    myRun, myDet_2d, myTag, numDP, hasAssemData, hasUnassemData, hasFront, hasBack = sc.initMPCCD( filename )
    return darkDet, myRun, myTag, numDP, hasAssemData, filename

def subplot(filename,dark,myRun,myTag,hasAssemData,numDP,num,counter,subDimX=5,subDimY=5,N=400,M=400,fsize=10,title='',vmin=0,vmax=5,wspace=0.001,hspace=0.1):
    import matplotlib.pyplot as plt
    ind = 1
    numShown = 0
    cx = cy = 1200
    plt.figure(num,figsize=(fsize,fsize))
    fig = plt.gcf()
    fig.suptitle(title, fontsize=14)
    for i in range(subDimY):
        for j in range(subDimX):
            if counter < numDP:
                plt.subplot(subDimX,subDimY,ind)
                if hasAssemData:
                    img = sc.loadAssembledMPCCD( filename, myRun, myTag, counter, 1) # Front detector
                    #img =  loadAssembledMPCCD( filename, myRun, myTag, counter, 0) # Back detector
                    imgplot = plt.imshow(np.log10(np.abs(img[cy-N:cy+N,cx-M:cx+M]-dark[cy-N:cy+N,cx-M:cx+M]+1)))
                if vmax > 0:
                    imgplot.set_clim(vmin,vmax)
                plt.title(counter)
                plt.axis('off')
                ind += 1
                counter += 1
    plt.subplots_adjust(wspace=wspace,hspace=hspace)
    plt.show()

if __name__ == '__main__':
    # Parameters
    path = sys.argv[1]
    darkFileNumber = sys.argv[2]
    fileNumber = sys.argv[3]
    dark, myRun, myTag, numDP, hasAssemData, filename = init(path,darkFileNumber,fileNumber)
    subDimX = 5
    subDimY = 4
    cropX = 400
    cropY = 400
    figSize = 12
    numFigs = int(np.ceil(float(numDP)/(subDimX*subDimY)))
    counter = 0
    for num in range(numFigs):
        p = multiprocessing.Process(target=subplot,args=(filename,dark,myRun,myTag,hasAssemData,numDP,num,counter,subDimX,subDimY,cropX,cropY,figSize,))
        p.start()
        counter = counter+subDimX*subDimY
    p.join()
    
    
