#!/usr/bin/env python
"""
LCLS toolbox
Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

Unagi is a Japanese word for "total state of awareness"
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.

UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import pprint
import random
import sys
import h5py
import numpy as np
from scipy import sparse

class cspad:
    det_numPixX = 1736 # compact pixel dimension in x
    det_numPixY = 1736 # compact pixel dimension in y
    numSensors = 1
    cold = 20          # cold pixel for dark image
    hot = 2300         # hot pixel for dark image
    data = np.zeros((1,1))        # diffraction data

    def __init__(self):
        self.name = 'cspad'
    
    def displayName(self):
        print self.name

    def loadUnassembledCSPAD(self, filename ):
        """
        Return unassembled detector image

        filename - LCLS CSPAD hdf5 file containing unassembled sensors
        """
        f = h5py.File( filename , "r") # load hdf5 file for reading
        if 'rawdata' in f['data'].keys():
            self.data = f['/data/rawdata']
        elif 'data' in f['data'].keys():
            self.data = f['/data/data']
	return self.data
	f.close()

class opal:
    det_numPix = 0 # pixel dimension
    det_cx = 0 # centre pixel
    numSensors = 1
    cold = 0
    hot = 0
    data = np.zeros((1,1))        # camera data

    def __init__(self, name):
        self.name = name
        if name == 'opal1k':
            self.det_numPix = 1000
            self.det_cx = 500
        elif name == 'opal2k':
            self.det_numPix = 2000
            self.det_cx = 1000
        elif name == 'opal4k':
            self.det_numPix = 4000
            self.det_cx = 2000
        else:
            print 'This detector does not exist %s' % name
    
    def displayName(self):
        print self.name

class acqiris:
    det_numPixX = 2480#1024 # assemble pixel dimension in x
    numSensors = 1
    cold = 0
    hot = 0
    data = np.zeros((1,1))        # trace data

    def __init__(self):
        self.name = 'acqiris'
    
    def displayName(self):
        print self.name

#-----------------------------------------------------------------------
# Deprecate this function. Use CsPAD class
def loadAssembledCSPAD( filename ):#, myRun, myTag, tagInst, front=1):
        """
        Return already assembled detector image with geometry correction.

        filename - LCLS CSPAD hdf5 file
        """
        img = None
        f = h5py.File( filename , "r") # load hdf5 file for reading
        if 'assembleddata' in f['data'].keys():
            img = f['/data/assembleddata']
        elif 'data' in f['data'].keys():
            img = f['/data/data']
        #else:
	    #img = f[myRun+'/detector_2d_assembled_2/'+myTag[tagInst]+'/detector_data']
	return img
	f.close()

# Deprecate this function. Use CsPAD class
def loadUnassembledCSPAD( filename ):
        """
        Return unassembled detector image

        filename - LCLS CSPAD hdf5 file containing unassembled sensors
        """
        img = None
        f = h5py.File( filename , "r") # load hdf5 file for reading
        if 'rawdata' in f['data'].keys():
            img = f['/data/rawdata']
        elif 'data' in f['data'].keys():
            img = f['/data/data']
	return img
	f.close()

# Deprecate this function. Use CsPAD class
def getImageList(directory, tag='h5'):
    """
    Return a recursive list of h5 in given directory.

    directory - root directory for recursive search for files
    tag - look for filenames containing tag  
    """
    myFiles = []
    for dirname, dirnames, filenames in os.walk('.'):
        # print path to all filenames.
        for filename in filenames:
            if tag in filename:
                myFiles.append(os.path.join(dirname, filename))
    return myFiles
