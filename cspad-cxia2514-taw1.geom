clen = /LCLS/detector0-EncoderValue
coffset = 564.6e-3
adu_per_eV = 0.00338
max_adu = 3000
res = 9097.525473

q0a0/min_fs = 0
q0a0/min_ss = 0
q0a0/max_fs = 193
q0a0/max_ss = 184
q0a0/fs = +0.000781x +1.000000y
q0a0/ss = -1.000000x +0.000781y
q0a0/corner_x = 424.067704
q0a0/corner_y = -28.13178

q0a1/min_fs = 194
q0a1/min_ss = 0
q0a1/max_fs = 387
q0a1/max_ss = 184
q0a1/fs = +0.000781x +1.000000y
q0a1/ss = -1.000000x +0.000781y
q0a1/corner_x = 424.221534
q0a1/corner_y = 168.86816

q0a2/min_fs = 0
q0a2/min_ss = 185
q0a2/max_fs = 193
q0a2/max_ss = 369
q0a2/fs = +0.000184x +1.000000y
q0a2/ss = -1.000000x +0.000184y
q0a2/corner_x = 218.104161
q0a2/corner_y = -27.966905

q0a3/min_fs = 194
q0a3/min_ss = 185
q0a3/max_fs = 387
q0a3/max_ss = 369
q0a3/fs = +0.000184x +1.000000y
q0a3/ss = -1.000000x +0.000184y
q0a3/corner_x = 218.140366
q0a3/corner_y = 169.033092

q0a4/min_fs = 0
q0a4/min_ss = 370
q0a4/max_fs = 193
q0a4/max_ss = 554
q0a4/fs = -1.000000x -0.000287y
q0a4/ss = +0.000287x -1.000000y
q0a4/corner_x = 847.103578
q0a4/corner_y = 365.505982

q0a5/min_fs = 194
q0a5/min_ss = 370
q0a5/max_fs = 387
q0a5/max_ss = 554
q0a5/fs = -1.000000x -0.000287y
q0a5/ss = +0.000287x -1.000000y
q0a5/corner_x = 650.103586
q0a5/corner_y = 365.449422

q0a6/min_fs = 0
q0a6/min_ss = 555
q0a6/max_fs = 193
q0a6/max_ss = 739
q0a6/fs = -1.000000x -0.000494y
q0a6/ss = +0.000494x -1.000000y
q0a6/corner_x = 847.314567
q0a6/corner_y = 159.076237

q0a7/min_fs = 194
q0a7/min_ss = 555
q0a7/max_fs = 387
q0a7/max_ss = 739
q0a7/fs = -1.000000x -0.000494y
q0a7/ss = +0.000494x -1.000000y
q0a7/corner_x = 650.314591
q0a7/corner_y = 158.979002

q0a8/min_fs = 0
q0a8/min_ss = 740
q0a8/max_fs = 193
q0a8/max_ss = 924
q0a8/fs = +0.000895x -1.000000y
q0a8/ss = +1.000000x +0.000895y
q0a8/corner_x = 454.945511
q0a8/corner_y = 786.997581

q0a9/min_fs = 194
q0a9/min_ss = 740
q0a9/max_fs = 387
q0a9/max_ss = 924
q0a9/fs = +0.000895x -1.000000y
q0a9/ss = +1.000000x +0.000895y
q0a9/corner_x = 455.121827
q0a9/corner_y = 589.99766

q0a10/min_fs = 0
q0a10/min_ss = 925
q0a10/max_fs = 193
q0a10/max_ss = 1109
q0a10/fs = +0.000620x -1.000000y
q0a10/ss = +1.000000x +0.000620y
q0a10/corner_x = 663.299129
q0a10/corner_y = 787.682928

q0a11/min_fs = 194
q0a11/min_ss = 925
q0a11/max_fs = 387
q0a11/max_ss = 1109
q0a11/fs = +0.000620x -1.000000y
q0a11/ss = +1.000000x +0.000620y
q0a11/corner_x = 663.421258
q0a11/corner_y = 590.682966

q0a12/min_fs = 0
q0a12/min_ss = 1110
q0a12/max_fs = 193
q0a12/max_ss = 1294
q0a12/fs = -0.999983x -0.005874y
q0a12/ss = +0.005874x -0.999983y
q0a12/corner_x = 423.366191
q0a12/corner_y = 768.44392

q0a13/min_fs = 194
q0a13/min_ss = 1110
q0a13/max_fs = 387
q0a13/max_ss = 1294
q0a13/fs = -0.999983x -0.005874y
q0a13/ss = +0.005874x -0.999983y
q0a13/corner_x = 226.36959
q0a13/corner_y = 767.286664

q0a14/min_fs = 0
q0a14/min_ss = 1295
q0a14/max_fs = 193
q0a14/max_ss = 1479
q0a14/fs = -0.999999x -0.001102y
q0a14/ss = +0.001102x -0.999999y
q0a14/corner_x = 422.018481
q0a14/corner_y = 559.854868

q0a15/min_fs = 194
q0a15/min_ss = 1295
q0a15/max_fs = 387
q0a15/max_ss = 1479
q0a15/fs = -0.999999x -0.001102y
q0a15/ss = +0.001102x -0.999999y
q0a15/corner_x = 225.018601
q0a15/corner_y = 559.63774

q1a0/min_fs = 388
q1a0/min_ss = 0
q1a0/max_fs = 581
q1a0/max_ss = 184
q1a0/fs = -1.000000x +0.000781y
q1a0/ss = -0.000781x -1.000000y
q1a0/corner_x = 13.13178
q1a0/corner_y = 420.067704

q1a1/min_fs = 582
q1a1/min_ss = 0
q1a1/max_fs = 775
q1a1/max_ss = 184
q1a1/fs = -1.000000x +0.000781y
q1a1/ss = -0.000781x -1.000000y
q1a1/corner_x = -183.86816
q1a1/corner_y = 420.221534

q1a2/min_fs = 388
q1a2/min_ss = 185
q1a2/max_fs = 581
q1a2/max_ss = 369
q1a2/fs = -1.000000x +0.000184y
q1a2/ss = -0.000184x -1.000000y
q1a2/corner_x = 12.966905
q1a2/corner_y = 214.104161

q1a3/min_fs = 582
q1a3/min_ss = 185
q1a3/max_fs = 775
q1a3/max_ss = 369
q1a3/fs = -1.000000x +0.000184y
q1a3/ss = -0.000184x -1.000000y
q1a3/corner_x = -184.033092
q1a3/corner_y = 214.140366

q1a4/min_fs = 388
q1a4/min_ss = 370
q1a4/max_fs = 581
q1a4/max_ss = 554
q1a4/fs = +0.000287x -1.000000y
q1a4/ss = +1.000000x +0.000287y
q1a4/corner_x = -380.505982
q1a4/corner_y = 843.103578

q1a5/min_fs = 582
q1a5/min_ss = 370
q1a5/max_fs = 775
q1a5/max_ss = 554
q1a5/fs = +0.000287x -1.000000y
q1a5/ss = +1.000000x +0.000287y
q1a5/corner_x = -380.449422
q1a5/corner_y = 646.103586

q1a6/min_fs = 388
q1a6/min_ss = 555
q1a6/max_fs = 581
q1a6/max_ss = 739
q1a6/fs = +0.000494x -1.000000y
q1a6/ss = +1.000000x +0.000494y
q1a6/corner_x = -174.076237
q1a6/corner_y = 843.314567

q1a7/min_fs = 582
q1a7/min_ss = 555
q1a7/max_fs = 775
q1a7/max_ss = 739
q1a7/fs = +0.000494x -1.000000y
q1a7/ss = +1.000000x +0.000494y
q1a7/corner_x = -173.979002
q1a7/corner_y = 646.314591

q1a8/min_fs = 388
q1a8/min_ss = 740
q1a8/max_fs = 581
q1a8/max_ss = 924
q1a8/fs = +1.000000x +0.000895y
q1a8/ss = -0.000895x +1.000000y
q1a8/corner_x = -801.997581
q1a8/corner_y = 450.945511

q1a9/min_fs = 582
q1a9/min_ss = 740
q1a9/max_fs = 775
q1a9/max_ss = 924
q1a9/fs = +1.000000x +0.000895y
q1a9/ss = -0.000895x +1.000000y
q1a9/corner_x = -604.99766
q1a9/corner_y = 451.121827

q1a10/min_fs = 388
q1a10/min_ss = 925
q1a10/max_fs = 581
q1a10/max_ss = 1109
q1a10/fs = +1.000000x +0.000620y
q1a10/ss = -0.000620x +1.000000y
q1a10/corner_x = -802.682928
q1a10/corner_y = 659.299129

q1a11/min_fs = 582
q1a11/min_ss = 925
q1a11/max_fs = 775
q1a11/max_ss = 1109
q1a11/fs = +1.000000x +0.000620y
q1a11/ss = -0.000620x +1.000000y
q1a11/corner_x = -605.682966
q1a11/corner_y = 659.421258

q1a12/min_fs = 388
q1a12/min_ss = 1110
q1a12/max_fs = 581
q1a12/max_ss = 1294
q1a12/fs = +0.005874x -0.999983y
q1a12/ss = +0.999983x +0.005874y
q1a12/corner_x = -783.44392
q1a12/corner_y = 419.366191

q1a13/min_fs = 582
q1a13/min_ss = 1110
q1a13/max_fs = 775
q1a13/max_ss = 1294
q1a13/fs = +0.005874x -0.999983y
q1a13/ss = +0.999983x +0.005874y
q1a13/corner_x = -782.286664
q1a13/corner_y = 222.36959

q1a14/min_fs = 388
q1a14/min_ss = 1295
q1a14/max_fs = 581
q1a14/max_ss = 1479
q1a14/fs = +0.001102x -0.999999y
q1a14/ss = +0.999999x +0.001102y
q1a14/corner_x = -574.854868
q1a14/corner_y = 418.018481

q1a15/min_fs = 582
q1a15/min_ss = 1295
q1a15/max_fs = 775
q1a15/max_ss = 1479
q1a15/fs = +0.001102x -0.999999y
q1a15/ss = +0.999999x +0.001102y
q1a15/corner_x = -574.63774
q1a15/corner_y = 221.018601

q2a0/min_fs = 776
q2a0/min_ss = 0
q2a0/max_fs = 969
q2a0/max_ss = 184
q2a0/fs = -0.000781x -1.000000y
q2a0/ss = +1.000000x -0.000781y
q2a0/corner_x = -431.067704
q2a0/corner_y = 12.13178

q2a1/min_fs = 970
q2a1/min_ss = 0
q2a1/max_fs = 1163
q2a1/max_ss = 184
q2a1/fs = -0.000781x -1.000000y
q2a1/ss = +1.000000x -0.000781y
q2a1/corner_x = -431.221534
q2a1/corner_y = -184.86816

q2a2/min_fs = 776
q2a2/min_ss = 185
q2a2/max_fs = 969
q2a2/max_ss = 369
q2a2/fs = -0.000184x -1.000000y
q2a2/ss = +1.000000x -0.000184y
q2a2/corner_x = -225.104161
q2a2/corner_y = 11.966905

q2a3/min_fs = 970
q2a3/min_ss = 185
q2a3/max_fs = 1163
q2a3/max_ss = 369
q2a3/fs = -0.000184x -1.000000y
q2a3/ss = +1.000000x -0.000184y
q2a3/corner_x = -225.140366
q2a3/corner_y = -185.033092

q2a4/min_fs = 776
q2a4/min_ss = 370
q2a4/max_fs = 969
q2a4/max_ss = 554
q2a4/fs = +1.000000x +0.000287y
q2a4/ss = -0.000287x +1.000000y
q2a4/corner_x = -854.103578
q2a4/corner_y = -381.505982

q2a5/min_fs = 970
q2a5/min_ss = 370
q2a5/max_fs = 1163
q2a5/max_ss = 554
q2a5/fs = +1.000000x +0.000287y
q2a5/ss = -0.000287x +1.000000y
q2a5/corner_x = -657.103586
q2a5/corner_y = -381.449422

q2a6/min_fs = 776
q2a6/min_ss = 555
q2a6/max_fs = 969
q2a6/max_ss = 739
q2a6/fs = +1.000000x +0.000494y
q2a6/ss = -0.000494x +1.000000y
q2a6/corner_x = -854.314567
q2a6/corner_y = -175.076237

q2a7/min_fs = 970
q2a7/min_ss = 555
q2a7/max_fs = 1163
q2a7/max_ss = 739
q2a7/fs = +1.000000x +0.000494y
q2a7/ss = -0.000494x +1.000000y
q2a7/corner_x = -657.314591
q2a7/corner_y = -174.979002

q2a8/min_fs = 776
q2a8/min_ss = 740
q2a8/max_fs = 969
q2a8/max_ss = 924
q2a8/fs = -0.000895x +1.000000y
q2a8/ss = -1.000000x -0.000895y
q2a8/corner_x = -461.945511
q2a8/corner_y = -802.997581

q2a9/min_fs = 970
q2a9/min_ss = 740
q2a9/max_fs = 1163
q2a9/max_ss = 924
q2a9/fs = -0.000895x +1.000000y
q2a9/ss = -1.000000x -0.000895y
q2a9/corner_x = -462.121827
q2a9/corner_y = -605.99766

q2a10/min_fs = 776
q2a10/min_ss = 925
q2a10/max_fs = 969
q2a10/max_ss = 1109
q2a10/fs = -0.000620x +1.000000y
q2a10/ss = -1.000000x -0.000620y
q2a10/corner_x = -670.299129
q2a10/corner_y = -803.682928

q2a11/min_fs = 970
q2a11/min_ss = 925
q2a11/max_fs = 1163
q2a11/max_ss = 1109
q2a11/fs = -0.000620x +1.000000y
q2a11/ss = -1.000000x -0.000620y
q2a11/corner_x = -670.421258
q2a11/corner_y = -606.682966

q2a12/min_fs = 776
q2a12/min_ss = 1110
q2a12/max_fs = 969
q2a12/max_ss = 1294
q2a12/fs = +0.999983x +0.005874y
q2a12/ss = -0.005874x +0.999983y
q2a12/corner_x = -430.366191
q2a12/corner_y = -784.44392

q2a13/min_fs = 970
q2a13/min_ss = 1110
q2a13/max_fs = 1163
q2a13/max_ss = 1294
q2a13/fs = +0.999983x +0.005874y
q2a13/ss = -0.005874x +0.999983y
q2a13/corner_x = -233.36959
q2a13/corner_y = -783.286664

q2a14/min_fs = 776
q2a14/min_ss = 1295
q2a14/max_fs = 969
q2a14/max_ss = 1479
q2a14/fs = +0.999999x +0.001102y
q2a14/ss = -0.001102x +0.999999y
q2a14/corner_x = -429.018481
q2a14/corner_y = -575.854868

q2a15/min_fs = 970
q2a15/min_ss = 1295
q2a15/max_fs = 1163
q2a15/max_ss = 1479
q2a15/fs = +0.999999x +0.001102y
q2a15/ss = -0.001102x +0.999999y
q2a15/corner_x = -232.018601
q2a15/corner_y = -575.63774

q3a0/min_fs = 1164
q3a0/min_ss = 0
q3a0/max_fs = 1357
q3a0/max_ss = 184
q3a0/fs = +1.000000x -0.000781y
q3a0/ss = +0.000781x +1.000000y
q3a0/corner_x = -24.13178
q3a0/corner_y = -439.067704

q3a1/min_fs = 1358
q3a1/min_ss = 0
q3a1/max_fs = 1551
q3a1/max_ss = 184
q3a1/fs = +1.000000x -0.000781y
q3a1/ss = +0.000781x +1.000000y
q3a1/corner_x = 172.86816
q3a1/corner_y = -439.221534

q3a2/min_fs = 1164
q3a2/min_ss = 185
q3a2/max_fs = 1357
q3a2/max_ss = 369
q3a2/fs = +1.000000x -0.000184y
q3a2/ss = +0.000184x +1.000000y
q3a2/corner_x = -23.966905
q3a2/corner_y = -233.104161

q3a3/min_fs = 1358
q3a3/min_ss = 185
q3a3/max_fs = 1551
q3a3/max_ss = 369
q3a3/fs = +1.000000x -0.000184y
q3a3/ss = +0.000184x +1.000000y
q3a3/corner_x = 173.033092
q3a3/corner_y = -233.140366

q3a4/min_fs = 1164
q3a4/min_ss = 370
q3a4/max_fs = 1357
q3a4/max_ss = 554
q3a4/fs = -0.000287x +1.000000y
q3a4/ss = -1.000000x -0.000287y
q3a4/corner_x = 369.505982
q3a4/corner_y = -862.103578

q3a5/min_fs = 1358
q3a5/min_ss = 370
q3a5/max_fs = 1551
q3a5/max_ss = 554
q3a5/fs = -0.000287x +1.000000y
q3a5/ss = -1.000000x -0.000287y
q3a5/corner_x = 369.449422
q3a5/corner_y = -665.103586

q3a6/min_fs = 1164
q3a6/min_ss = 555
q3a6/max_fs = 1357
q3a6/max_ss = 739
q3a6/fs = -0.000494x +1.000000y
q3a6/ss = -1.000000x -0.000494y
q3a6/corner_x = 163.076237
q3a6/corner_y = -862.314567

q3a7/min_fs = 1358
q3a7/min_ss = 555
q3a7/max_fs = 1551
q3a7/max_ss = 739
q3a7/fs = -0.000494x +1.000000y
q3a7/ss = -1.000000x -0.000494y
q3a7/corner_x = 162.979002
q3a7/corner_y = -665.314591

q3a8/min_fs = 1164
q3a8/min_ss = 740
q3a8/max_fs = 1357
q3a8/max_ss = 924
q3a8/fs = -1.000000x -0.000895y
q3a8/ss = +0.000895x -1.000000y
q3a8/corner_x = 790.997581
q3a8/corner_y = -469.945511

q3a9/min_fs = 1358
q3a9/min_ss = 740
q3a9/max_fs = 1551
q3a9/max_ss = 924
q3a9/fs = -1.000000x -0.000895y
q3a9/ss = +0.000895x -1.000000y
q3a9/corner_x = 593.99766
q3a9/corner_y = -470.121827

q3a10/min_fs = 1164
q3a10/min_ss = 925
q3a10/max_fs = 1357
q3a10/max_ss = 1109
q3a10/fs = -1.000000x -0.000620y
q3a10/ss = +0.000620x -1.000000y
q3a10/corner_x = 791.682928
q3a10/corner_y = -678.299129

q3a11/min_fs = 1358
q3a11/min_ss = 925
q3a11/max_fs = 1551
q3a11/max_ss = 1109
q3a11/fs = -1.000000x -0.000620y
q3a11/ss = +0.000620x -1.000000y
q3a11/corner_x = 594.682966
q3a11/corner_y = -678.421258

q3a12/min_fs = 1164
q3a12/min_ss = 1110
q3a12/max_fs = 1357
q3a12/max_ss = 1294
q3a12/fs = -0.005874x +0.999983y
q3a12/ss = -0.999983x -0.005874y
q3a12/corner_x = 772.44392
q3a12/corner_y = -438.366191

q3a13/min_fs = 1358
q3a13/min_ss = 1110
q3a13/max_fs = 1551
q3a13/max_ss = 1294
q3a13/fs = -0.005874x +0.999983y
q3a13/ss = -0.999983x -0.005874y
q3a13/corner_x = 771.286664
q3a13/corner_y = -241.36959

q3a14/min_fs = 1164
q3a14/min_ss = 1295
q3a14/max_fs = 1357
q3a14/max_ss = 1479
q3a14/fs = -0.001102x +0.999999y
q3a14/ss = -0.999999x -0.001102y
q3a14/corner_x = 563.854868
q3a14/corner_y = -437.018481

q3a15/min_fs = 1358
q3a15/min_ss = 1295
q3a15/max_fs = 1551
q3a15/max_ss = 1479
q3a15/fs = -0.001102x +0.999999y
q3a15/ss = -0.999999x -0.001102y
q3a15/corner_x = 563.63774
q3a15/corner_y = -240.018601

