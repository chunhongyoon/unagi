#!/usr/bin/env python
"""
vmiViewer:
A GUI for monitoring Velocity-Map-Imaging (VMI).

Unagi is a Japanese word for 'total state of awareness'
For more explanation:
http://www.youtube.com/watch?v=OJOYdAkIDq0

Copyright (c) Chun Hong Yoon
chun.hong.yoon@desy.de

This file is part of UNAGI.

UNAGI is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UNAGI is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UNAGI.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import time
import numpy 
import zmq
import pickle
import copy
import signal
import pyqtgraph
import PyQt4.QtGui
import PyQt4.QtCore
import cspad_ij_to_xy as geom
import scipy.signal
import h5py
from datetime import datetime

class ZMQListener(PyQt4.QtCore.QObject):
    zmqmessage = PyQt4.QtCore.pyqtSignal(int, dict)
    zmqmessage_monitorparams = PyQt4.QtCore.pyqtSignal(dict)
    
    def __init__(self, curr_rec_ip, curr_rec_port):
        
        PyQt4.QtCore.QObject.__init__(self)
        
        self.rec_ip = curr_rec_ip
        self.rec_port = curr_rec_port
        self.zmq_context = zmq.Context()

        print ('Listening at tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe = self.zmq_context.socket(zmq.SUB)
        self.zmq_subscribe.connect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe.setsockopt_string(zmq.SUBSCRIBE, u'cspaddata')

        self.zmq_subscribe_monitorparams = self.zmq_context.socket(zmq.SUB)
        self.zmq_subscribe.set_hwm(1)
        self.zmq_subscribe_monitorparams.connect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe_monitorparams.setsockopt_string(zmq.SUBSCRIBE, u'monitorparams')

        self.zmq_poller = zmq.Poller()
        self.zmq_poller.register(self.zmq_subscribe, zmq.POLLIN)
        self.zmq_poller.register(self.zmq_subscribe_monitorparams, zmq.POLLIN)

        self.gui_integration_size = 1

        self.listening_timer = PyQt4.QtCore.QTimer()
        self.listening_timer.timeout.connect(self.listen)


    def start_listening(self):
        self.listening_timer.start()


    def stop_listening(self):
        self.listening_timer.stop()
        print ('Disconnecting from tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe.disconnect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        self.zmq_subscribe_monitorparams.disconnect('tcp://%s:%d' % (self.rec_ip, self.rec_port))
        
                
    def listen(self):
        socks = dict(self.zmq_poller.poll(0))
        if self.zmq_subscribe in socks and socks[self.zmq_subscribe] == zmq.POLLIN:
            print 'recieved'
            header, tag, msg = self.zmq_subscribe.recv_multipart()
            zmq_dict = pickle.loads(msg)
            recv_tag =int(tag)
            self.zmqmessage.emit(recv_tag, zmq_dict)
        if self.zmq_subscribe_monitorparams in socks and socks[self.zmq_subscribe_monitorparams] == zmq.POLLIN:
            header, msg = self.zmq_subscribe_monitorparams.recv_multipart()
            params_dict = pickle.loads(msg)
            self.zmqmessage_monitorparams.emit(params_dict)



class MainFrame(PyQt4.QtGui.QWidget):
    """
    The main frame of the application
    """

    listening_thread_start_processing = PyQt4.QtCore.pyqtSignal()
    listening_thread_stop_processing = PyQt4.QtCore.pyqtSignal()
    
    def __init__(self, arg_list):
        super(MainFrame, self).__init__()
        
        # parameters
        self.cspad_shape = (4, 8, 185, 388)
        self.tags = 8
        self.title = 'LCLS CSpad Monitor'
        self.zmq_timer = 4000                # milli seconds
        self.integration_depth_counter = [0,0,0,0,0,0,0,0] # one for each tag
        geom_fnam = 'cspad-cxia2514-taw1.geom'
        self.ij_geom, self.cspad_geom_shape = geom.get_ij_psana_shaped(geom_fnam)
        self.col = numpy.array([0,0,0], dtype=numpy.int)
        self.asic_region = numpy.zeros(self.cspad_geom_shape, numpy.float)
        self.asic_region[self.ij_geom[0], self.ij_geom[1]] = 1.
        self.im = numpy.zeros(self.cspad_geom_shape, numpy.int64)

        self.rec_ip = '127.0.0.1'
        self.rec_port = 14747
        if len(arg_list) == 3:
            self.rec_ip = str(arg_list[1])
            self.rec_port = int(arg_list[2])
        
        # core GUI data for display
        self.network_data = {} 
        self.network_data['event_ids'] = []
        self.local_data                = self.network_data
        
        # cspad
        dis_temp = {}
        dis_temp['cspad_radial_values'] = None
        #
        dis_temp['event_ids'] = []
        dis_temp['cspad_photons']          = numpy.zeros(self.cspad_shape, dtype=numpy.int64)
        dis_temp['cspad_photons_sum']      = numpy.zeros(self.cspad_shape, dtype=numpy.int64)
        dis_temp['cspad_photons_geom']     = numpy.zeros(self.cspad_geom_shape, dtype=numpy.int64)
        dis_temp['cspad_photons_counts']   = []
        dis_temp['cspad_photons_radial_profile'] = None
        dis_temp['cspad_raw']              = numpy.zeros(self.cspad_geom_shape, dtype=numpy.int64)
        dis_temp['cspad_raw_sum']          = numpy.zeros(self.cspad_geom_shape, dtype=numpy.int64)
        dis_temp['cspad_raw_counts']       = []
        dis_temp['cspad_raw_histogram']    = None
        dis_temp['cspad_raw_bins']         = None
        dis_temp['cspad_raw_radial_profile'] = None
        dis_temp['frame_counter']          = 0
        dis_temp['cspad_radial_values']    = None
        dis_temp['tag']                    = 999
        dis_temp['event_ids']              = []
        dis_temp['frames']                 = 0
        
        #
        self.display_data = []
        for i in range(self.tags):
            self.display_data.append(copy.deepcopy(dis_temp))
            self.display_data[-1]['tag'] = i

        self.det_numPixX = 0
        self.det_numPixY = 0
        self.roi = numpy.zeros((1,1))
        self.roi = numpy.where(self.roi) 
        self.changeRoi = False
        
        # user controlled parameters
        self.dis_temp = {}
        self.dis_temp['integration_depth_gui'] = 1
        self.dis_temp['integration_depth'] = 1
        self.dis_temp['rec_ip']            = self.rec_ip
        self.dis_temp['rec_port']          = self.rec_port
        self.dis_temp['snd_ip']            = None
        self.dis_temp['snd_port']          = None
        self.dis_temp['livestream']        = False
        self.dis_temp['viewPeaks']         = False
        self.dis_temp['viewSum']           = False
        self.dis_temp['tag']               = False
        self.dis_temp['bin_cspad']         = 1
        self.dis_temp['radiusroimin']      = 0
        self.dis_temp['radiusroimax']      = 0
        self.dis_temp['angleroimin']       = 0
        self.dis_temp['angleroimax']       = 181

        self.radii = str(self.dis_temp['radiusroimin'])+','+str(self.dis_temp['radiusroimax'])
        

        # initialisation of GUI and network functions
        self.init_zmq()
        self.init_listening_thread()
        self.initUI()
        self.init_timer()


    def init_zmq(self):
        self.zmq_controlling_context = zmq.Context()
        self.zmq_control = self.zmq_controlling_context.socket(zmq.REQ)
        if self.dis_temp['snd_ip'] != None:
            sip = self.dis_temp['snd_ip']
        else:
            sip = self.dis_temp['rec_ip']
        if self.dis_temp['snd_port'] != None:
            sport = self.dis_temp['snd_port']
        else:
            sport = self.dis_temp['rec_port']+1
        self.curr_snd_ip = sip
        self.curr_snd_port = sport
        self.zmq_control.connect('tcp://%s:%d' % (sip, sport))
        return


    def update_display_data(self):
        """ network data --> display data --> call update image (if needed) """
        # live streaming ?
        if self.dis_temp['livestream'] == False :
            return False
        
        # copy the data from the network data
        if self.network_data is None:
            return False
        else :
            self.local_data = copy.deepcopy(self.network_data)
            self.network_data = None
        
        
        tag = self.local_data['tag']


        # Integrate events
        if self.local_data.has_key('cspad_raw') :
            self.display_data[tag]['cspad_raw_sum'][self.ij_geom[0], self.ij_geom[1]] += self.local_data['cspad_raw'].ravel()
        
        if self.local_data.has_key('cspad_photons_ijkl_peaks') :
            for peaks in self.local_data['cspad_photons_ijkl_peaks']:
                self.display_data[tag]['cspad_photons_sum'][peaks] += 1
        
        # zero stuff if count == 0 or count > depth and we are not in running total mode
        if self.dis_temp['integration_depth_gui'] != 0 and (self.dis_temp['integration_depth_gui'] < self.integration_depth_counter[tag] or self.integration_depth_counter[tag] == 0):
            self.integration_depth_counter[tag] = 0
            self.zero_stuff(tag)
        
        if self.dis_temp['integration_depth_gui'] == 0 :
            self.add_stuff(1, 1)
            if tag == self.dis_temp['tag'] :
                self.update_image()
            self.integration_depth_counter[tag] += 1
        
        elif self.dis_temp['integration_depth_gui'] > self.integration_depth_counter[tag] :
            self.add_stuff()
            self.integration_depth_counter[tag] += 1
            
            if self.dis_temp['integration_depth_gui'] == self.integration_depth_counter[tag] :
                self.add_stuff(self.integration_depth_counter[tag], self.dis_temp['integration_depth_gui'])
                self.integration_depth_counter[tag] = 0
                if tag == self.dis_temp['tag'] :
                    self.update_image()
        
        return True


    def update_image(self):
        """ display data + user options --> screen """

        tag = self.dis_temp['tag'] 

        # have we processed any data for this tag yet...?
        # well?
        if self.display_data[tag]['frames'] == 0 :
            return False

        if self.dis_temp['viewPeaks']  :
            
            if self.display_data[tag].has_key('cspad_photons'):
                if self.dis_temp['viewSum'] :
                    self.im[self.ij_geom[0], self.ij_geom[1]] = self.display_data[tag]['cspad_photons_sum'].flatten()
                else :
                    self.im[self.ij_geom[0], self.ij_geom[1]] = self.display_data[tag]['cspad_photons'].flatten()
                bin = self.dis_temp['bin_cspad']
                if bin != 1:
                    self.im = scipy.signal.convolve2d( self.im, numpy.ones((bin, bin)) ) / scipy.signal.convolve2d( self.asic_region, numpy.ones((bin, bin)) )
                    self.im = self.im[::bin, ::bin]

                self.det_numPixY = self.im.shape[0]
                self.det_numPixX = self.im.shape[1]
                self.im[self.roi] = 0

                self.imageWidget.setImage(self.im, autoRange = False, autoLevels = False, autoHistogramRange = False)
            
            if self.display_data[tag].has_key('cspad_photons_counts'):
                self.countsWidget.clear()
                self.countsWidget.setTitle('Photon counts')
                self.countsWidget.plot(self.display_data[tag]['cspad_photons_counts'])

            if self.display_data[tag].has_key('cspad_photons_radial_profile'):
                self.radProfWidget.clear()
                self.radProfWidget.plot(self.display_data[tag]['cspad_radial_values'], self.display_data[tag]['cspad_photons_radial_profile'])
            
        else :
            if self.display_data[tag].has_key('cspad_raw'):
                if self.dis_temp['viewSum'] :
                    self.im = self.display_data[tag]['cspad_raw_sum']
                else :
                    self.im = self.display_data[tag]['cspad_raw']
                bin = self.dis_temp['bin_cspad']
                if bin != 1:
                    self.im = scipy.signal.convolve2d( self.im, numpy.ones((bin, bin)) ) / scipy.signal.convolve2d( self.asic_region, numpy.ones((bin, bin)) )
                    self.im = self.im[::bin, ::bin]

                self.det_numPixY = self.im.shape[0]
                self.det_numPixX = self.im.shape[1]
                self.im[self.roi == 0] = 0

                self.imageWidget.setImage(self.im, autoRange = False, autoLevels = False, autoHistogramRange = False)

            if self.display_data[tag].has_key('cspad_raw_histogram'):
                if self.display_data[tag]['cspad_raw_histogram'] is not None:
                    self.histWidget.plot(self.display_data[tag]['cspad_raw_bins'][1 :-1], self.display_data[tag]['cspad_raw_histogram'][1 :] + 1)
            
            if self.display_data[tag].has_key('cspad_raw_counts'):
                self.countsWidget.clear()
                self.countsWidget.setTitle('Integrated signal')
                self.countsWidget.plot(self.display_data[tag]['cspad_raw_counts'])

            if self.display_data[tag].has_key('cspad_raw_radial_profile'):
                self.radProfWidget.clear()
                self.radProfWidget.plot(self.display_data[tag]['cspad_radial_values'], self.display_data[tag]['cspad_raw_radial_profile'])

        self.display_data[tag]['event_ids'] = self.local_data['event_ids']

        return True


    def add_stuff(self, c=1, t=2):
        PyQt4.QtGui.QApplication.processEvents()

        tag = self.local_data['tag']
        
        if self.local_data.has_key('cspad_raw') :
            if self.local_data['cspad_raw'] is not None:
                self.display_data[tag]['cspad_raw'][self.ij_geom[0], self.ij_geom[1]] += self.local_data['cspad_raw'].ravel()
             
        if self.local_data.has_key('cspad_raw_sum') :
            if self.local_data['cspad_raw_sum'] is not None :
                self.display_data[tag]['cspad_raw_counts'].append(self.local_data['cspad_raw_sum'])
        
        PyQt4.QtGui.QApplication.processEvents()
        
        if self.local_data.has_key('cspad_raw_histogram') :
            if self.local_data['cspad_raw_histogram'] is not None :
                self.display_data[tag]['cspad_raw_bins'], self.display_data[tag]['cspad_raw_histogram'] = self.merge_hists(self.display_data[tag]['cspad_raw_bins'], self.display_data[tag]['cspad_raw_histogram'],
                                                                                                                       self.local_data['cspad_raw_bins'], self.local_data['cspad_raw_histogram'])
        if self.local_data.has_key('cspad_radial_values') :
            if self.local_data['cspad_radial_values'] is not None :
                self.display_data[tag]['cspad_radial_values'] = self.local_data['cspad_radial_values']
             
        if self.local_data.has_key('cspad_raw_radial_profile') :
            if self.local_data['cspad_raw_radial_profile'] is not None :
                if self.display_data[tag]['cspad_raw_radial_profile'] != None :
                    self.display_data[tag]['cspad_raw_radial_profile'] = self.display_data[tag]['cspad_raw_radial_profile'] + self.local_data['cspad_raw_radial_profile']
                else :
                    self.display_data[tag]['cspad_raw_radial_profile'] = self.local_data['cspad_raw_radial_profile']

        if self.local_data.has_key('cspad_photons_radial_profile') :
            if self.local_data['cspad_photons_radial_profile'] is not None :
                self.display_data[tag]['cspad_photons_radial_profile'] = self.local_data['cspad_photons_radial_profile']

        PyQt4.QtGui.QApplication.processEvents()
        
        if self.local_data.has_key('cspad_photons_ijkl_peaks') :
            if self.local_data['cspad_photons_ijkl_peaks'] is not None :
                for peaks in self.local_data['cspad_photons_ijkl_peaks']:
                    self.display_data[tag]['cspad_photons'][peaks] += 1
             
        if self.local_data.has_key('cspad_photons_sum') :
            if self.local_data['cspad_photons_sum'] is not None :
                self.display_data[tag]['cspad_photons_counts'].append(self.local_data['cspad_photons_sum'])
        
        self.display_data[tag]['frames'] += 1


    def zero_stuff(self, tag):
        self.display_data[tag]['event_ids'] = []
        self.display_data[tag]['cspad_raw'].fill(0)
        self.display_data[tag]['cspad_raw_histogram'] = None
        self.display_data[tag]['cspad_photons'].fill(0)
        self.display_data[tag]['cspad_photons_histogram'] = None
        self.display_data[tag]['cspad_raw_bins'] = None
        if self.display_data[tag]['cspad_raw_radial_profile'] is not None :
            self.display_data[tag]['cspad_raw_radial_profile'].fill(0)
        if self.display_data[tag]['cspad_photons_radial_profile'] is not None :
            self.display_data[tag]['cspad_photons_radial_profile'].fill(0)


    def merge_hists(self, bins_old, hist_old, bins_new, hist_new):
        if (bins_old is None) or (hist_old is None) :
            bins = bins_new
            hist = hist_new
        elif (bins_new is None) or (hist_new is None) :
            bins = bins_old
            hist = hist_old
        else :
            # expand the bins to fit both histograms, the bins must have the property: bins[i] = bins[i-1] + 1
            bin_max = max([bins_old[-1], bins_new[-1]])
            bin_min = min([bins_old[0], bins_new[0]])
            bins = numpy.arange(bin_min, bin_max+1, 1)
            
            # add the hists
            hist = numpy.zeros((bins.shape[0]-1), dtype=numpy.int)
            
            h_args = [numpy.where(bins == bins_old[0])[0][0], numpy.where(bins == bins_old[-2])[0][0] + 1]
            hist[ h_args[0] : h_args[1] ] = hist_old 
            
            h_args = [numpy.where(bins == bins_new[0])[0][0], numpy.where(bins == bins_new[-2])[0][0] + 1]
            hist[ h_args[0] : h_args[1] ] += hist_new 
        return bins, hist


    def init_listening_thread(self):
        self.zeromq_listener_thread = PyQt4.QtCore.QThread()
        self.zeromq_listener = ZMQListener(self.dis_temp['rec_ip'], self.dis_temp['rec_port'])
        self.zeromq_listener.moveToThread(self.zeromq_listener_thread)
        self.zeromq_listener.zmqmessage.connect(self.data_received)
        self.zeromq_listener.zmqmessage_monitorparams.connect(self.parameter_changed)
        self.listening_thread_start_processing.connect(self.zeromq_listener.start_listening)
        self.listening_thread_stop_processing.connect(self.zeromq_listener.stop_listening)
        self.zeromq_listener_thread.start()
        self.listening_thread_start_processing.emit()

    
    def data_received(self, tag, di):
        self.network_data = di
        self.network_data['tag'] = tag


    def init_timer(self):
        """ Update the image every milli_secs. """
        self.refresh_timer = PyQt4.QtCore.QTimer()
        self.refresh_timer.timeout.connect(self.update_display_data)
        self.refresh_timer.start(self.zmq_timer)


    def initUI(self):
        """ Set the layout of the GUI window """
        # Input checkers
        self.intregex = PyQt4.QtCore.QRegExp('[0-9]+')
        self.floatregex = PyQt4.QtCore.QRegExp('[0-9\.]+')
        self.ipregex = PyQt4.QtCore.QRegExp('[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')
        
        self.qtipvalidator = PyQt4.QtGui.QRegExpValidator()
        self.qtipvalidator.setRegExp(self.ipregex)
        self.qtintvalidator = PyQt4.QtGui.QRegExpValidator()
        self.qtintvalidator.setRegExp(self.intregex)
        self.qtfloatvalidator = PyQt4.QtGui.QRegExpValidator()
        self.qtfloatvalidator.setRegExp(self.floatregex)

        self.setWindowTitle(self.title)

        # cspad image viewer
        self.imageWidget = pyqtgraph.ImageView(self)
        self.imageWidget.ui.normBtn.hide()
        self.imageWidget.ui.roiBtn.hide()
        self.imageWidget.setLevels(0, 40)
        self.imageWidget.setFixedSize(self.cspad_geom_shape[0],self.cspad_geom_shape[1])

        # cspad histogram viewer
        self.histWidget = pyqtgraph.PlotWidget(self)
        self.histWidget.setTitle('histogram (single frame)')
        self.histWidget.setLabel('bottom', text = 'adus')

        # clear cspad histogram button
        self.histClear = PyQt4.QtGui.QPushButton("clear histogram", self)
        self.histClear.clicked.connect(self.clearHistogram)

        # save state
        self.saveStateButton = PyQt4.QtGui.QPushButton("save state", self)
        self.saveStateButton .clicked.connect(self.saveState)

        # load state
        self.loadStateButton = PyQt4.QtGui.QPushButton("load state", self)
        self.loadStateButton.clicked.connect(self.loadState)

        # photon counts viewer 
        self.countsWidget = pyqtgraph.PlotWidget(self)
        self.countsWidget.setTitle('photon counts')
        self.countsWidget.setLabel('bottom', text = 'frame')
        
        # radial profile viewer
        self.radProfWidget = pyqtgraph.PlotWidget(self)
        self.radProfWidget.setTitle('radial profile')
        self.radProfWidget.setLabel('bottom', text = 'pixel radius')

        # IP Port Input buttons
        self.connect_label = PyQt4.QtGui.QLabel(self)
        self.connect_label.setText('Connect To (IP, Port):')
        self.connectip_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.connectip_lineedit.setText(self.dis_temp['rec_ip'])
        self.connectip_lineedit.setValidator(self.qtipvalidator)
        self.connectip_lineedit.editingFinished.connect(self.update_connection)

        self.connectport_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.connectport_lineedit.setText(str(self.dis_temp['rec_port']))
        self.connectport_lineedit.setValidator(self.qtintvalidator)
        self.connectport_lineedit.editingFinished.connect(self.update_connection)

        # Integration depth button (Master)
        self.integrate_label = PyQt4.QtGui.QLabel(self)
        self.integrate_label.setText('<b>integrate images:</b>')
        toolTip = \
                """Integrate frames on the master side                                        
                <b>1</b>: display each frame <br>
                <b>n</b>: integrate <b>n</b> frames before display <br>
                The histogram is also integrated. Note that this is 
                not the same as taking the histogram of the sum! 
                """
        self.integrate_label.setToolTip(toolTip)
        self.integrate_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.integrate_lineedit.setValidator(self.qtintvalidator)
        self.integrate_lineedit.setText(str(self.dis_temp['integration_depth']))
        self.integrate_lineedit.editingFinished.connect(self.update_integration_size)
         
        # X-ray button
        self.Xray_button = PyQt4.QtGui.QPushButton("X-ray", self)
        self.Xray_button.setCheckable(True)
        self.Xray_button.clicked.connect(self.update_laserTag)

        # Align button
        self.Align_button = PyQt4.QtGui.QPushButton("Align", self)
        self.Align_button.setCheckable(True)
        self.Align_button.clicked.connect(self.update_laserTag)
        
        # Frag button
        self.Frag_button = PyQt4.QtGui.QPushButton("Frag", self)
        self.Frag_button.setCheckable(True)
        self.Frag_button.clicked.connect(self.update_laserTag)
    
        # live stream checkbox
        self.livestream_checkbox = PyQt4.QtGui.QCheckBox('live stream', self)
        self.livestream_checkbox.stateChanged.connect(self.update_livestream)

        # view peaks checkbox
        self.viewPeaks_checkbox = PyQt4.QtGui.QCheckBox('view peaks', self)
        self.viewPeaks_checkbox.stateChanged.connect(self.update_viewPeaks)
 
        # view sum
        self.viewSum_checkbox = PyQt4.QtGui.QCheckBox('view sum', self)
        self.viewSum_checkbox.stateChanged.connect(self.update_viewSum)

        # clear sum
        self.sumClear = PyQt4.QtGui.QPushButton("clear sum", self)
        self.sumClear.clicked.connect(self.clearSum)

        # bin display
        self.bin_label = PyQt4.QtGui.QLabel(self)
        self.bin_label.setText('Bin factor:')
        toolTip = "Bin the cspad for display"
        self.bin_label.setToolTip(toolTip)
        self.bin_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.bin_lineedit.setValidator(self.qtintvalidator)
        self.bin_lineedit.setText(str(self.dis_temp['bin_cspad']))
        self.bin_lineedit.editingFinished.connect(self.update_bin_cspad)

        # ROI
        self.radiusroi_label = PyQt4.QtGui.QLabel(self)
        self.radiusroi_label.setText('NOT USED FOR CALCULATING STATS! Radius ROI (Min,Max):')
        self.radiusroimin_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.radiusroimin_lineedit.setValidator(self.qtfloatvalidator)
        self.radiusroimin_lineedit.setText(str(self.dis_temp['radiusroimin']))
        self.radiusroimin_lineedit.editingFinished.connect(self.update_roi)
        self.radiusroimax_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.radiusroimax_lineedit.setValidator(self.qtfloatvalidator)
        self.radiusroimax_lineedit.setText(str(self.dis_temp['radiusroimax']))
        self.radiusroimax_lineedit.editingFinished.connect(self.update_roi)
 
        self.angleroi_label = PyQt4.QtGui.QLabel(self)
        self.angleroi_label.setText('Angle ROI (Min,Max):')
        self.angleroimin_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.angleroimin_lineedit.setValidator(self.qtfloatvalidator)
        self.angleroimin_lineedit.setText(str(self.dis_temp['angleroimin']))
        self.angleroimin_lineedit.editingFinished.connect(self.update_roi)
        self.angleroimax_lineedit = PyQt4.QtGui.QLineEdit(self)
        self.angleroimax_lineedit.setValidator(self.qtfloatvalidator)
        self.angleroimax_lineedit.setText(str(self.dis_temp['angleroimax']))
        self.angleroimax_lineedit.editingFinished.connect(self.update_roi)

        # Add all the stuff to the layout
        hlayouts = []

        # cspad | hist
        #         ----
        #         phot
        #         ----
        #         radial profile
        # IP Port integrate clear hist X-ray live stream
        #         integrate save state Align view peaks
        #                   load state Frag  view total
        # Bin dis 

        # cspad image layout
        hlayouts.append(PyQt4.QtGui.QHBoxLayout())
        Hsplitter = PyQt4.QtGui.QSplitter(PyQt4.QtCore.Qt.Horizontal)
        Hsplitter.addWidget(self.imageWidget)

        # histogram plot layout
        Vsplitter = PyQt4.QtGui.QSplitter(PyQt4.QtCore.Qt.Vertical)
        Vsplitter.addWidget(self.histWidget)

        # integrated signal / photon counts plot layout
        Vsplitter.addWidget(self.countsWidget)
        
        # radial profile plot layout
        Vsplitter.addWidget(self.radProfWidget)
        Hsplitter.addWidget(Vsplitter)

        hlayouts[-1].addWidget(Hsplitter)
        
        # IP Port Input layout
        hlayouts.append(PyQt4.QtGui.QHBoxLayout())
        hlayouts[-1].addWidget(self.connect_label)
        hlayouts[-1].addWidget(self.connectip_lineedit)
        hlayouts[-1].addWidget(self.connectport_lineedit)

        # integration depth layout
        vlayout2 = PyQt4.QtGui.QVBoxLayout()
        hlayout = PyQt4.QtGui.QHBoxLayout()
        #hlayout.addWidget(self.integrate_gui_label)
        #hlayout.addWidget(self.integrate_gui_lineedit)
        vlayout2.addLayout(hlayout)

        hlayout2 = PyQt4.QtGui.QHBoxLayout()
        hlayout2.addWidget(self.integrate_label)
        hlayout2.addWidget(self.integrate_lineedit)
        vlayout2.addLayout(hlayout2)
        
        hlayouts[-1].addLayout(vlayout2)

        # X-ray layout
        vlayout1 = PyQt4.QtGui.QVBoxLayout()
        vlayout1.addWidget(self.Xray_button)

        # Align layout
        vlayout1.addWidget(self.Align_button)

        # Frag layout
        vlayout1.addWidget(self.Frag_button)
        hlayouts[-1].addLayout(vlayout1)

        # clear histogram button layout
        vlayout3 = PyQt4.QtGui.QVBoxLayout()
        vlayout3.addWidget(self.histClear)
        vlayout3.addWidget(self.saveStateButton)
        vlayout3.addWidget(self.loadStateButton)
        hlayouts[-1].addLayout(vlayout3)

        # live checkbox layout
        vlayout0 = PyQt4.QtGui.QVBoxLayout()
        vlayout0.addWidget(self.livestream_checkbox)

        # view peaks layout
        vlayout0.addWidget(self.viewPeaks_checkbox)

        # view sum layout
        vlayout0.addWidget(self.viewSum_checkbox)

        # clear sum layout
        vlayout0.addWidget(self.sumClear)
        hlayouts[-1].addLayout(vlayout0)
        
        # bin display layout
        hlayouts.append(PyQt4.QtGui.QHBoxLayout())
        hlayouts[-1].addWidget(self.bin_label)
        hlayouts[-1].addWidget(self.bin_lineedit)

        # ROI display layout
        hlayouts.append(PyQt4.QtGui.QHBoxLayout())
        hlayouts[-1].addWidget(self.radiusroi_label)
        hlayouts[-1].addWidget(self.radiusroimin_lineedit)
        hlayouts[-1].addWidget(self.radiusroimax_lineedit)
        hlayouts[-1].addWidget(self.angleroi_label)
        hlayouts[-1].addWidget(self.angleroimin_lineedit)
        hlayouts[-1].addWidget(self.angleroimax_lineedit)
        hlayouts[-1].addStretch()


        # stack everything vertically 
        vlayout = PyQt4.QtGui.QVBoxLayout()
        for hlayout in hlayouts :
            vlayout.addLayout(hlayout)

        self.setLayout(vlayout)
        self.resize(self.cspad_geom_shape[0]+1000,self.cspad_geom_shape[1]+200)
        self.show()


    def update_connection(self, force = False):
        new_rec_ip = str(self.connectip_lineedit.text())
        new_rec_port = int(self.connectport_lineedit.text())
        if (new_rec_ip != self.dis_temp['rec_ip'] or new_rec_port != self.dis_temp['rec_port']) or force:
            self.dis_temp['rec_ip'] = new_rec_ip
            self.dis_temp['rec_port'] = new_rec_port  
            self.listening_thread_stop_processing.emit()
            self.zeromq_listener_thread.quit()
            self.zeromq_listener_thread.wait()
            self.init_listening_thread()
            self.zmq_control.disconnect('tcp://%s:%d' % (self.curr_snd_ip, self.curr_snd_port))
            self.init_zmq()


    def update_livestream(self, state):
        if state == PyQt4.QtCore.Qt.Checked:
            self.dis_temp['livestream'] = True
            self.refresh_timer.start(self.zmq_timer)
        else :
            self.dis_temp['livestream'] = False
            self.refresh_timer.stop()                                       


    def update_viewPeaks(self, state):
        if state == PyQt4.QtCore.Qt.Checked:
            self.dis_temp['viewPeaks'] = True
            self.imageWidget.setLevels(0, 2)
            self.update_image()
        else :
            self.dis_temp['viewPeaks'] = False
            self.imageWidget.setLevels(0, 40)
            self.update_image()


    def update_viewSum(self, state):
        if state == PyQt4.QtCore.Qt.Checked:
            self.dis_temp['viewSum'] = True
            self.update_image()
        else :
            self.dis_temp['viewSum'] = False
            self.update_image()


    def update_integration_size(self):
        new_integration_size = int(self.integrate_lineedit.text())
        if new_integration_size != self.dis_temp['integration_depth']:
            self.dis_temp['integration_depth'] = new_integration_size            
            print 'Sending command to %s:%d' % (self.curr_snd_ip, self.curr_snd_port)
            dict_to_send = {}
            dict_to_send['cspad_average_size'] = new_integration_size
            self.zmq_control.send_pyobj(dict_to_send)
            self.zmq_control.recv()
            print 'Command received'            


    def update_integration_size_gui(self):
        self.dis_temp['integration_depth_gui'] = int(self.integrate_gui_lineedit.text())


    def update_laserTag(self, state):
        source = self.sender()
                        
        if source.text() == "X-ray":
            self.col[2] = state                
        elif source.text() == "Align":
            self.col[1] = state            
        else :
            self.col[0] = state 
        self.dis_temp['tag'] = numpy.sum( self.col * numpy.array([4, 2, 1]) )


    def update_roi(self):

        new_radiusroimin = float(self.radiusroimin_lineedit.text())
        new_radiusroimax = float(self.radiusroimax_lineedit.text())
        new_angleroimin = float(self.angleroimin_lineedit.text())
        new_angleroimax = float(self.angleroimax_lineedit.text())
        if new_radiusroimin != self.dis_temp['radiusroimin'] or new_radiusroimax != self.dis_temp['radiusroimax'] or new_angleroimin != self.dis_temp['angleroimin'] or new_angleroimax != self.dis_temp['angleroimax']:
            self.dis_temp['radiusroimin'] = new_radiusroimin
            self.dis_temp['radiusroimax'] = new_radiusroimax
            self.dis_temp['angleroimin'] = new_angleroimin
            self.dis_temp['angleroimax'] = new_angleroimax
            self.roi = wedgeMask(self.det_numPixY,self.det_numPixX,self.dis_temp['radiusroimax'],self.dis_temp['radiusroimin'],self.dis_temp['angleroimin'],self.dis_temp['angleroimax'])
            self.roi = numpy.where(self.roi == 0)
            self.changeRoi = True
            print 'Sending command to %s:%d' % (self.curr_snd_ip, self.curr_snd_port)
            dict_to_send = {}
            dict_to_send['cspad_roi_radius_min'] = new_radiusroimin
            dict_to_send['cspad_roi_radius_max'] = new_radiusroimax
            dict_to_send['cspad_roi_angle_min'] = new_angleroimin
            dict_to_send['cspad_roi_angle_max'] = new_angleroimax
            self.zmq_control.send_pyobj(dict_to_send)
            self.zmq_control.recv()
            print 'Command received'           
            self.update_image()


    def clearHistogram(self):
        self.histWidget.clear()


    def saveState(self):
        """Save all of the class variables to a h5 file with a time"""
        fnam = 'saved_state_cspadMonitor_%s.h5'%datetime.now().strftime('%Y-%m-%d-%s')
        print 'writing to file:', fnam
        f = h5py.File(fnam, 'w')
        # loop over key value pairs and write to h5
        inp_h5 = f.create_group('dis_temp')
        for k in self.dis_temp.keys() :
            print 'writing ', k, ' of type ', type(self.dis_temp[k])
            if self.dis_temp[k] is not None :
                inp_h5.create_dataset(k, data = self.dis_temp[k])
        #
        dis_h5 = f.create_group('display_data')
        for display in self.display_data :
            dis_tag_h5 = dis_h5.create_group(str(display['tag']))
            for k in display.keys() :
                print 'writing ', k, ' of type ', type(display[k])
                if display[k] is not None :
                    dis_tag_h5.create_dataset(k, data = display[k])
        f.close()
        print 'done'


    def loadState(self):
        fnam = PyQt4.QtGui.QFileDialog.getOpenFileName(self, 'Open file', './')
        print 'reading from file:', fnam, type(fnam)
        fnam = str(fnam)
        print 'reading from file:', fnam, type(fnam)
        f = h5py.File(fnam, 'r')
        print 'hello'
        # loop over key value pairs and read from h5
        dis_temp = f['dis_temp']
        for k in dis_temp.keys() :
            print 'reading ', k, ' of type ', type(dis_temp[k].value)
            self.dis_temp[k] = dis_temp[k].value
        #
        display_data = f['display_data']
        for k_tag in display_data :
            dis_tag = display_data[k_tag]
            for k in dis_tag.keys() :
                print 'reading ', k, ' of type ', type(dis_tag[k].value)
                self.display_data[int(k_tag)][k] = dis_tag[k].value
        f.close()
        self.integrate_lineedit.setText(str(self.dis_temp['integration_depth']))
        self.integrate_gui_lineedit.setText(str(self.dis_temp['integration_depth_gui']))
        
        self.connectip_lineedit.setText(self.dis_temp['rec_ip'])
        self.connectport_lineedit.setText(str(self.dis_temp['rec_port']))
        self.update_connection(force = True)
        
        for tag in range(self.tags):
            self.display_data[tag]['cspad_photons_counts']   = list(self.display_data[tag]['cspad_photons_counts'])
            self.display_data[tag]['cspad_raw_counts']       = list(self.display_data[tag]['cspad_raw_counts']    )
            self.display_data[tag]['event_ids']              = list(self.display_data[tag]['event_ids']           )

        self.update_image()
        print 'done'


    def clearSum(self):
        print 'clearing totals...'
        for tag in range(self.tags) : 
            self.display_data[tag]['cspad_raw_sum'].fill(0)
            self.display_data[tag]['cspad_photons_sum'].fill(0)
        self.update_image()


    def update_bin_cspad(self):
        self.dis_temp['bin_cspad'] = int(self.bin_lineedit.text())
        print 'Bin factor for cspad display:', self.dis_temp['bin_cspad']
        self.update_image()
        

    def parameter_changed(self, di):
        if 'cspad_average_size' in di.keys():
            if di['cspad_average_size'] != self.dis_temp['integration_depth']:
                self.dis_temp['integration_depth'] = int(di['cspad_average_size'])
                self.integrate_lineedit.setText(str(di['cspad_average_size']))
                self.dialog = PyQt4.QtGui.QMessageBox(self)
                self.dialog.setStandardButtons(PyQt4.QtGui.QMessageBox.Ok)
                self.dialog.setIcon(PyQt4.QtGui.QMessageBox.Warning)                
                self.dialog.setWindowTitle('Monitor Parameter Changed')
                self.dialog.setText('WARNING!\n\nOne of the monitor parameters has been changed from another GUI')
                self.dialog.setModal(False)
                self.dialog.show()

    
def wedgeMask(N,M,R,r,minTheta,maxTheta):
    """
    Calculate a donut mask.

    N,M - The height and width of the image
    R   - Maximum radius from center
    r   - Minimum radius from center
    minTheta maxTheta
    """
    import math
    mask = numpy.zeros((N,M))
    for i in range(N):
        xDist = i-N/2.
        xDistSq = xDist**2
        for j in range(M):
            yDist = j-M/2.
            yDistSq = yDist**2
            rSq = xDistSq+yDistSq
            if rSq <= R**2 and rSq >= r**2:
                ang = math.atan2(i-N/2,j-M/2)
                if numpy.abs(ang)*180./3.14 <= maxTheta and numpy.abs(ang)*180./3.14 >= minTheta:
                    mask[j,i] = 1
    return mask

def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)    # allow Control-C 
    app = PyQt4.QtGui.QApplication(sys.argv)
    ex = MainFrame(sys.argv)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()    
